//
//  SNChecksum.m
//  Spinar
//
//  Created by Hena Sofronov on 4/8/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNChecksum.h"

@implementation SNChecksum

+ (unsigned char)checksum:(NSData *)data
{
    unsigned char result;
    if (data.length > 1) {
        result = 0x0;
    }
    else {
        result = 0xff;
    }
    unsigned char *dataBytes = malloc(data.length);
    [data getBytes:dataBytes];
    for (NSUInteger iterator = 0; iterator < data.length; iterator ++) {
        result = result ^ dataBytes[iterator];
    }
    return result;
}

@end
