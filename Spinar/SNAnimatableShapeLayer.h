//
//  SNAnimatableShapeLayer.h
//  Spinar
//
//  Created by Duhov Filipp on 7/18/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

typedef CGPathRef (^SNPathGeneratorBlock)(CGFloat progress);

@interface SNAnimatableShapeLayer : CAShapeLayer

@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, copy) SNPathGeneratorBlock pathGeneratorBlock;

@end
