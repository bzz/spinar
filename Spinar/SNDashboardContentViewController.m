//
//  SNDashboardContentViewController.m
//  Spinar
//
//  Created by Hena Sofronov on 2/29/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNDashboardContentViewController.h"
#import "SNWheelConnector.h"

@interface SNDashboardContentViewController ()

@end

@implementation SNDashboardContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self speedProgressBarView] setState:SNProgressStateSpeed];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [[self speedProgressBarView] setState:([[SNWheelConnector sharedInstance] connectedDevice] ? SNProgressStateSpeed : SNProgressStateNoDevice)];
//    [[self speedProgressBarView] setState:SNProgressStateSpeed];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.speedLabel invalidateIntrinsicContentSize];
}

- (IBAction)handleTapGesture:(UITapGestureRecognizer *)gesture
{
    if ([self.speedProgressBarView isPointInsideArc:[gesture locationInView:self.speedProgressBarView]]) {
        [self.delegate speedViewTapped:self];
    }
}

- (void)refresh
{
    
}

@end
