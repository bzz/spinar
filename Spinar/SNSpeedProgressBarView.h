//
//  SNSpeedProgressBarView.h
//  Spinar
//
//  Created by Duhov Filipp on 7/17/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SNProgressState) {
    SNProgressStateNoDevice,
    SNProgressStateSpeed,
};

IB_DESIGNABLE

@interface SNSpeedProgressBarView : UIView

@property (nonatomic, assign) IBInspectable SNProgressState state;
- (void)setState:(SNProgressState)state animated:(BOOL)animated;

@property (nonatomic, assign) IBInspectable CGFloat slicesStartDegree;
@property (nonatomic, assign) IBInspectable CGFloat slicesEndDegree;

// speed slices
@property (nonatomic, assign) IBInspectable NSUInteger slicesCount;
@property (nonatomic, assign) IBInspectable CGSize sliceSize;
@property (nonatomic, strong) IBInspectable UIColor *sliceColor;
@property (nonatomic, strong) IBInspectable UIColor *selectedSliceColor;
@property (nonatomic, assign) IBInspectable CGFloat speedProgress;

// energy sector
@property (nonatomic, assign) IBInspectable CGFloat batteryOffset;
@property (nonatomic, assign) IBInspectable CGFloat batteryThickness;
@property (nonatomic, strong) IBInspectable UIColor *batteryColor;
@property (nonatomic, assign) IBInspectable CGFloat batteryProgress;
@property (nonatomic, assign) IBInspectable UIColor *batteryBackgroundColor;

// no device
@property (nonatomic, assign) IBInspectable CGFloat borderThickness;
@property (nonatomic, strong) IBInspectable UIColor *borderColor;

- (BOOL)isPointInsideArc:(CGPoint)point;

@end
