//
//  SNBLEManager.h
//  Spinar
//
//  Created by Bohdan Pozharskyi on 11/7/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "SNDevice.h"

static NSString * const kDeviceName = @"DeviceName";
static NSString * const kDeviceUUID = @"DeviceUUID";

typedef void (^WriteCompleteBlock)(NSError *error);
typedef void(^WriteWithValueResponseCompleteBlock)(NSError *error, double value);
typedef void(^WriteWithStringResponseCompleteBlock)(NSError *error, NSString *string);
typedef void(^TimeoutBlock)();

typedef NS_ENUM(NSInteger, SNCharacteristicType) {
    SNCharacteristicTypeNone, //0
    
    SNCharacteristicTypeWheelDirection,
    SNCharacteristicTypeLightsStatus,
    SNCharacteristicTypeBatteryVoltage,
    SNCharacteristicTypeTemperature,
    SNCharacteristicTypeTemperatureGyro,
    SNCharacteristicTypeErrorCode,
    SNCharacteristicTypeWheelPitch,
    SNCharacteristicTypeWheelRoll,
    SNCharacteristicTypeDistanceTraveledPositive,
    
    SNCharacteristicTypeDistanceTraveledNegative, //9
    SNCharacteristicTypeWheelSpeed,
    SNCharacteristicTypeTorque,
    SNCharacteristicTypeQDComponentQ,
    SNCharacteristicTypeQDComponentD,
    
    SNCharacteristicTypeTimeSinceOn, //14
    
    SNCharacteristicTypeCommand, //15
    
    SNCharacteristicTypeWheelID, //16
    SNCharacteristicTypeTotalUseTime,
    SNCharacteristicTypeTotalDistance,
    SNCharacteristicTypeTotalUseTimeUnresettable,
    SNCharacteristicTypeTotalDistanceUnresettable,
    SNCharacteristicTypeMaxSpeed,
    SNCharacteristicTypeFirmwareVersionBT,
    SNCharacteristicTypeFirmwareVersionWheel,
    SNCharacteristicTypeFirmwareVersionBL,
    
    SNCharacteristicTypePasswordStatus,//24

    SNCharacteristicTypeLockStatus,//25
    SNCharacteristicTypeWheelModeStatus,
    SNCharacteristicTypeLearningModeStatus,
    SNCharacteristicTypeSpeedLimitStatus,
    SNCharacteristicTypeSafetyStatus,
    SNCharacteristicTypeLog,
    SNCharacteristicTypeBinaryData,
    SNCharacteristicTypePasswordData,
    SNCharacteristicTypeCommandConfirm,
    SNCharacteristicTypeCount
};

typedef NS_ENUM(NSInteger, SNBTCommand) {
    
    SNBTCommandSetLockStatus,
    SNBTCommandSetLockStatusForProgramming,
    SNBTCommandSetLockStatusForOnlyBtProgramming,
    
    SNBTCommandSetWheelModeStatus,
    SNBTCommandSetLearningMode,
    SNBTCommandSetSpeedLimit,
    SNBTCommandSetLightsStatus,
    SNBTCommandSetSafetyStatus,
    SNBTCommandSetResetSettings,
    
    SNBTCommandSetDeviceName,
    
    SNBTCommandGetBootloaderVersion,
    SNBTCommandSetBootloader,
    SNBTCommandSetProxy,
    SNBTCommandSetReadProtect,
    SNBTCommandSetFlashErase,
    SNBTCommandSetSerialDefault,
    SNBTCommandSetBootUpdateEnable,
    SNBTCommandSetApplyAppImage,
    SNBTCommandSetApplyBootImage,
    SNBTCommandSetProxyExit,
    SNBTCommandSetChunkWrite,
    
    SNBTCommandResetTotalTime,
    SNBTCommandResetTotalDistance,
    SNBTCommandResetMaxSpeed,
    
    SNBTCommandCount
};

@protocol SNBLEManagerConnectionDelegate<NSObject>

- (void)deviceListUpdated;
- (void)deviceConnectedWithUUID:(NSString *)uuidString name:(NSString *)name;
- (void)deviceDisconnected;

@end

@protocol SNBLEManagerDataDelegate <NSObject>

- (void)resetData;
- (void)updateValue:(id)value forCharacteristicType:(SNCharacteristicType)characteristicType;

@end

static NSString  * const kErrorDomainWriting = @"BLE Writing Error";

@interface SNBLEManager : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>

/**
 *  Shared BLE Manager Singleton
 *
 *  @return SNBLEManager shared instance
 */
+ (SNBLEManager *)sharedManager;

- (CBCentralManagerState)bleState;

@property (weak, nonatomic) id<SNBLEManagerConnectionDelegate> connectionDelegate;
@property (weak, nonatomic) id<SNBLEManagerDataDelegate> dataDelegate;

#pragma mark - Connection

/**
 *  Discovered Devices in JSON present. Array of Dictionaries with kDeviceName and kDeviceUUID keys
 *
 *  @return Devices Array
 */
- (NSArray *)discoveredDevices;

/**
 *  Connected Device in JSON present. Dictionary with kDeviceName and kDeviceUUID keys
 *
 *  @return Device Dictionary
 */
- (NSDictionary *)connectedDevice;

/**
 *  Starts discovering devices
 *
 */
- (void)startScan;

/**
 *  Stop scanning devices
 *
 */
- (void)stopScan;

/**
 *  Connect To Device with UUID String
 *
 *  @param uuidString Unique ID of Device with NSString
 */
- (BOOL)connectToUUID:(NSString *)uuidString;

/**
 *  Disconnect from connected Device
 */
- (BOOL)disconnect;

#pragma mark - Accessing Data

/**
 *  Attempt to write data for Characteristic Type for Connected Device
 *
 *  @param data               NSData object to write
 *  @param characteristicType characteristic type
 *  @param completeBlock      callback when done, error == nil if success
 */
- (void)writeData:(NSData *)data forConnectedDeviceCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteCompleteBlock)completeBlock;

/**
 *  Requesting for some characteristic value
 *
 *  @param characteristicType type of needed characteristic
 */
- (void)writeRequestValueForCharacteristic:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock;
- (void)writeRequestStringForCharacteristic:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock;

/**
 *  Write password text data to specific BLE characteristic to access BLE info
 *
 *  @param data          binary data to write, presents UTF8 Encoding for password
 *  @param completeBlock invoked when done
 */
- (void)writePasswordData:(NSData *)data complete:(WriteCompleteBlock)completeBlock;

/**
 *  Write binary data (not command, simple chunk of data) to specific BLE characteristic
 *
 *  @param data          binary data to write
 *  @param completeBlock invoked when done
 */
- (void)writeBinaryData:(NSData *)data complete:(WriteCompleteBlock)completeBlock;
- (void)writeBinaryData:(NSData *)data isChecksumNeeded:(BOOL)isChecksum complete:(WriteCompleteBlock)completeBlock;
- (void)writeBinaryData:(NSData *)data isChecksumNeeded:(BOOL)isChecksum isConfirmNeeded:(BOOL)isConfirmNeeded complete:(WriteCompleteBlock)completeBlock;

/**
 *  Wait the value for some characteristic that will be in some time
 *
 *  @param characteristicType characteristic type where the value should appear
 *  @param completeBlock      block with value you expect
 */
- (void)expectValueForCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock;
- (void)expectValueForCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock useTimeout:(BOOL)isTimeout;
- (void)expectStringForCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock;
- (void)expectStringForCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock useTimeout:(BOOL)isTimeout;

- (void)writeCommand:(SNBTCommand)command complete:(WriteCompleteBlock)completeBlock;
- (void)writeCommand:(SNBTCommand)command withPayload:(NSData *)payloadData complete:(WriteCompleteBlock)completeBlock;
- (void)writeCommand:(SNBTCommand)command withPayload:(NSData *)payloadData complete:(WriteCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout;
- (void)writeCommand:(SNBTCommand)command withPayload:(NSData *)payloadData isConfirmNeeded:(BOOL)isConfirmNeeded complete:(WriteCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout;

- (void)writeCommand:(SNBTCommand)command withValueResponseInCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock;
- (void)writeCommand:(SNBTCommand)command withValueResponseInCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout;
- (void)writeCommand:(SNBTCommand)command withStringResponseInCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock;
- (void)writeCommand:(SNBTCommand)command withStringResponseInCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout;

- (void)writeCommand:(SNBTCommand)command withValueResponsesCount:(NSInteger)count inCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock;
- (void)writeCommand:(SNBTCommand)command withValueResponsesCount:(NSInteger)count inCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout;
- (void)writeCommand:(SNBTCommand)command withStringResponsesCount:(NSInteger)count inCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock;
- (void)writeCommand:(SNBTCommand)command withStringResponsesCount:(NSInteger)count inCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout;

@end
