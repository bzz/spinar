//
//  SNDashboardViewController.m
//  Spinar
//
//  Created by hena on 10/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "SNDashboardViewController.h"
#import "SNWheel.h"
#import "SNSettings.h"
#import "SWRevealViewController.h"
#import "SNConvertor.h"
#import "SNSpeedProgressBarView.h"
#import "UIColorFromRGB.h"
#import "SNWheelConnector.h"
#import "SNDashboardContentViewController.h"
#import "UIAlertView+SNMessage.h"
#import "AppDelegate.h"
#import "SNSpeedStatistics.h"

@interface SNDashboardViewController () <SNWheelDelegate, SNWheelConnectorDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate, SNDashboardContentViewControllerDelegate>

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *distanceUnitsLabels;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UILabel *estimateDistanceLabel;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSDictionary *storyboardIdForIndexOfPage;
@property (strong, nonatomic) NSMutableArray <SNDashboardContentViewController *> *contentViewControllersArray;
@property (strong, nonatomic) NSArray *pageControlColors;
@end

@implementation SNDashboardViewController

#pragma mark - View Controller LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [[SNWheelConnector sharedInstance] startTrying];
    [[SNSpeedStatistics sharedInstance] startStats];
    self.storyboardIdForIndexOfPage = @{@(0): @"SpeedDashboardContentViewController",
                                        @(1): @"ChargeDashboardContentViewController"};
    self.pageControlColors = @[UIColorFromRGB(57, 179, 224), [UIColor whiteColor]];
    [self setPageViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"]];
    [self.pageViewController setDataSource:self];
    [self.pageViewController setDelegate:self];
    [self setContentViewControllersArray:[[NSMutableArray alloc] initWithObjects:[self viewControllerAtIndex:0], nil]];
    
    [self.pageViewController setViewControllers:self.contentViewControllersArray direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    
    [self addChildViewController:self.pageViewController];
    [self.view insertSubview:self.pageViewController.view atIndex:0];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    if ([self currentSpeedController].state == SNProgressStateNoDevice && [[SNWheelConnector sharedInstance] connectedDevice]) {
//        [[SNWheel currentWheel] writePassword:[@"123456" dataUsingEncoding:NSUTF8StringEncoding] withCompleteBlock:^(NSError *error) {
//            if (error) {
//                [UIAlertView showMessage:@"Wheel is not available now" withHeader:@"Error"];
//                return ;
//            }
//        }];
//    }
//    [[self currentSpeedController] setState:([[SNWheelConnector sharedInstance] connectedDevice] ? SNProgressStateSpeed : SNProgressStateNoDevice)];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] connectionStateChanged];
    [SNWheel currentWheel].delegate = self;
    [[SNWheelConnector sharedInstance] setDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [SNWheel currentWheel].delegate = nil;
    [super viewWillDisappear:animated];
}

#pragma mark - IBActions

- (IBAction)pageControlValueChanged:(id)sender {
    NSUInteger index = ((SNDashboardContentViewController *)self.pageViewController.viewControllers.firstObject).pageIndex;
    NSUInteger newIndex = self.pageControl.currentPage;
    [self.pageControl setCurrentPageIndicatorTintColor:self.pageControlColors[self.pageControl.currentPage]];
    [self.pageViewController setViewControllers:@[ [self viewControllerAtIndex:newIndex] ]
                                      direction:(newIndex > index) ? UIPageViewControllerNavigationDirectionForward
                                               : UIPageViewControllerNavigationDirectionReverse
                                       animated:YES
                                     completion:nil];
}
#pragma mark - ContentViewControllersDelegate

- (void)lightsSwitchValueChanged:(BOOL)isOn
{
    [[SNWheel currentWheel] setLightOn:isOn withBlock:NULL];
}

- (void)speedViewTapped:(id)sender
{
//    [self performSegueWithIdentifier:@"ChooseDeviceSegue" sender:self];
}

#pragma mark - SNWheelDelegate protocol implementation

- (void)didUpdateProperties
{
    [self refresh];
    [[self currentContentController] refresh];
}

#pragma mark - Storyboard Sugues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ChooseDeviceSegue"]) {
        
    }
}

#pragma mark - SNWheelConnectorDelegate

- (void)devicesUpdated {
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] connectionStateChanged];
    if ([[SNWheelConnector sharedInstance] connectedDevice]) {
        [[SNWheel currentWheel] writePassword:[@"123456" dataUsingEncoding:NSUTF8StringEncoding] withCompleteBlock:^(NSError *error) {
            if (error) {
                [UIAlertView showMessage:@"Wheel is not available now" withHeader:@"Error"];
                return ;
            }
        }];
    }
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [(SNDashboardContentViewController*) viewController pageIndex];
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index - 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [(SNDashboardContentViewController*) viewController pageIndex];
    
    if (index == NSNotFound || index == 1) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index + 1];
}

- (void)pageViewController:(UIPageViewController *)viewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
    if (!completed) {
        return;
    }
    
    SNDashboardContentViewController *currentViewController = self.pageViewController.viewControllers.lastObject;
    self.pageControl.currentPage = currentViewController.pageIndex;
    [self.pageControl setCurrentPageIndicatorTintColor:self.pageControlColors[self.pageControl.currentPage]];
}


#pragma mark - Helper methods

- (SNSpeedProgressBarView *)currentSpeedController
{
    return [[self currentContentController] speedProgressBarView];
}

- (SNLabel *)currentSpeedLabel
{
    return [[self currentContentController] speedLabel];
}

- (SNDashboardContentViewController *)currentContentController
{
    return self.pageViewController.viewControllers.lastObject;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (self.contentViewControllersArray.count > index) {
        return [self.contentViewControllersArray objectAtIndex:index];
    }
    SNDashboardContentViewController *contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:self.storyboardIdForIndexOfPage[@(index)]];
    [contentViewController setPageIndex:index];
    [contentViewController setDelegate:self];
    [self.contentViewControllersArray addObject:contentViewController];
    return contentViewController;
}

- (void)refresh
{
    self.navigationItem.rightBarButtonItem.tintColor = [SNSettings colorForErrorCode:[[SNWheel currentWheel] errorCode]];
    SNWheel *currentWheel = [SNWheel currentWheel];
    [self currentSpeedController].speedProgress = currentWheel.speed / kWheelMaxSpeed;
    [self currentSpeedController].batteryProgress = currentWheel.batteryLife;
    
    
    double convertedSpeed = currentWheel.speed;
    double convertedDistanceEstimated = currentWheel.distanceEstimate;

    if ([SNSettings sharedSettings].areDisplayingUnitsMiles) {
        convertedSpeed = [SNConvertor kilometresToMiles:convertedSpeed];
        convertedDistanceEstimated = [SNConvertor kilometresToMiles:convertedDistanceEstimated];
    }
    
    self.estimateDistanceLabel.text = [self roundedStringFromDouble:convertedDistanceEstimated];
    [self currentSpeedLabel].text = [[self currentContentController] pageIndex] ?
    [self roundedIntStringFromDouble:currentWheel.batteryLife * 100] : [self roundedStringFromDouble:convertedSpeed];
    
    NSString *distanceUnits = [SNSettings stringForDistanceUnits:[SNSettings sharedSettings].areDisplayingUnitsMiles];
    for (UILabel *distanceUnitsLabel in self.distanceUnitsLabels) {
        distanceUnitsLabel.text = distanceUnits;
    }
}

- (NSString *)roundedIntStringFromDouble:(double)value
{
    return [NSString stringWithFormat:@"%02.0f", value];
}

- (NSString *)roundedStringFromDouble:(double)value
{
    return [NSString stringWithFormat:@"%.1f", value];
}

@end
