//
//  SNSpecificValueExpectation.m
//  Spinar
//
//  Created by Hena Sofronov on 3/23/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNSpecificValueExpectation.h"

@interface SNSpecificValueExpectation()
@property (assign, nonatomic) double value;
@end

@implementation SNSpecificValueExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic isSigned:(BOOL)isSigned value:(double)value completeBlock:(WriteCompleteBlock)block useTimeout:(BOOL)isTimeout
{
    self = [super initWithCharacteristic:characteristic isSigned:isSigned valueBlock:^(NSError *error, double incomeValue) {
//        if (block) {
//            block(error?:(incomeValue != value ? [NSError errorWithDomain:@"Command Writing" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Unexpected Value"}] : nil));
//        }
        if (block) {
            block(error);
        }
    } useTimeout:isTimeout];
    if (self) {
        _value = value;
    }
    return self;
}

- (BOOL)invokeForCharacteristic:(SNCharacteristicType)characteristic binaryData:(NSData *)data error:(NSError *)error
{
    if (error) {
        return [super invokeForCharacteristic:characteristic binaryData:data error:error];
    }
    if ([self valueFromData:data] != self.value) {
        return NO;
    }
    return [super invokeForCharacteristic:characteristic binaryData:data error:error];
}
@end
