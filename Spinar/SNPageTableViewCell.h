//
//  SNPageTableViewCell.h
//  Spinar
//
//  Created by Duhov Filipp on 9/9/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

@class SNPageTableViewCell;

FOUNDATION_EXPORT NSString *const kDistanceUnitKey;
FOUNDATION_EXPORT NSString *const kSpeedUnitKey;
FOUNDATION_EXPORT NSString *const kTimeUnitKey;

@protocol SNPageTableViewCellDataSource <NSObject>

- (id)pageCell:(SNPageTableViewCell *)pageCell valueForKey:(NSString *)key;
- (id)pageCell:(SNPageTableViewCell *)pageCell valueCellPage:(NSUInteger)cellPage;

@end

@protocol SNPageTableViewCellDelegate <NSObject>

- (void)pageSelectedWithIndex:(NSInteger)index;
- (void)pageDidAppearWithIndex:(NSInteger)index;

@end

IB_DESIGNABLE

@interface SNPageTableViewCell : UITableViewCell

@property (nonatomic, copy) IBInspectable NSString *pagesNibName;
@property (nonatomic, weak) id<SNPageTableViewCellDataSource> dataSource;
@property (nonatomic, weak) id<SNPageTableViewCellDelegate> delegate;

- (void)reloadDataCellPage:(NSUInteger)cellPage;
- (void)reloadData;

@end
