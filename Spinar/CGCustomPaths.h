//
//  CGCustomPaths.h
//  Spinar
//
//  Created by Duhov Filipp on 7/20/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#ifndef Spinar_CGCustomPaths_h
#define Spinar_CGCustomPaths_h

static inline CGPathRef CGPathSegment(CGRect rect, CGFloat startAngle, CGFloat endAngle, CGFloat thickness)
{
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    CGFloat arcDiametr = MIN(CGRectGetWidth(rect), CGRectGetHeight(rect));
    CGFloat smallArcRadius = (arcDiametr - thickness) / 2;
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddArc(path, NULL, center.x, center.y, smallArcRadius, startAngle, endAngle, NO);
    CGPathRef strokedPath = CGPathCreateCopyByStrokingPath(path, NULL, thickness, kCGLineCapButt, kCGLineJoinMiter, 10);
    CGPathRelease(path);
    return strokedPath;
}

#endif
