//
//  SNExpectationQueue.m
//  Spinar
//
//  Created by hena on 10/7/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNExpectationQueue.h"

@interface SNExpectationQueue ()<SNExpectationDelegate>
@property (strong, nonatomic) NSMutableArray *expectationArray;
@end

@implementation SNExpectationQueue

- (id)init {
    self = [super init];
    if (self) {
        _expectationArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addExpectation:(SNExpectation *)expectation
{
    expectation.delegate = self;
    [self.expectationArray addObject:expectation];
}

- (void)clearExpectations
{
    [self.expectationArray removeAllObjects];
}

- (BOOL)invokeExpectationForCharacteristicType:(SNCharacteristicType)characteristicType withBinaryData:(NSData *)data error:(NSError *)error
{
    for (SNExpectation *expectation in self.expectationArray) {
        if ([expectation invokeForCharacteristic:characteristicType binaryData:data error:error]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isExpectForCharacteristic:(SNCharacteristicType)characteristicType
{
    for (SNExpectation *expectation in self.expectationArray) {
        if ([expectation isExpectCharacteristic:characteristicType]) {
            return YES;
        }
    }
    return NO;
}

- (void)expectationInvoked:(id)sender
{
    SNExpectation *expectation = sender;
    [self.expectationArray removeObject:expectation];
}

@end
