//
//  SNFirmwareLoader.h
//  Spinar
//
//  Created by hena on 6/26/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNWheel.h"
#import "SNFirmware.h"

typedef NS_ENUM(NSInteger, SNFirmwareLoaderState) {
    SNFirmwareLoaderStateLocking,
    SNFirmwareLoaderStateWaitingForReboot,
    SNFirmwareLoaderStateInitingBootloader,
    SNFirmwareLoaderStateWritingBT,
    SNFirmwareLoaderStateWritingWheel,
    SNFirmwareLoaderStateDone
};

@protocol SNFirmwareLoaderDelegate <NSObject>

- (void)finishedWithError:(NSError *)error;
- (void)stateChanged:(SNFirmwareLoaderState)state;
- (void)progressChanged:(double)progress;

@end
                      
    

@interface SNFirmwareLoader : NSObject

@property (weak, nonatomic) id<SNFirmwareLoaderDelegate> delegate;
@property (strong, nonatomic) SNFirmware *firmwareBT;
@property (strong, nonatomic) SNFirmware *firmwareWheel;
@property (strong, nonatomic) SNFirmware *firmwareBootloaderBT;
@property (strong, nonatomic) SNFirmware *firmwareBootloaderWheel;
@property (assign, nonatomic) SNFirmwareLoaderState state;

- (void)lockForProgramming;
- (void)lockForProgrammingOnlyBt;
- (void)startLoadingWheelFirmware:(SNFirmware *)wheelFirmware
                    andBTFirmware:(SNFirmware *)btFirmware;
- (void)startLoadingBootloaderWheelFirmware:(SNFirmware *)wheelBootloaderFirmware
                              andBTFirmware:(SNFirmware *)btBootloaderFirmware;
@end
