//
//  SNSpeedProgressBarView.m
//  Spinar
//
//  Created by Duhov Filipp on 7/17/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNSpeedProgressBarView.h"
#import "SNAnimatableShapeLayer.h"
#import "CGCustomPaths.h"
#import "CGRectOffset.h"

@interface SNSpeedProgressBarView () {
    CGFloat _speedProgress;
    CGFloat _batteryProgress;
}

@property (nonatomic, weak) IBOutlet UIView *speedView;
@property (nonatomic, weak) IBOutlet UIView *noDeviceView;

@property (nonatomic, strong) CAShapeLayer *speedSlicesBackgroundShapeLayer;
@property (nonatomic, strong) SNAnimatableShapeLayer *speedSlicesShapeLayer;
@property (nonatomic, strong) CAShapeLayer *batteryBackgroundSegmentShapeLayer;
@property (nonatomic, strong) SNAnimatableShapeLayer *batterySegmentShapeLayer;
@property (nonatomic, strong) CAShapeLayer *roundedBorderShapeLayer;

@end

@implementation SNSpeedProgressBarView

#pragma mark - Properties

- (void)setState:(SNProgressState)state
{
    if (_state != state) {
        _state = state;
        
        [self setNeedsLayout];
    }
}

- (void)setState:(SNProgressState)state animated:(BOOL)animated
{
    [UIView animateWithDuration:.6
                     animations:^{
                         self.state = state;
                         [self layoutIfNeeded];
                     }];
}

- (void)setSliceColor:(UIColor *)sliceColor
{
    _sliceColor = sliceColor;
    self.speedSlicesBackgroundShapeLayer.fillColor = sliceColor.CGColor;
}

- (void)setSelectedSliceColor:(UIColor *)selectedSliceColor
{
    _selectedSliceColor = selectedSliceColor;
    self.speedSlicesShapeLayer.fillColor = selectedSliceColor.CGColor;
}

- (void)setBatteryColor:(UIColor *)batteryColor
{
    _batteryColor = batteryColor;
    self.batterySegmentShapeLayer.fillColor = batteryColor.CGColor;
}

- (void)setBatteryBackgroundColor:(UIColor *)batteryBackgroundColor
{
    _batteryBackgroundColor = batteryBackgroundColor;
    self.batteryBackgroundSegmentShapeLayer.fillColor = batteryBackgroundColor.CGColor;
}

- (void)setBorderColor:(UIColor *)borderColor
{
    _borderColor = borderColor;
    self.roundedBorderShapeLayer.fillColor = borderColor.CGColor;
}

- (void)setSpeedProgress:(CGFloat)speedProgress
{
    speedProgress = MIN(1, MAX(0, speedProgress));
    if (_speedProgress != speedProgress) {
        _speedProgress = speedProgress;
        [self setNeedsLayout];
    }
}

- (void)setBatteryProgress:(CGFloat)batteryProgress
{
    batteryProgress = MIN(1, MAX(0, batteryProgress));
    if (_batteryProgress != batteryProgress) {
        _batteryProgress = batteryProgress;
        [self setNeedsLayout];
    }
}

- (CGFloat)speedProgress
{
    if (self.state == SNProgressStateSpeed) {
        return _speedProgress;
    }
    return 0;
}

- (CGFloat)batteryProgress
{
    if (self.state == SNProgressStateSpeed) {
        return _batteryProgress;
    }
    return 0;
}

#pragma mark - Initialization

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self customInit];
    }
    return self;
}

- (void)customInit
{
    __weak typeof(self) weakSelf = self;
    
    self.speedSlicesBackgroundShapeLayer = [CAShapeLayer layer];
    [self.layer addSublayer:self.speedSlicesBackgroundShapeLayer];
    self.roundedBorderShapeLayer = [CAShapeLayer layer];
    [self.layer addSublayer:self.roundedBorderShapeLayer];
    
    // custom layers with animated progress change
    self.speedSlicesShapeLayer = [SNAnimatableShapeLayer layer];
    self.speedSlicesShapeLayer.bounds = self.layer.bounds;
    self.speedSlicesShapeLayer.pathGeneratorBlock = ^CGPathRef(CGFloat progress) {
        return [weakSelf speedSlicesWithProgress:progress];
    };
    [self.layer addSublayer:self.speedSlicesShapeLayer];
    
    self.batteryBackgroundSegmentShapeLayer = [SNAnimatableShapeLayer layer];
    [self.layer addSublayer:self.batteryBackgroundSegmentShapeLayer];
    
    self.batterySegmentShapeLayer = [SNAnimatableShapeLayer layer];
    self.batterySegmentShapeLayer.bounds = self.layer.bounds;
    self.batterySegmentShapeLayer.pathGeneratorBlock = ^CGPathRef(CGFloat progress) {
        return [weakSelf batterySegmentPathWithProgress:progress];
    };
    [self.layer addSublayer:self.batterySegmentShapeLayer];
}

#pragma mark - View lifecycle

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.speedSlicesBackgroundShapeLayer.path = [self speedSlicesWithProgress:1];
    self.batteryBackgroundSegmentShapeLayer.path = [self batterySegmentPathWithProgress:self.state == SNProgressStateSpeed ? 1 : 0];
    self.speedSlicesShapeLayer.progress = self.speedProgress;
    self.batterySegmentShapeLayer.progress = self.batteryProgress;
    self.roundedBorderShapeLayer.path = [self roundedBorderPath];
    
    self.roundedBorderShapeLayer.hidden = (self.state != SNProgressStateNoDevice);
    
    self.speedView.alpha = (self.state == SNProgressStateSpeed);
    self.noDeviceView.alpha = (self.state == SNProgressStateNoDevice);
}

#pragma mark - Paths generation

- (CGPathRef)roundedBorderPath
{
    CGRect rect = CGRectWithOffset(self.bounds, MAX(self.sliceSize.height, self.sliceSize.width) + self.batteryOffset);
    return CGPathSegment(rect, 0, 2 * M_PI, self.borderThickness);
}

- (CGPathRef)batterySegmentPathWithProgress:(CGFloat)progress
{
    CGRect rect = CGRectWithOffset(self.bounds, MAX(self.sliceSize.height, self.sliceSize.width) + self.batteryOffset);
    
    CGFloat startAngle = SNAngleWithDegrees(self.slicesStartDegree);
    CGFloat endAngle = SNAngleWithDegrees(self.slicesEndDegree);
    return CGPathSegment(rect, startAngle, (endAngle + 2 * M_PI - startAngle) * progress + startAngle, self.batteryThickness);
}

- (CGPathRef)speedSlicesWithProgress:(CGFloat)progress
{
    progress = MIN(1, MAX(0, progress));
    CGMutablePathRef path = CGPathCreateMutable();
    CGRect rect = self.bounds;
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    CGFloat startAngle = SNAngleWithDegrees(self.slicesStartDegree);
    CGFloat endAngle = SNAngleWithDegrees(self.slicesEndDegree);
    
    CGRect originalRect = CGRectZero;
    originalRect.size = self.sliceSize;
    originalRect.origin.x = CGRectGetWidth(rect) - CGRectGetWidth(originalRect) - center.y;
    originalRect.origin.y = CGRectGetMidY(rect) - CGRectGetMidY(originalRect) - center.x;
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(center.x, center.y);
    CGFloat rotateStep = (2 * M_PI - (startAngle - endAngle)) / (self.slicesCount - 1);
    transform = CGAffineTransformRotate(transform, startAngle);
    
    for (NSUInteger i = 0; i < self.slicesCount * progress; i++, transform = CGAffineTransformRotate(transform, rotateStep)) {
        CGPathAddRect(path, &transform, originalRect);
    }
    
    return path;
}

#pragma mark - View helpers

- (BOOL)isPointInsideArc:(CGPoint)point
{
    CGPoint center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    CGFloat bigArcRadius = MIN(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds)) / 2 - MAX(self.sliceSize.height, self.sliceSize.width);
    return pow(point.x - center.x, 2) + pow(point.y - center.y, 2) < pow(bigArcRadius, 2);
}

@end
