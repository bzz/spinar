//
//  SNMyWheelTableViewController.m
//  Spinar
//
//  Created by hena on 10/23/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//
#import "SNMyWheelTableViewController.h"
#import "SNWheel.h"
#import "SNConvertor.h"
#import "SNSettings.h"
#import "NSAttributedString+StringForUnits.h"
#import "SNQuestionAlertView.h"
#import "UIColorFromRGB.h"

#import "SNRadialView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import "SWRevealViewController.h"
#import "SNSegmentView.h"
#import "SNPageTableViewCell.h"
#import "UIAlertView+SNMessage.h"

#import "SNSpeedStatistics.h"
//#import "CorePlot-CocoaTouch.h"

double const kpitchGaugeScaleViewVisiblePart = .88;

@interface SNMyWheelTableViewController () <SNWheelDelegate, SNPageTableViewCellDataSource, SNPageTableViewCellDelegate>

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *distanceUnitsLabels;

@property (nonatomic, weak) IBOutlet SNSegmentView *temperatureView;
@property (nonatomic, weak) IBOutlet SNSegmentView *powerView;
@property (nonatomic, weak) IBOutlet UILabel *wheelPitchLabel;
@property (nonatomic, weak) IBOutlet UILabel *powerLabel;
@property (nonatomic, weak) IBOutlet UILabel *temperatureLabel;

@property (nonatomic, weak) IBOutlet UIImageView *pointerView;
@property (nonatomic, weak) IBOutlet UIImageView *pitchGaugeScaleView;

@property (weak, nonatomic) IBOutlet UIView *graphHost;
//@property (weak, nonatomic) IBOutlet CPTGraphHostingView *graphHost;
//@property (strong, nonatomic) CPTXYGraph *graph;

@property (strong, nonatomic) SNSpeedStatistics *speedStats;

@end

@implementation SNMyWheelTableViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.speedStats = [SNSpeedStatistics sharedInstance];
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.navigationItem.rightBarButtonItem.tintColor = UIColorFromRGB(62, 186, 231);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SNWheel currentWheel].delegate = self;
    [self updateData];
    [self refresh];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    // This code makes separators in ios 8.x without left insets
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [SNWheel currentWheel].delegate = nil;
    [super viewWillDisappear:animated];
}

#pragma mark - SNWheelDelegate protocol implementation

- (void)didUpdateProperties
{
    [UIView animateWithDuration:.6
                     animations:^{
                         [self refresh];
                     }
                     completion:^(BOOL finished) {
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - UITableViewDelegate protocol implementation

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    if (indexPath.row + 1 == [tableView numberOfRowsInSection:indexPath.section]) {
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, CGRectGetWidth(cell.frame));
    }
    if ([cell isKindOfClass:[SNPageTableViewCell class]]) {
        SNPageTableViewCell *pageCell = (id)cell;
        pageCell.dataSource = self;
        pageCell.delegate = self;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - SNPageTableViewCellDataSource protocol implementation

- (id)pageCell:(SNPageTableViewCell *)pageCell valueCellPage:(NSUInteger)cellPage
{
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:pageCell];
    if (!cellIndexPath) {
        return nil;
    }
    if (cellIndexPath.row == 1) {
        switch (cellPage) {
            case 0: {
                double value = [[SNWheel currentWheel] journeyDistance];
                if (value > 0) {
                    if ([SNSettings sharedSettings].areDisplayingUnitsMiles) {
                        value = [SNConvertor kilometresToMiles:value];
                    }
                    return @(value);
                }
                return nil;
            }
            case 1: {
                double value = [[SNWheel currentWheel] journeyTime];
                if (value > 0) {
                    return @(value);
                }
                return nil;
            }
            case 2: {
                double value = [[SNWheel currentWheel] maxSpeed];
                if (value > 0) {
                    if ([SNSettings sharedSettings].areDisplayingUnitsMiles) {
                        value = [SNConvertor kilometresToMiles:value];
                    }
                    return @(value);
                }
                return nil;
            }
            default:
                break;
        }
    }
    if (cellIndexPath.row == 2) {
        switch (cellPage) {
            case 0: {
                double value = [[SNWheel currentWheel] totalTime];
                if (value > 0) {
                    return @(value);
                }
                return nil;
            }
            case 1: {
                double value = [[SNWheel currentWheel] totalDistance];
                if (value > 0) {
                    return @(value);
                }
                return nil;
            }
            default:
                break;
        }
    }
    return nil;
}

- (id)pageCell:(SNPageTableViewCell *)pageCell valueForKey:(NSString *)key
{
    if ([key isEqualToString:kDistanceUnitKey]) {
        return [SNSettings stringForDistanceUnits:[SNSettings sharedSettings].areDisplayingUnitsMiles];
    }
    if ([key isEqualToString:kSpeedUnitKey]) {
        return [SNSettings stringForSpeedUnits:[SNSettings sharedSettings].areDisplayingUnitsMiles];
    }
    if ([key isEqualToString:kTimeUnitKey]) {
        return @[[SNSettings stringForTimeUnitsHours], [SNSettings stringForTimeUnitsMinutes]];
    }
    return nil;
}

#pragma mark - SNPageTableViewCellDelegate protocol implementation

- (void)pageDidAppearWithIndex:(NSInteger)index
{
    [self updateData];
}

- (void)pageSelectedWithIndex:(NSInteger)index
{
    if (index == 3) {
        [SNQuestionAlertView showQuestion:@"Usage data will reset. Are you sure?" okButton:@"Yes" noButton:@"No" withHeader:@"Usage" completeBlock:^(BOOL isOk) {
            if (isOk) {
                [[SNWheel currentWheel] resetJourneyTimeWithCompleteBlock:^(NSError *error) {
                    [self refresh];
                    [[SNWheel currentWheel] resetJourneyDistanceWithCompleteBlock:^(NSError *error) {
                        [self refresh];
                        [[SNWheel currentWheel] resetMaxSpeedWithCompleteBlock:^(NSError *error) {
                            [self refresh];
                            [self updateData];
                        }];
                    }];
                }];
            }
        }];
    }
}

#pragma mark - Helper methods

- (void)updatePitchGaugeWithPitch:(double)pitch
{
    [self.pointerView sizeToFit];
    int usedPitch = MIN(MAX(pitch, -kWheelMaxAbsPitch), kWheelMaxAbsPitch);
    CGAffineTransform transform = CGAffineTransformMakeTranslation(
                                                                   usedPitch / kWheelMaxAbsPitch * CGRectGetWidth(self.pitchGaugeScaleView.frame) / 2 * kpitchGaugeScaleViewVisiblePart,
                                                                   abs(usedPitch) / kWheelMaxAbsPitch * CGRectGetHeight(self.pitchGaugeScaleView.frame) / 2 * kpitchGaugeScaleViewVisiblePart);
    transform = CGAffineTransformRotate(transform, usedPitch / kWheelMaxAbsPitch * (M_PI_2 - 1.212));
    self.pointerView.transform = transform;
    self.wheelPitchLabel.text = [NSString stringWithFormat:@"%d", (int)pitch];
}

- (void)updateData
{
    [[SNWheel currentWheel] updateJourneyTimeWithCompleteBlock:^(NSError *error) {
        [self refresh];
        [[SNWheel currentWheel] updateJourneyDistanceWithCompleteBlock:^(NSError *error) {
            [self refresh];
            [[SNWheel currentWheel] updateMaxSpeedWithCompleteBlock:^(NSError *error) {
                [self refresh];
                [[SNWheel currentWheel] updateTotalTimeWithCompleteBlock:^(NSError *error) {
                    [self refresh];
                    [[SNWheel currentWheel] updateTotalDistanceWithCompleteBlock:^(NSError *error) {
                        [self refresh];
                    }];
                }];
            }];
        }];
    }];
}

- (void)refresh
{
    self.navigationItem.rightBarButtonItem.tintColor = [SNSettings colorForErrorCode:[[SNWheel currentWheel] errorCode]];
    self.temperatureLabel.text = @((int)[SNWheel currentWheel].temperature).description;
    self.powerLabel.text = [NSString stringWithFormat:@"%d", MIN((int)([SNWheel currentWheel].motorTorque), 100)];
    [self updatePitchGaugeWithPitch:[SNWheel currentWheel].pitch];
    
    NSString *distanceUnits = [SNSettings stringForDistanceUnits:[SNSettings sharedSettings].areDisplayingUnitsMiles];
    for (UILabel *distanceUnitsLabel in self.distanceUnitsLabels) {
        distanceUnitsLabel.text = distanceUnits;
    }
    self.temperatureView.progress = [SNWheel currentWheel].temperature / kWheelMaxTemperature;
    self.powerView.progress = ([SNWheel currentWheel].motorTorque)/100;
    
    for (id cell in [self.tableView visibleCells]) {
        if ([cell isKindOfClass:[SNPageTableViewCell class]]) {
            [(SNPageTableViewCell *)cell reloadData];
        }
    }
    [self refreshGraph];
}

- (void)refreshGraph
{
//    if ( !self.graph ) {
//        CPTXYGraph *newGraph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
////        CPTTheme *theme      = [CPTTheme themeNamed:kCPTDarkGradientTheme];
////        [newGraph applyTheme:theme];
//        self.graph = newGraph;
//        
//        newGraph.plotAreaFrame.paddingTop    = 30.0;
//        newGraph.plotAreaFrame.paddingBottom = 50.0;
//        newGraph.plotAreaFrame.paddingLeft   = 25.0;
//        newGraph.plotAreaFrame.paddingRight  = 15.0;
//        
//        newGraph.paddingTop    = 0.0;
//        newGraph.paddingBottom = 0.0;
//        newGraph.paddingLeft   = 0.0;
//        newGraph.paddingRight  = 0.0;
//        
//        CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] initWithFrame:newGraph.bounds];
//        dataSourceLinePlot.identifier = @"Data Source Plot";
//        
//        CPTMutableLineStyle *lineStyle = [dataSourceLinePlot.dataLineStyle mutableCopy];
//        lineStyle.lineWidth              = 5.0;
//        lineStyle.lineColor              = [CPTColor colorWithComponentRed:57.0/255 green:179.0/255 blue:224.0/255 alpha:1];
//        dataSourceLinePlot.dataLineStyle = lineStyle;
//        
//        dataSourceLinePlot.dataSource = self;
//        [newGraph addPlot:dataSourceLinePlot];
//    }
//    
//    CPTXYGraph *theGraph = self.graph;
//    self.graphHost.hostedGraph = theGraph;
//    
//    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)theGraph.defaultPlotSpace;
//    NSInteger maxSpeed = (NSInteger)[SNSettings sharedSettings].areDisplayingUnitsMiles ? [SNConvertor kilometresToMiles:24] : 24;
//    NSDecimalNumber *high   = [NSDecimalNumber decimalNumberWithDecimal:@(maxSpeed).decimalValue];
//    NSDecimalNumber *low    = [NSDecimalNumber decimalNumberWithDecimal:@(0).decimalValue];
//    NSDecimalNumber *length = [high decimalNumberBySubtracting:low];
//    
//    // NSLog(@"high = %@, low = %@, length = %@", high, low, length);
//    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:@0.0 length:@(self.speedStats.lastResults.count)];
//    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:low length:length];
//    // Axes
//    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)theGraph.axisSet;
//    
//    CPTXYAxis *x = axisSet.xAxis;
//    x.axisLineStyle = nil;
//    x.majorIntervalLength   = @10.0;
//    x.orthogonalPosition    = @0.0;
//    x.minorTicksPerInterval = 1;
//    CPTMutableTextStyle *textStyle = [[CPTMutableTextStyle alloc] init];
//    textStyle.color = [CPTColor whiteColor];
//    x.titleTextStyle = textStyle;
//    x.titleLocation = @7.5;
//    x.titleOffset = 15;
//    x.titleRotation = 0;
//    x.axisTitle = [[CPTAxisTitle alloc] initWithText:@"Time" textStyle:textStyle];
//    x.labelingPolicy = CPTAxisLabelingPolicyNone;
//    [x updateAxisTitle];
//    
//    CPTXYAxis *y  = axisSet.yAxis;
//    y.axisLineStyle = nil;
//    NSDecimal six = CPTDecimalFromInteger(2);
//    y.majorIntervalLength   = [NSDecimalNumber decimalNumberWithDecimal:CPTDecimalDivide(length.decimalValue, six)];
//    CPTMutableLineStyle *line = [[CPTMutableLineStyle alloc] init];
//    line.lineWidth = 1;
//    line.lineColor = [CPTColor whiteColor];
//    y.majorGridLineStyle = line;
//    y.majorTickLineStyle = nil;
//    y.minorTicksPerInterval = 0;
//    y.minorTickLineStyle    = nil;
//    y.orthogonalPosition    = @0.0;
//    NSNumberFormatter *Xformatter = [[NSNumberFormatter alloc] init];
//    [Xformatter setGeneratesDecimalNumbers:NO];
//    [Xformatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    CPTMutableTextStyle *textStyleY = [[CPTMutableTextStyle alloc] init];
//    textStyleY.color = [CPTColor whiteColor];
//    textStyleY.fontSize = 14;
//    y.labelTextStyle = textStyleY;
//    y.labelFormatter = Xformatter;
//    
//    y.alternatingBandFills  = @[/*[[CPTColor whiteColor] colorWithAlphaComponent:CPTFloat(0.1)], */[NSNull null]];
//    double avg = [SNSettings sharedSettings].areDisplayingUnitsMiles ? [SNConvertor kilometresToMiles:self.speedStats.average] : self.speedStats.average;
//    CPTPlotRange *range = [CPTPlotRange plotRangeWithLocation:@0
//                                                       length:@(avg)];
//    CPTFill *bandFill = [CPTFill fillWithColor:[[CPTColor whiteColor] colorWithAlphaComponent:CPTFloat(0.1)]];
//    [y removeAllBackgroundLimitBands];
//    [y addBackgroundLimitBand:[CPTLimitBand limitBandWithRange:range
//                                                              fill:bandFill]];
//    
//    NSNumber *anchor = @(MIN(MAX(2, avg/2), maxSpeed - 2));
//    CPTPlotSpaceAnnotation *annotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:plotSpace anchorPlotPoint:@[@2, anchor]];
//    CPTTextLayer *label = [[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%@ Avg.",[self roundedStringFromDouble:avg]]];
//    CPTMutableTextStyle *textStyleAnn = [[CPTMutableTextStyle alloc] init];
//    textStyleAnn.color = [CPTColor whiteColor];
//    textStyleAnn.fontSize = 14;
//    label.textStyle = textStyleAnn;
//    annotation.contentLayer = label;
//    [theGraph.plotAreaFrame.plotArea removeAllAnnotations];
//    [theGraph.plotAreaFrame.plotArea addAnnotation:annotation];
//    
//    
//    [theGraph reloadData];
}

- (NSString *)roundedStringFromDouble:(double)value
{
    return [NSString stringWithFormat:@"%.1f", value];
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecords
{
    return self.speedStats.lastResults.count;
}

//-(NSUInteger)numberOfRecordsForPlot:(nonnull CPTPlot *)plot
//{
//    return [self numberOfRecords];
//}
//
//-(nullable id)numberForPlot:(nonnull CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
//{
//    NSNumber *num = @0;
//    
//    if ( fieldEnum == CPTScatterPlotFieldX ) {
//        num = @(index);
//    }
//    else if ( fieldEnum == CPTScatterPlotFieldY ) {
//        NSArray *data = [self.speedStats.lastResults copy];
//        
//        num = @([SNSettings sharedSettings].areDisplayingUnitsMiles ? [SNConvertor kilometresToMiles:[data[index] doubleValue]] : [data[index] doubleValue]);
//    }
//    
//    return num;
//}
//
//-(nullable CPTLayer *)dataLabelForPlot:(nonnull CPTPlot *)plot recordIndex:(NSUInteger)idx
//{
//    if (idx != self.speedStats.lastResults.count - 1) {
//        return [NSNull null];
//    }
//    CPTTextLayer *label = [[CPTTextLayer alloc] initWithText:[self roundedStringFromDouble:[self.speedStats.lastResults.lastObject doubleValue]]];
//    CPTMutableTextStyle *textStyleY = [[CPTMutableTextStyle alloc] init];
//    textStyleY.color = [CPTColor colorWithComponentRed:57.0/255 green:179.0/255 blue:224.0/255 alpha:1];
//    textStyleY.fontSize = 14;
//    label.textStyle = textStyleY;
//    return label;
//}

@end
