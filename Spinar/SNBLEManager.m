//
//  SNBLEManager.m
//  Spinar
//
//  Created by Bohdan Pozharskyi on 11/7/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "SNBLEManager.h"
#import "UIAlertView+SNMessage.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "SNExpectationQueue.h"
#import "SNValueExpectation.h"
#import "SNStringExpectation.h"
#import "SNWritingQueue.h"
#import "SNWriteInvokation.h"
#import "SNSpecificValueExpectation.h"
#import "SNHeaderValueExpectation.h"
#import "SNHeaderStringExpectation.h"
#import "SNDataToIntegerConverter.h"
#import "SNChecksum.h"

#define SCAN_INTERVAL_SEC 5

NSString * const kServiceUUID = @"569A1000-B87F-490C-92CB-11BA5EA5167C";

@interface SNBLEManager ()

@property (strong, nonatomic) CBCentralManager *bluetoothCentralManager;

@property (strong, nonatomic) NSTimer *scanTimer;
@property (strong, nonatomic) NSMutableArray *discoveredPeripheralArray;

@property (strong, nonatomic) CBPeripheral *connectPeripheral;
@property (strong, nonatomic) SNExpectationQueue *expectationQueue;
@property (strong, nonatomic) SNWritingQueue *writingQueue;

@property (strong, nonatomic) NSDictionary *characteristicsForUUID;
@property (strong, nonatomic) NSDictionary *bitrangeForCharacteristic;
@property (strong, nonatomic) NSDictionary *commandValueForCharacteristic;
@property (strong, nonatomic) NSDictionary *commandValueForCommand;
@property (strong, nonatomic) NSArray *responseCharacteristics;
@property (strong, nonatomic) NSArray *signedCharacteristics;
@property (strong, nonatomic) NSArray *checksumCommands;

@end

@implementation SNBLEManager

#pragma mark - Initialize

- (SNBLEManager *)init {
    self = [super init];
    if (self)
    {
        _bluetoothCentralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
        _discoveredPeripheralArray = [[NSMutableArray alloc] init];
        _expectationQueue = [[SNExpectationQueue alloc] init];
        _writingQueue = [[SNWritingQueue alloc] init];
        
        self.responseCharacteristics = @[@(SNCharacteristicTypePasswordStatus),
                                         @(SNCharacteristicTypeCommandConfirm)];
        
        self.signedCharacteristics = @[@(SNCharacteristicTypeBatteryVoltage),
                                       @(SNCharacteristicTypeErrorCode),
                                       @(SNCharacteristicTypeTemperature),
                                       @(SNCharacteristicTypeTemperatureGyro),
                                       @(SNCharacteristicTypeWheelPitch),
                                       @(SNCharacteristicTypeWheelRoll),
                                       @(SNCharacteristicTypeTorque),
                                       @(SNCharacteristicTypeQDComponentQ),
                                       @(SNCharacteristicTypeQDComponentD),
                                       @(SNCharacteristicTypeSpeedLimitStatus)];
        
        self.checksumCommands = @[
//                                  @(SNBTCommandSetBootloader),
                                  @(SNBTCommandGetBootloaderVersion),
                                  @(SNBTCommandSetProxy),
                                  @(SNBTCommandSetReadProtect),
                                  @(SNBTCommandSetFlashErase),
                                  @(SNBTCommandSetSerialDefault),
                                  @(SNBTCommandSetBootUpdateEnable),
                                  @(SNBTCommandSetApplyAppImage),
                                  @(SNBTCommandSetApplyBootImage),
//                                  @(SNBTCommandSetProxyExit),
                                  @(SNBTCommandSetChunkWrite)
                                  ];
        
        self.commandValueForCharacteristic = @{@(SNCharacteristicTypeWheelID): @(0x69),
                                               @(SNCharacteristicTypeTotalUseTime): @(0x76),
                                               @(SNCharacteristicTypeTotalDistance): @(0x45),
                                               @(SNCharacteristicTypeTotalUseTimeUnresettable): @(0x74),
                                               @(SNCharacteristicTypeTotalDistanceUnresettable): @(0x41),
                                               @(SNCharacteristicTypeMaxSpeed): @(0x38),
                                               @(SNCharacteristicTypeFirmwareVersionBT): @(0x68),
                                               @(SNCharacteristicTypeFirmwareVersionWheel): @(0x69),
                                               @(SNCharacteristicTypeFirmwareVersionBL): @(0x4d),
                                               @(SNCharacteristicTypeLockStatus): @(0x75),
                                               @(SNCharacteristicTypeWheelModeStatus): @(0x54),
                                               @(SNCharacteristicTypeLearningModeStatus): @(0x20),
                                               @(SNCharacteristicTypeSpeedLimitStatus): @(0x3a),
                                               @(SNCharacteristicTypeSafetyStatus): @(0x30)};
        
        self.commandValueForCommand = @{@(SNBTCommandSetLockStatus): @(0x36),
                                        @(SNBTCommandSetWheelModeStatus): @(0x52),
                                        @(SNBTCommandSetLearningMode): @(0x22),
                                        @(SNBTCommandSetSpeedLimit): @(0x3b),
                                        @(SNBTCommandSetLightsStatus): @(0x78),
                                        @(SNBTCommandSetSafetyStatus): @(0x37),
                                        @(SNBTCommandSetResetSettings): @(0x3c),
                                        @(SNBTCommandSetDeviceName): @(0x4e),
                                        @(SNBTCommandSetLockStatusForProgramming): @(0x4c),
                                        @(SNBTCommandSetLockStatusForOnlyBtProgramming): @(0x6c),
                                        @(SNBTCommandResetTotalDistance): @(0x35),
                                        @(SNBTCommandResetTotalTime): @(0x42),
                                        @(SNBTCommandResetMaxSpeed): @(0x34),
                                        @(SNBTCommandGetBootloaderVersion): @(0x08),
                                        @(SNBTCommandSetBootloader): @(0x7f),
                                        @(SNBTCommandSetProxy): @(0x28),
                                        @(SNBTCommandSetReadProtect): @(0x82),
                                        @(SNBTCommandSetFlashErase): @(0x43),
                                        @(SNBTCommandSetSerialDefault): @(0x0a),
                                        @(SNBTCommandSetBootUpdateEnable): @(0xa2),
                                        @(SNBTCommandSetApplyAppImage): @(0x39),
                                        @(SNBTCommandSetApplyBootImage): @(0x33),
                                        @(SNBTCommandSetProxyExit): @(0x7e),
                                        @(SNBTCommandSetChunkWrite): @(0x31)};
        
        self.characteristicsForUUID = @{@"569A1000-B87F-490C-92CB-11BA5EA5167C": @{@(SNCharacteristicTypeWheelDirection): [NSValue valueWithRange:NSMakeRange(0, 1)],
                                                                                   @(SNCharacteristicTypeLightsStatus): [NSValue valueWithRange:NSMakeRange(0, 1)],
                                                                                   @(SNCharacteristicTypeBatteryVoltage): [NSValue valueWithRange:NSMakeRange(1, 2)],
                                                                                   @(SNCharacteristicTypeTemperature): [NSValue valueWithRange:NSMakeRange(4, 1)],
                                                                                   @(SNCharacteristicTypeTemperatureGyro): [NSValue valueWithRange:NSMakeRange(5, 1)],
                                                                                   @(SNCharacteristicTypeWheelPitch): [NSValue valueWithRange:NSMakeRange(6, 4)],
                                                                                   @(SNCharacteristicTypeWheelRoll): [NSValue valueWithRange:NSMakeRange(10, 4)],
                                                                                   @(SNCharacteristicTypeDistanceTraveledPositive): [NSValue valueWithRange:NSMakeRange(14, 3)]},
                                        @"569A1001-B87F-490C-92CB-11BA5EA5167C": @{@(SNCharacteristicTypeDistanceTraveledNegative): [NSValue valueWithRange:NSMakeRange(0, 3)],
                                                                                   @(SNCharacteristicTypeWheelSpeed): [NSValue valueWithRange:NSMakeRange(3, 2)],
                                                                                   @(SNCharacteristicTypeTorque): [NSValue valueWithRange:NSMakeRange(5, 2)],
                                                                                   @(SNCharacteristicTypeQDComponentQ): [NSValue valueWithRange:NSMakeRange(7, 2)],
                                                                                   @(SNCharacteristicTypeQDComponentD): [NSValue valueWithRange:NSMakeRange(9, 2)]},
                                        @"569A1002-B87F-490C-92CB-11BA5EA5167C": @{@(SNCharacteristicTypeTimeSinceOn): [NSValue valueWithRange:NSMakeRange(0, 4)],
                                                                                   @(SNCharacteristicTypeErrorCode): [NSValue valueWithRange:NSMakeRange(4, 4)]},
                                        @"569A1003-B87F-490C-92CB-11BA5EA5167C": @{},
                                        @"569A1004-B87F-490C-92CB-11BA5EA5167C": @{@(SNCharacteristicTypeTotalUseTime): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypeTotalDistance): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypeTotalUseTimeUnresettable): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypeTotalDistanceUnresettable): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypeMaxSpeed): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypeFirmwareVersionBT): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypeFirmwareVersionWheel): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypeFirmwareVersionBL): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypeLockStatus): [NSValue valueWithRange:NSMakeRange(0, 2)],
                                                                                   @(SNCharacteristicTypeWheelModeStatus): [NSValue valueWithRange:NSMakeRange(0, 2)],
                                                                                   @(SNCharacteristicTypeSafetyStatus): [NSValue valueWithRange:NSMakeRange(0, 2)],
                                                                                   @(SNCharacteristicTypeLearningModeStatus): [NSValue valueWithRange:NSMakeRange(0, 2)],
                                                                                   @(SNCharacteristicTypeSpeedLimitStatus): [NSValue valueWithRange:NSMakeRange(0, 2)],
                                                                                   @(SNCharacteristicTypeWheelID): [NSValue valueWithRange:NSMakeRange(0, 13)],
                                                                                   @(SNCharacteristicTypeCommandConfirm): [NSValue valueWithRange:NSMakeRange(0, 5)],
                                                                                   @(SNCharacteristicTypePasswordStatus): [NSValue valueWithRange:NSMakeRange(0, 1)]},
                                        @"569A1005-B87F-490C-92CB-11BA5EA5167C": @{@(SNCharacteristicTypeLog): [NSValue valueWithRange:NSMakeRange(0, 20)]},
                                        @"569A1006-B87F-490C-92CB-11BA5EA5167C": @{@(SNCharacteristicTypeCommand): [NSValue valueWithRange:NSMakeRange(0, 2)],
                                                                                   @(SNCharacteristicTypeBinaryData): [NSValue valueWithRange:NSMakeRange(0, 17)]},
                                        @"569A1007-B87F-490C-92CB-11BA5EA5167C": @{@(SNCharacteristicTypePasswordData): [NSValue valueWithRange:NSMakeRange(0, 16)]}
                                        };
        
        self.bitrangeForCharacteristic = @{@(SNCharacteristicTypeLightsStatus): [NSValue valueWithRange:NSMakeRange(0, 1)],
                                           @(SNCharacteristicTypeWheelDirection): [NSValue valueWithRange:NSMakeRange(7, 1)]};
    }
    return self;
}

+ (SNBLEManager *)sharedManager {
    static SNBLEManager *manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        manager = [[SNBLEManager alloc] init];
    });
    return manager;
}

- (CBCentralManagerState)bleState
{
    return self.bluetoothCentralManager.state;
}

#pragma mark - Discover

- (NSArray *)discoveredDevices
{
    NSMutableArray *devices = [[NSMutableArray alloc] init];
    for (CBPeripheral *peripheral in self.discoveredPeripheralArray)
        [devices addObject:@{kDeviceName: peripheral.name.length ? peripheral.name : @"No name",
                             kDeviceUUID: [peripheral.identifier UUIDString]}];
    return devices;
}

- (NSDictionary *)connectedDevice
{
    if (!self.connectPeripheral || self.connectPeripheral.state != CBPeripheralStateConnected)
        return nil;
    return @{kDeviceName: self.connectPeripheral.name.length ? self.connectPeripheral.name : @"No name",
             kDeviceUUID: [self.connectPeripheral.identifier UUIDString]};
}

- (void)startScan
{
    [self stopScan];
    [self.discoveredPeripheralArray removeAllObjects];
    [self scanForTimer];
    self.scanTimer = [NSTimer scheduledTimerWithTimeInterval:SCAN_INTERVAL_SEC target:self selector:@selector(scanForTimer) userInfo:nil repeats:YES];
}

- (void)scanForTimer
{
    [self.bluetoothCentralManager stopScan];
    [self.bluetoothCentralManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)stopScan
{
    [self.scanTimer invalidate];
    [self.bluetoothCentralManager stopScan];
}

#pragma mark - Connecting

- (BOOL)connectToUUID:(NSString *)uuidString
{
    if (self.connectPeripheral) {
        return NO;
    }
    for (CBPeripheral *peripheral in self.discoveredPeripheralArray)
        if ([[peripheral.identifier UUIDString] isEqualToString:uuidString])
        {
            self.connectPeripheral = peripheral;
            break;
        }
    if (self.connectPeripheral && self.connectPeripheral.state == CBPeripheralStateDisconnected) {
        NSLog(@"CONNECTING %@",uuidString);
        [self.bluetoothCentralManager connectPeripheral:self.connectPeripheral options:nil];
        return YES;
    }
    
    NSArray *recentConnections = [self.bluetoothCentralManager retrievePeripheralsWithIdentifiers:@[[[NSUUID alloc] initWithUUIDString:uuidString]]];
    self.connectPeripheral = [recentConnections firstObject];
    if (self.connectPeripheral && self.connectPeripheral.state == CBPeripheralStateDisconnected) {
        NSLog(@"CONNECTING %@",uuidString);
        [self.bluetoothCentralManager connectPeripheral:self.connectPeripheral options:nil];
        return YES;
    }
    
    NSArray *connectedPeripherals = [self.bluetoothCentralManager retrieveConnectedPeripheralsWithServices:@[[[NSUUID alloc] initWithUUIDString:uuidString]]];
    self.connectPeripheral = [connectedPeripherals firstObject];
    if (self.connectPeripheral) {
        NSLog(@"CONNECTING %@",uuidString);
        [self.bluetoothCentralManager connectPeripheral:self.connectPeripheral options:nil];
        return YES;
    }
    
    [self.bluetoothCentralManager scanForPeripheralsWithServices:nil options:nil];
    return NO;
}

- (BOOL)disconnect
{
    if (self.connectPeripheral) {
        if (self.connectPeripheral.state == CBPeripheralStateDisconnected) {
            [self setConnectPeripheral:nil];
            return NO;
        }
        [self.bluetoothCentralManager cancelPeripheralConnection:self.connectPeripheral];
        return YES;
    }
    return NO;
}

#pragma mark - Writing Values

- (void)writeData:(NSData *)data forConnectedDeviceCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteCompleteBlock)completeBlock
{
    if (!self.connectPeripheral || self.connectPeripheral.state != CBPeripheralStateConnected)
    {
        if (completeBlock)
            completeBlock([NSError errorWithDomain:kErrorDomainWriting code:0 userInfo:@{NSLocalizedDescriptionKey: @"No connected device"}]);
        return;
    }
    CBService *service = [self serviceForPeripheral:self.connectPeripheral withUUIDString:kServiceUUID];
    if (!service)
    {
        if (completeBlock)
            completeBlock([NSError errorWithDomain:kErrorDomainWriting code:1 userInfo:@{NSLocalizedDescriptionKey: @"Service is not available"}]);
        return;
    }
    CBCharacteristic *characteristic = [self characteristicForService:service withCharacteristicType:characteristicType];
    if (!characteristic)
    {
        if (completeBlock)
            completeBlock([NSError errorWithDomain:kErrorDomainWriting code:2 userInfo:@{NSLocalizedDescriptionKey: @"UndefinedCharacteristic"}]);
        return;
    }
    [self.writingQueue addWriteInvokation:[[SNWriteInvokation alloc] initWithPeripheral:self.connectPeripheral
                                                                         characteristic:characteristic
                                                                                   data:data
                                                                        writeCompletion:completeBlock]];
}

- (NSData *)dataFromInteger:(NSInteger)integerValue
{
    NSInteger value = integerValue;
    NSUInteger bytesCount = sizeof(value);
    NSMutableData *data = [[NSMutableData alloc] init];
    NSInteger byteOffset = bytesCount - 1;
    while (((value >> (8 * byteOffset)) & 255) == 0) {
        byteOffset--;
    }
    for (NSInteger byteOffsetToWrite = byteOffset; byteOffsetToWrite >= 0; byteOffsetToWrite --) {
        unsigned char byteToAppend = (value >> (8 * byteOffsetToWrite) & 255);
        [data appendBytes:&byteToAppend length:sizeof(byteToAppend)];
    }
    return [data copy];
}

- (NSData *)commandDataForBTCommand:(SNBTCommand)command
{
    if ([self.checksumCommands containsObject:@(command)]) {
        NSData *commandData = [self dataFromInteger:[self.commandValueForCommand[@(command)] integerValue]];
        unsigned char checksum = [SNChecksum checksum:commandData];
        NSMutableData *resultData = [commandData mutableCopy];
        [resultData appendBytes:&checksum length:1];
        return [resultData copy];
    }
    return [self dataFromInteger:[self.commandValueForCommand[@(command)] integerValue]];
}

- (NSData *)commandDataForCharacteristic:(SNCharacteristicType)characteristicType
{
    return [self dataFromInteger:[self.commandValueForCharacteristic[@(characteristicType)] integerValue]];
}

- (void)writeRequestValueForCharacteristic:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock
{
    [self writeData:[self commandDataForCharacteristic:characteristicType] forConnectedDeviceCharacteristicType:SNCharacteristicTypeCommand complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error, 0);
            }
            return ;
        }
        [self expectValueWithHeader:[self.commandValueForCharacteristic[@(characteristicType)] doubleValue] forCharacterictic:characteristicType complete:completeBlock];
    }];
}

- (void)writeRequestStringForCharacteristic:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock
{
    [self writeData:[self commandDataForCharacteristic:characteristicType] forConnectedDeviceCharacteristicType:SNCharacteristicTypeCommand complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error, nil);
            }
            return ;
        }
        [self expectStringWithHeader:[self.commandValueForCharacteristic[@(characteristicType)] doubleValue] forCharacterictic:characteristicType complete:completeBlock];
    }];
}

- (void)writePasswordData:(NSData *)data complete:(WriteCompleteBlock)completeBlock
{
    [self writeData:data forConnectedDeviceCharacteristicType:SNCharacteristicTypePasswordData complete:completeBlock];
}

- (void)writeBinaryData:(NSData *)data complete:(WriteCompleteBlock)completeBlock
{
    [self writeBinaryData:data isChecksumNeeded:YES complete:completeBlock];
}

- (void)writeBinaryData:(NSData *)data isChecksumNeeded:(BOOL)isChecksum complete:(WriteCompleteBlock)completeBlock
{
    [self writeBinaryData:data isChecksumNeeded:isChecksum isConfirmNeeded:YES complete:completeBlock];
}

- (void)writeBinaryData:(NSData *)data isChecksumNeeded:(BOOL)isChecksum isConfirmNeeded:(BOOL)isConfirmNeeded complete:(WriteCompleteBlock)completeBlock
{
    NSMutableData *dataToWrite = [data mutableCopy];
    if (isChecksum) {
        unsigned char checksum = [SNChecksum checksum:data];
        [dataToWrite appendBytes:&checksum length:1];
    }
    [self writeBinaryData:[dataToWrite copy] offset:0 isConfirmNeeded:isConfirmNeeded complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        if (completeBlock) {
            completeBlock(nil);
        }
    }];
}

- (void)writeBinaryData:(NSData *)data offset:(NSUInteger)offset isConfirmNeeded:(BOOL)isConfirmNeeded complete:(WriteCompleteBlock)completeBlock
{
    if (offset >= data.length) {
        if (completeBlock) {
            completeBlock(nil);
        }
        return;
    }
    [self writeData:[data subdataWithRange:NSMakeRange(offset, MIN(20, data.length - offset))] forConnectedDeviceCharacteristicType:SNCharacteristicTypeBinaryData complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        if (isConfirmNeeded) {
            [self expectValue:0x79 forCharacterictic:SNCharacteristicTypeCommandConfirm complete:^(NSError *error) {
                if (error) {
                    if (completeBlock) {
                        completeBlock(error);
                    }
                    return ;
                }
                [self writeBinaryData:data offset:offset + MIN(20, data.length - offset) isConfirmNeeded:isConfirmNeeded complete:completeBlock];
            }];
        }
        else {
            [self writeBinaryData:data offset:offset + MIN(20, data.length - offset) isConfirmNeeded:isConfirmNeeded complete:completeBlock];
        }
    }];
}

#pragma mark - Writing Commands

- (void)writeCommand:(SNBTCommand)command complete:(WriteCompleteBlock)completeBlock
{
    [self writeCommand:command withPayload:nil complete:completeBlock];
}

- (void)writeCommand:(SNBTCommand)command withPayload:(NSData *)payloadData complete:(WriteCompleteBlock)completeBlock
{
    [self writeCommand:command withPayload:payloadData complete:completeBlock isTimeout:YES];
}

- (void)writeCommand:(SNBTCommand)command withPayload:(NSData *)payloadData complete:(WriteCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    [self writeCommand:command withPayload:payloadData isConfirmNeeded:YES complete:completeBlock isTimeout:isTimeout];
}

- (void)writeCommand:(SNBTCommand)command withPayload:(NSData *)payloadData isConfirmNeeded:(BOOL)isConfirmNeeded complete:(WriteCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    NSData *commandData = [self commandDataForBTCommand:command];
    NSMutableData *dataToWrite = [commandData mutableCopy];
    if (payloadData) {
        [dataToWrite appendData:payloadData];
    }
    [self writeData:[dataToWrite copy] forConnectedDeviceCharacteristicType:SNCharacteristicTypeCommand complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        if (isConfirmNeeded) {
            [self expectValue:[self.commandValueForCommand[@(command)] doubleValue] forCharacterictic:SNCharacteristicTypeCommandConfirm complete:completeBlock isTimeout:isTimeout];
        }
        else {
            if (completeBlock) {
                completeBlock(nil);
            }
        }
    }];
}

- (void)writeCommand:(SNBTCommand)command withValueResponsesCount:(NSInteger)count inCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock
{
    [self writeCommand:command withValueResponsesCount:count inCharacteristicType:characteristicType complete:completeBlock isTimeout:YES];
}

- (void)writeCommand:(SNBTCommand)command withValueResponsesCount:(NSInteger)count inCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    [self writeData:[self commandDataForBTCommand:command] forConnectedDeviceCharacteristicType:SNCharacteristicTypeCommand complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error, 0);
            }
            return ;
        }
        for (NSInteger iterator = 0; iterator < count; iterator ++) {
            [self expectValueForCharacterictic:characteristicType complete:completeBlock useTimeout:isTimeout];
        }
    }];
}

- (void)writeCommand:(SNBTCommand)command withStringResponsesCount:(NSInteger)count inCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock
{
    [self writeCommand:command withStringResponsesCount:count inCharacteristicType:characteristicType complete:completeBlock isTimeout:YES];
}

- (void)writeCommand:(SNBTCommand)command withStringResponsesCount:(NSInteger)count inCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    [self writeData:[self commandDataForBTCommand:command] forConnectedDeviceCharacteristicType:SNCharacteristicTypeCommand complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error, nil);
            }
            return ;
        }
        for (NSInteger iterator = 0; iterator < count; iterator ++) {
            [self expectStringForCharacterictic:characteristicType complete:completeBlock useTimeout:isTimeout];
        }
    }];
}

- (void)writeCommand:(SNBTCommand)command withValueResponseInCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock
{
    [self writeCommand:command withValueResponsesCount:1 inCharacteristicType:characteristicType complete:completeBlock];
}

- (void)writeCommand:(SNBTCommand)command withValueResponseInCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    [self writeCommand:command withValueResponsesCount:1 inCharacteristicType:characteristicType complete:completeBlock isTimeout:isTimeout];
}

- (void)writeCommand:(SNBTCommand)command withStringResponseInCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock
{
    [self writeCommand:command withStringResponsesCount:1 inCharacteristicType:characteristicType complete:completeBlock];
}

- (void)writeCommand:(SNBTCommand)command withStringResponseInCharacteristicType:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    [self writeCommand:command withStringResponsesCount:1 inCharacteristicType:characteristicType complete:completeBlock isTimeout:isTimeout];
}

#pragma mark - Expectations

- (void)expectValue:(double)value forCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteCompleteBlock)completeBlock
{
    [self expectValue:value forCharacterictic:characteristicType complete:completeBlock isTimeout:YES];
}

- (void)expectValue:(double)value forCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    [self.expectationQueue addExpectation:[[SNSpecificValueExpectation alloc] initWithCharacteristic:characteristicType
                                                                                            isSigned:[self.signedCharacteristics containsObject:@(characteristicType)]
                                                                                               value:value
                                                                                       completeBlock:completeBlock
                                                                                          useTimeout:isTimeout]];
}

- (void)expectValueWithHeader:(double)headerValue forCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock
{
    [self expectValueWithHeader:headerValue
              forCharacterictic:characteristicType
                       complete:completeBlock
                      isTimeout:YES];
}

- (void)expectValueWithHeader:(double)headerValue forCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    [self.expectationQueue addExpectation:[[SNHeaderValueExpectation alloc] initWithCharacteristic:characteristicType
                                                                                          isSigned:[self.signedCharacteristics containsObject:@(characteristicType)]
                                                                                       headerValue:headerValue
                                                                                     completeBlock:completeBlock
                                                                                        useTimeout:isTimeout]];
}

- (void)expectStringWithHeader:(double)headerValue forCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock
{
    [self expectStringWithHeader:headerValue
               forCharacterictic:characteristicType
                        complete:completeBlock
                       isTimeout:YES];
}

- (void)expectStringWithHeader:(double)headerValue forCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock isTimeout:(BOOL)isTimeout
{
    [self.expectationQueue addExpectation:[[SNHeaderStringExpectation alloc] initWithCharacteristic:characteristicType
                                                                                        headerValue:headerValue
                                                                                      completeBlock:completeBlock
                                                                                         useTimeout:isTimeout]];
}

- (void)expectValueForCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock
{
    [self expectValueForCharacterictic:characteristicType
                              complete:completeBlock
                            useTimeout:YES];
}

- (void)expectValueForCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithValueResponseCompleteBlock)completeBlock useTimeout:(BOOL)isTimeout
{
    [self.expectationQueue addExpectation:[[SNValueExpectation alloc] initWithCharacteristic:characteristicType
                                                                                    isSigned:[self.signedCharacteristics containsObject:@(characteristicType)]
                                                                                  valueBlock:completeBlock
                                                                                  useTimeout:isTimeout]];
}

- (void)expectStringForCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock
{
    [self expectStringForCharacterictic:characteristicType
                               complete:completeBlock
                             useTimeout:YES];
}

- (void)expectStringForCharacterictic:(SNCharacteristicType)characteristicType complete:(WriteWithStringResponseCompleteBlock)completeBlock useTimeout:(BOOL)isTimeout
{
    [self.expectationQueue addExpectation:[[SNStringExpectation alloc] initWithCharacteristic:characteristicType
                                                                                  stringBlock:completeBlock
                                                                                   useTimeout:isTimeout]];
}

#pragma Storing Data

- (void)addDiscoveredPeripheral:(CBPeripheral *)peripheral
{
    BOOL isDublicate = NO;
    for (CBPeripheral *savedPeripheral in self.discoveredPeripheralArray)
        if ([[savedPeripheral.identifier UUIDString] isEqualToString:[peripheral.identifier UUIDString]])
        {
            isDublicate = YES;
            break;
        }
    if (!isDublicate)
    {
        [self.discoveredPeripheralArray addObject:peripheral];
        [self.connectionDelegate deviceListUpdated];
    }
}

- (CBService *)serviceForPeripheral:(CBPeripheral *)peripheral withUUIDString:(NSString *)uuidString
{
    for (CBService *service in peripheral.services)
        if ([[service.UUID UUIDString] isEqualToString:uuidString])
            return service;
    return nil;
}

- (CBCharacteristic *)characteristicForService:(CBService *)service withCharacteristicType:(SNCharacteristicType)type
{
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        NSDictionary *rangesForTypes = self.characteristicsForUUID[[characteristic.UUID UUIDString]];
        if ([[rangesForTypes allKeys] containsObject:@(type)])
            return characteristic;
    }
    return nil;
}

- (NSDictionary *)valuesAndTypesFromData:(NSData *)data withUUID:(NSString *)uuidString
{
    NSMutableDictionary *valuesForType = [[NSMutableDictionary alloc] init];
    NSDictionary *rangeForCharactertistic = self.characteristicsForUUID[uuidString];
    for (NSNumber *key in [rangeForCharactertistic allKeys])
    {
        BOOL isCharacteristicForExpectation = ([self.responseCharacteristics containsObject:key] || [[self.commandValueForCharacteristic allKeys] containsObject:key]);
        NSRange characteristicRangeInData = [rangeForCharactertistic[key] rangeValue];
        if (isCharacteristicForExpectation && !([self.expectationQueue isExpectForCharacteristic:(SNCharacteristicType)[key integerValue]]))
            continue;
        if (!isCharacteristicForExpectation && characteristicRangeInData.location + characteristicRangeInData.length > [data length])
            continue;
        characteristicRangeInData.length = MIN(characteristicRangeInData.length, [data length] - characteristicRangeInData.location);
        
        NSData *characteristicsData = [data subdataWithRange:characteristicRangeInData];
        if (isCharacteristicForExpectation) {
            [self.expectationQueue invokeExpectationForCharacteristicType:(SNCharacteristicType)[key integerValue] withBinaryData:characteristicsData error:nil];
            continue;
        }
        NSInteger integerValueOfBytes;
        unsigned char *dataBytes = malloc(MAX(characteristicRangeInData.length, sizeof(integerValueOfBytes)));
        bzero(dataBytes, sizeof(dataBytes));
        [data getBytes:dataBytes range:characteristicRangeInData];
        if (self.bitrangeForCharacteristic[key]) {
            NSRange bitRange = [self.bitrangeForCharacteristic[key] rangeValue];
            NSUInteger numericValueOfBits = 0;
            for (NSInteger byteIndex = 0; byteIndex < characteristicRangeInData.length; byteIndex ++) {
                numericValueOfBits = numericValueOfBits << (sizeof(dataBytes[0]) * 8);
                numericValueOfBits = numericValueOfBits | dataBytes[byteIndex];
            }
            integerValueOfBytes = (numericValueOfBits & (((1 << bitRange.length) - 1) << (characteristicRangeInData.length * 8 - bitRange.location - bitRange.length))) >> (characteristicRangeInData.length * 8 - bitRange.location - bitRange.length);
        } else {
            if ([self.signedCharacteristics containsObject:key]) {
                integerValueOfBytes = [SNDataToIntegerConverter signedIntegerFromData:[data subdataWithRange:characteristicRangeInData]];
            }
            else {
                integerValueOfBytes = [SNDataToIntegerConverter unsignedIntegerFromData:[data subdataWithRange:characteristicRangeInData]];
            }
        }
        free(dataBytes);
        valuesForType[key] = @(integerValueOfBytes);
    }
    return valuesForType;
}

#pragma mark - Central Delegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    // TODO: send message smbdy
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI
{
    if (!advertisementData[CBAdvertisementDataServiceUUIDsKey])
        return;
    for (CBUUID *serviceUUID in advertisementData[CBAdvertisementDataServiceUUIDsKey])
        if ([[serviceUUID UUIDString] isEqualToString:kServiceUUID])
            [self addDiscoveredPeripheral:peripheral];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"PERIPHERAL CONNECTED");
    [self.bluetoothCentralManager stopScan];
    [self.connectPeripheral setDelegate:self];
    [self.connectPeripheral discoverServices:@[[CBUUID UUIDWithString:kServiceUUID]]];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"PERIPHERAL CONNECT FAIL, error: %@",error.localizedDescription);
    [self.connectionDelegate deviceDisconnected];
    [self setConnectPeripheral:nil];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"PERIPHERAL DISCONNECTED, error: %@",error.localizedDescription);
    [self.connectionDelegate deviceDisconnected];
    [self setConnectPeripheral:nil];
    [self.writingQueue finishAllInvokationsWithError:[NSError errorWithDomain:@"BLE" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Device disconnected"}]];
    for (SNCharacteristicType type = 0; type < SNCharacteristicTypeCount; type ++)
        [self.dataDelegate updateValue:nil forCharacteristicType:type];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    CBService *service = [self serviceForPeripheral:peripheral withUUIDString:kServiceUUID];
    if (service)
        [peripheral discoverCharacteristics:nil forService:service];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        [peripheral readValueForCharacteristic:characteristic];
        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
    }
    [self.connectionDelegate deviceConnectedWithUUID:[peripheral.identifier UUIDString] name:peripheral.name.length ? peripheral.name : @"No name"];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    //TODO : HANDLE ERROR WHEN SUBSCRIBE
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if ([characteristic.UUID.UUIDString rangeOfString:@"569A1000"].location == NSNotFound && [characteristic.UUID.UUIDString rangeOfString:@"569A1001"].location == NSNotFound && [characteristic.UUID.UUIDString rangeOfString:@"569A1002"].location == NSNotFound) {
//        if ([characteristic.UUID.UUIDString isEqualToString:@"569A1005-B87F-490C-92CB-11BA5EA5167C"]) {
//            NSLog(@"LOG: %@ BYTE: %@",[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding], characteristic.value);
//        }
//        else {
            NSLog(@"get ch:%@ val:%@",characteristic.UUID.UUIDString, characteristic.value);
//        }
    }
    NSDictionary *valueForTypes = [self valuesAndTypesFromData:characteristic.value withUUID:characteristic.UUID.UUIDString];
    for (NSNumber *type in [valueForTypes allKeys])
        [self.dataDelegate updateValue:valueForTypes[type] forCharacteristicType:(SNCharacteristicType)[type integerValue]];
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSLog(@"set ch:%@ confirm:%@", characteristic.UUID.UUIDString, @(!error));
    [self.writingQueue finishInvokationWithError:error];
}

@end
