//
//  SNSegmentView.h
//  Spinar
//
//  Created by Duhov Filipp on 7/20/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface SNSegmentView : UIView

@property (nonatomic, assign) IBInspectable CGFloat startDegree;
@property (nonatomic, assign) IBInspectable CGFloat endDegree;
@property (nonatomic, assign) IBInspectable CGFloat segmentThickness;
@property (nonatomic, strong) IBInspectable UIColor *segmentColor;

@property (nonatomic, assign) IBInspectable CGFloat progress;

@end
