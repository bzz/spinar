//
//  SNStatistics.h
//  Spinar
//
//  Created by Hena Sofronov on 4/19/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNStatistics : NSObject

+ (instancetype)sharedInstance;

- (void)resetStatistics;

- (void)startWriteInvokation;
- (void)finishWriteInvokation;

- (void)startDataWriteInvokation;
- (void)finishDataWriteInvokation;

@end
