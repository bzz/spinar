//
//  SNSettingsViewController.h
//  Spinar
//
//  Created by User on 10/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNSettingsViewController : UITableViewController <UIAlertViewDelegate>

@end
