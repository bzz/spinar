//
//  SNTabBarController.h
//  Spinar
//
//  Created by Duhov Filipp on 7/17/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNTabBarController : UITabBarController

@end
