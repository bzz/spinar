//
//  SNSettings.m
//  Spinar
//
//  Created by User on 10/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "SNSettings.h"
#import "UIColorFromRGB.h"

static NSString * const kBoolDisplayingUnitsMiles = @"areDisplayingUnitsMiles";

@implementation SNSettings

//+ (NSDictionary *)colorsForError {
//    return @{[NSValue valueWithRange:NSMakeRange(0, 2)]: [UIColor clearColor],
//             [NSValue valueWithRange:NSMakeRange(2, 49)]: [UIColor redColor],
//             [NSValue valueWithRange:NSMakeRange(51, 205)]: UIColorFromRGB(62, 186, 231)};
//}

+ (SNSettings *)sharedSettings {
    static SNSettings *settings = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        settings = [[SNSettings alloc] init];
    });
    return settings;
}

+ (NSString *) stringForDistanceUnits:(BOOL)areDisplayingUnitsMiles {
    return areDisplayingUnitsMiles? @"miles":@"km";
}

+ (NSString *) stringForSpeedUnits:(BOOL)areDisplayingUnitsMiles {
    return areDisplayingUnitsMiles? @"mph": @"km/h";
}

+ (NSString *)stringForTimeUnitsHours {
    return @"hrs";
}

+ (NSString *)stringForTimeUnitsMinutes {
    return @"min";
}

+ (UIColor *)colorForErrorCode:(NSUInteger)errorCode {
    NSUInteger redBits = 0x002CFFFF;
    NSUInteger yellowBits = 0x00130000;
    if (errorCode & redBits) {
        return [UIColor redColor];
    }
    if (errorCode & yellowBits) {
        return [UIColor yellowColor];
    }
    return [UIColor clearColor];
}

+ (NSString *)shortDescriptionForErrorCode:(NSUInteger)errorCode
{
    NSUInteger redBits = 0x002CFFFF;
    NSUInteger yellowBits = 0x00130000;
    if (errorCode & redBits) {
        return @"Error";
    }
    if (errorCode & yellowBits) {
        return @"Warning";
    }
    return @"Ready";
}

+ (NSString *)descriptionForErrorCode:(NSUInteger)errorCode
{
    NSArray *descriptionsForBits = @[
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     
                                     @"Current is over the threshold.",
                                     @"Voltage is under the threshold.",
                                     @"Voltage is over the threshold.",
                                     @"Voltage 12V from Bluetooth board is under the threshold.",
                                     @"Voltage 12V from Motor is under the threshold.",
                                     @"Battery Management System battery discharge or FAULT detected.",
                                     @"Temperature MOSFET is over the threshold.",
                                     @"Temperature Motor PTC is over the threshold.",
                                     
                                     @"Wheel fall detected.",
                                     @"Wheel pickup detected.",
                                     @"MainMCU disconnected from MotorMCU.",
                                     @"IR Proximity sensor communication fault.",
                                     @"User presense.",
                                     @"BLE disconnected from MainMCU.",
                                     @"",
                                     @"",

                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     @"",
                                     ];
    
    NSMutableString *result = [[NSMutableString alloc] init];
    for (NSUInteger index = 0; index < descriptionsForBits.count; index++) {
        if (errorCode & (1 << index)) {
            [result appendString:[NSString stringWithFormat:@"%@ ",descriptionsForBits[index]]];
        }
    }
    return result;
}

- (BOOL)areDisplayingUnitsMiles {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kBoolDisplayingUnitsMiles];
}

- (void)setAreDisplayingUnitsMiles:(BOOL)isDisplayingUnitsMiles {
    [[NSUserDefaults standardUserDefaults] setBool:isDisplayingUnitsMiles forKey:kBoolDisplayingUnitsMiles];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
