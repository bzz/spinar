//
//  SNRadialView.h
//  Spinar
//
//  Created by hena on 10/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "MDRadialProgressView.h"

@interface SNRadialView : MDRadialProgressView
/**
 *  Radial View Instance with defined full angle. If full angle is less then 2*pi we have an arc with symmetry related to vertical.
 *
 *  @param frame     view frame
 *  @param theme     color and style theme
 *  @param fullAngle full angle of radial view in radians
 *
 *  @return Radial View instance
 */
- (id)initWithFrame:(CGRect)frame andTheme:(MDRadialProgressTheme *)theme fullAngle:(double)fullAngle;

/**
 *  filled part of the Radial View. Value from 0.0 to 1.0 
 */
@property (assign, nonatomic) double progress;
@end
