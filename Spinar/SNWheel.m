//
//  SNWheel.m
//  Spinar
//
//  Created by hena on 10/23/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "SNWheel.h"
#import "SNBLEManager.h"
#import "SNSettings.h"
#import "SNConvertor.h"
#import "UIAlertView+SNMessage.h"
#import "SNFirmwareUpdater.h"
#import "SNDataToIntegerConverter.h"

double const kWheelMaxSpeed = 25;
double const kWheelMaxAbsPitch = 20;
double const kWheelMaxTemperature = 50;
double const kWheelMaxPowerUsage = 100;
double const kWheelMinBatteryLife = 490;
double const kWheelMidBatteryLife = 522;
double const kWheelMaxBatteryLife = 582;
double const kWheelMaxRange = 12.5;

@interface SNWheel()<SNBLEManagerDataDelegate>
@property (strong, nonatomic) NSMutableDictionary *info;

@end


@implementation SNWheel

- (id)init
{
    self = [super init];
    if (self)
    {
        _info = [[NSMutableDictionary alloc] init];
        [[SNBLEManager sharedManager] setDataDelegate:self];
    }
    return self;
}


+ (SNWheel *)currentWheel {
    static SNWheel *manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        manager = [[SNWheel alloc] init];
    });
    return manager;
}

#pragma mark - SNBLEManagerDataDelegate

- (void)resetData
{
    _info = [[NSMutableDictionary alloc] init];
    _journeyTime = 0;
    _journeyDistance = 0;
    _maxSpeed = 0;
    _totalTime = 0;
    _totalDistance = 0;
    _wheelId = nil;
    _wheelLocked = NO;
    _ridingMode = SNRidingModeComfort;
    _safetyOn = NO;
    _learningOn = NO;
    _maxSpeed = 0;
    _totalTime = 0;
    _totalDistance = 0;
    _journeyTime =0;
    _journeyDistance = 0;
    _firmwareVersionBT.major = 0;
    _firmwareVersionBT.minor = 0;
    _firmwareVersionBT.build = 0;
    _firmwareVersionWheel.major = 0;
    _firmwareVersionWheel.minor = 0;
    _firmwareVersionWheel.build = 0;
    _firmwareVersionBTBootloader.major = 0;
    _firmwareVersionBTBootloader.minor = 0;
    _firmwareVersionBTBootloader.build = 0;
    _firmwareVersionWheelBootloader.major = 0;
    _firmwareVersionWheelBootloader.minor = 0;
    _firmwareVersionWheelBootloader.build = 0;
    _firmwareVersionBL.major = 0;
    _firmwareVersionBL.minor = 0;
    _firmwareVersionBL.build = 0;
    _state = SNWheelStateReady;
    [self.delegate didUpdateProperties];
}

- (void)updateValue:(id)value forCharacteristicType:(SNCharacteristicType)characteristicType
{
    if (!value)
        [self.info removeObjectForKey:@(characteristicType)];
    else
        [self.info setObject:value forKey:@(characteristicType)];
    [self.delegate didUpdateProperties];
}

#pragma mark - Live Data

- (double)speed
{
    double speed = (double)[self.info[@(SNCharacteristicTypeWheelSpeed)] integerValue] * 0.0272;
    return speed;
}

- (double)batteryLife
{
    double voltage = [self.info[@(SNCharacteristicTypeBatteryVoltage)] doubleValue];
    if (voltage > kWheelMaxBatteryLife) {
        return 1;
    }
    if (voltage < kWheelMinBatteryLife) {
        return 0;
    }
    if (voltage > kWheelMidBatteryLife) {
        return (0.0833 * voltage + 51.5)/100;
    }
    return (2.9688 * voltage - 1454.7)/100;
}

- (double)distanceTraveled
{
    double km = ([self.info[@(SNCharacteristicTypeDistanceTraveledPositive)] integerValue] + [self.info[@(SNCharacteristicTypeDistanceTraveledNegative)] integerValue]) * 0.0006;
    return km;
}

- (double)timeTraveled
{
    return [self.info[@(SNCharacteristicTypeTimeSinceOn)] doubleValue] / 4 / 60 / 60;
}

- (double)pitch
{
    if (!self.info[@(SNCharacteristicTypeWheelPitch)])
        return 0;
    return [self.info[@(SNCharacteristicTypeWheelPitch)] doubleValue] / 1000 - 180;
}

- (double)temperature
{
    return [self.info[@(SNCharacteristicTypeTemperatureGyro)] doubleValue];
}

- (NSUInteger)errorCode
{
    return (NSUInteger)[self.info[@(SNCharacteristicTypeErrorCode)] integerValue];
}

- (double)voltage
{
    return [self.info[@(SNCharacteristicTypeBatteryVoltage)] doubleValue] / 10;
}

- (double)motorTorque
{
    return fabs([self.info[@(SNCharacteristicTypeTorque)] doubleValue])/250;
}

#pragma mark - Calculated Data

- (double)distanceEstimate
{
    double voltage = [self.info[@(SNCharacteristicTypeBatteryVoltage)] doubleValue];
    if (voltage > kWheelMaxBatteryLife) {
        return kWheelMaxRange;
    }
    if (voltage < kWheelMinBatteryLife) {
        return 0;
    }
    if (voltage > kWheelMidBatteryLife) {
        return 0.01667 * voltage + 2.3;
    }
    return 0.34375 * voltage - 168.44;
}

#pragma mark - Getting Non-Live Data

- (void)updatefirmwareVersionBTWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeFirmwareVersionBT complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _firmwareVersionBT.major = ((NSUInteger)value) & 0xFF;
        _firmwareVersionBT.minor = ((((NSUInteger)value) & 0xFF00) >> 8) & 0xFF;
        _firmwareVersionBT.build = ((((NSUInteger)value) & 0xFFFF0000) >> 16) & 0xFFFF;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)updatefirmwareVersionWheelWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeFirmwareVersionWheel complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _firmwareVersionWheel.major = ((NSUInteger)value) & 0xFF;
        _firmwareVersionWheel.minor = ((((NSUInteger)value) & 0xFF00) >> 8) & 0xFF;
        _firmwareVersionWheel.build = ((((NSUInteger)value) & 0xFFFF0000) >> 16) & 0xFFFF;
        if (completeBlock)
            completeBlock(error);
    }];
    
}

- (void)updatefirmwareVersionBLWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeFirmwareVersionBL complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _firmwareVersionBL.major = ((NSUInteger)value) & 0xFF;
        _firmwareVersionBL.minor = ((((NSUInteger)value) & 0xFF00) >> 8) & 0xFF;
        _firmwareVersionBL.build = ((((NSUInteger)value) & 0xFFFF0000) >> 16) & 0xFFFF;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)updatefirmwareVersionBTBootloaderWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandGetBootloaderVersion
                       withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          NSUInteger intValue = (NSUInteger)value;
                                          unsigned char firstByte = intValue & 0xFF;
                                          unsigned char secondByte = (intValue >> 8) & 0xFF;
                                          unsigned char thirdByte = (intValue >> 16) & 0xFF;
                                          unsigned char fourthByte = (intValue >> 24) & 0xFF;
                                          
                                          if (firstByte != 0x79 || fourthByte != 0x79) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Bootloader incorrect response"}]);
                                              }
                                              return;
                                          }
                                          _firmwareVersionBTBootloader.major = secondByte;
                                          _firmwareVersionBTBootloader.minor = thirdByte;
                                          if (completeBlock) {
                                              completeBlock(nil);
                                          }
                                      }];
}

- (void)updatefirmwareVersionWheelBootloaderWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandGetBootloaderVersion
                       withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          NSUInteger intValue = (NSUInteger)value;
                                          unsigned char firstByte = intValue & 0xFF;
                                          unsigned char secondByte = (intValue >> 8) & 0xFF;
                                          unsigned char thirdByte = (intValue >> 16) & 0xFF;
                                          unsigned char fourthByte = (intValue >> 24) & 0xFF;
                                          
                                          if (firstByte != 0x79 || fourthByte != 0x79) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Bootloader incorrect response"}]);
                                              }
                                              return;
                                          }
                                          _firmwareVersionWheelBootloader.major = secondByte;
                                          _firmwareVersionWheelBootloader.minor = thirdByte;
                                          if (completeBlock) {
                                              completeBlock(nil);
                                          }
                                      }];
}

- (void)updateJourneyTimeWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeTotalUseTime complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _journeyTime =  value / 4 / 60 / 60;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)resetJourneyTimeWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandResetTotalTime complete:completeBlock];
}

- (void)updateJourneyDistanceWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeTotalDistance complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _journeyDistance = value * 0.0006;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)resetJourneyDistanceWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandResetTotalDistance complete:completeBlock];
}

- (void)updateMaxSpeedWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeMaxSpeed complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _maxSpeed = MIN(value * 0.0272, 22);
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)resetMaxSpeedWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandResetMaxSpeed complete:completeBlock];
}

- (void)updateTotalDistanceWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeTotalDistanceUnresettable complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _totalDistance = value * 0.0006;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)updateTotalTimeWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeTotalUseTimeUnresettable complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _totalTime =  value / 4 / 60 / 60;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)updateWheelIdWithCompleteBlock:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestStringForCharacteristic:SNCharacteristicTypeWheelID complete:^(NSError *error, NSString *string) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _wheelId =  string;
        if (completeBlock)
            completeBlock(error);
    }];
}

#pragma mark - Getting and Setting Wheel properties


- (void)setWheelLocked:(BOOL)isLocked withBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    unsigned char payload = (unsigned char)isLocked;
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetLockStatus withPayload:[NSData dataWithBytes:&payload length:1] complete:^(NSError *error) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _wheelLocked = isLocked;
        if (completeBlock)
            completeBlock(error);

    }];
}

- (void)updateWheelLocked:(WritingValueCompleteBlock)completeBlock {
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeLockStatus complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        if (!(value >= 0 && value <= 1)) {
            if (completeBlock)
                completeBlock([NSError errorWithDomain:@"Wheel Lock" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Invalid value"}]);
            return ;
        }
        _wheelLocked = value != 0;
        if (completeBlock)
            completeBlock(nil);
    }];
}

- (void)setLearningOn:(BOOL)isOn withBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    unsigned char payload = (unsigned char)isOn;
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetLearningMode withPayload:[NSData dataWithBytes:&payload length:1] complete:^(NSError *error) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _learningOn = isOn;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)updateLearningOnWithBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeLearningModeStatus complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        if (!(value >= 0 && value <= 1)) {
            if (completeBlock)
                completeBlock([NSError errorWithDomain:@"Learning On" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Invalid value"}]);
            return ;
        }
        _learningOn = value != 0;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)setSpeedLimit:(double)speedLimit withBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    unsigned char payload = ((NSUInteger)speedLimit) & 0xFF;
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetSpeedLimit withPayload:[NSData dataWithBytes:&payload length:1] complete:^(NSError *error) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _speedLimit = speedLimit;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)updateSpeedLimitWithBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeSpeedLimitStatus complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _speedLimit = value;
        if (completeBlock)
            completeBlock(error);
    }];

}

- (void)setRidingMode:(SNRidingMode)ridingMode withBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    unsigned char payload = (unsigned char)ridingMode;
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetWheelModeStatus withPayload:[NSData dataWithBytes:&payload length:1] complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        _ridingMode = ridingMode;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)updateRidingModeWithBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeWheelModeStatus complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        if (!(value >= SNRidingModeComfort && value <= SNRidingModeSport)) {
            if (completeBlock)
                completeBlock([NSError errorWithDomain:@"Riding Mode" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Invalid value"}]);
            return ;
        }
        _ridingMode = (SNRidingMode)value;
        if (completeBlock)
            completeBlock(nil);
    }];
}

- (void)setLightOn:(BOOL)isOn withBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    unsigned char payload = (unsigned char)isOn;
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetLightsStatus withPayload:[NSData dataWithBytes:&payload length:1] complete:^(NSError *error) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        if (completeBlock)
            completeBlock(error);

    }];
}

- (BOOL)isLightOn
{
    return [self.info[@(SNCharacteristicTypeLightsStatus)] boolValue];
}

- (void)setSafetyOn:(BOOL)isOn withBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    unsigned char payload = (unsigned char)isOn;
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetSafetyStatus withPayload:[NSData dataWithBytes:&payload length:1] complete:^(NSError *error) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        _safetyOn = isOn;
        if (completeBlock)
            completeBlock(error);

    }];
}

- (void)updateSafetyOnWithBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeRequestValueForCharacteristic:SNCharacteristicTypeSafetyStatus complete:^(NSError *error, double value) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        if (!(value >= 0 && value <= 1)) {
            if (completeBlock)
                completeBlock([NSError errorWithDomain:@"Safety Lock" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Invalid value"}]);
            return ;
        }
        _safetyOn = value != 0;
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)setDeviceName:(NSString *)name withBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    NSData *stringData = [name dataUsingEncoding:NSUTF8StringEncoding];
    if (stringData.length > 16) {
        stringData = [stringData subdataWithRange:NSMakeRange(0, 16)];
    }
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetDeviceName withPayload:stringData complete:^(NSError *error) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        if (completeBlock)
            completeBlock(error);
    }];
}

- (void)setResetSettingsWithBlock:(WritingValueCompleteBlock)completeBlock
{
    if (self.state != SNWheelStateReady) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Wheel" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Wheel is not ready"}]);
        }
        return;
    }
    unsigned char payload = (unsigned char)(0x07);
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetResetSettings withPayload:[NSData dataWithBytes:&payload length:1] complete:^(NSError *error) {
        if (error) {
            if (completeBlock)
                completeBlock(error);
            return ;
        }
        if (completeBlock)
            completeBlock(error);
        
    }];
}

#pragma mark - Password

- (void)writePassword:(NSData *)data withCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writePasswordData:data complete:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        [[SNBLEManager sharedManager] expectValueForCharacterictic:SNCharacteristicTypePasswordStatus complete:^(NSError *error, double value) {
            if (error) {
                if (completeBlock) {
                    completeBlock(error);
                }
                return ;
            }
            if (value != 0xF1 && value != 0xF2 && value != 0xF0) {
                if (completeBlock) {
                    completeBlock([NSError errorWithDomain:@"Password" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Incorrect password"}]);
                }
                return;
            }
            if (value == 0xF2) {
                [[SNFirmwareUpdater sharedUpdater] continueLoadingAfterReboot];
            }
            if (value == 0xF0) {
                [[SNBLEManager sharedManager] expectValueForCharacterictic:SNCharacteristicTypePasswordStatus complete:^(NSError *error, double value) {
                    if (error) {
                        if (completeBlock) {
                            completeBlock(error);
                        }
                        return ;
                    }
                    if (value == 0xF2) {
                        [[SNFirmwareUpdater sharedUpdater] continueLoadingAfterReboot];
                    }
                    if (value != 0xF1 && value != 0xF2) {
                        if (completeBlock) {
                            completeBlock([NSError errorWithDomain:@"Password" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Incorrect password"}]);
                        }
                        return;
                    }
                    if (completeBlock) {
                        completeBlock(nil);
                    }
                }];
            }
            if (completeBlock) {
                completeBlock(nil);
            }
        }];
    }];
}

#pragma mark - Firmware Loading Commands

- (void)setProgrammingOnlyBtLock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetLockStatusForOnlyBtProgramming withPayload:nil complete:completeBlock];
}

- (void)setProgrammingLock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetLockStatusForProgramming withPayload:nil complete:completeBlock];
}

- (void)setBootloaderWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetBootloader
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              [self setBootloaderWithCompleteBlock:completeBlock];
                                              return;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Bootloader incorrect response"}]);
                                              }
                                              return;
                                          }
                                          if (completeBlock) {
                                              completeBlock(nil);
                                          }
                                      }];
}

- (void)setProxyWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    __block NSUInteger responsesCount = 0;
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetProxy
                       withValueResponsesCount:2
                          inCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          responsesCount ++;
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              if (responsesCount == 1) {
                                                  [self setProxyWithCompleteBlock:completeBlock];
                                              }
                                              else {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:1 userInfo:@{NSLocalizedDescriptionKey: @"SetProxy already done. Write next firmware"}]);
                                              }
                                              return;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"SetProxy incorrect response"}]);
                                              }
                                              return;
                                          }
                                          if (responsesCount == 2) {
                                              if (completeBlock) {
                                                  completeBlock(nil);
                                              }
                                          }
                                      }];
}

- (void)setReadProtectWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetReadProtect
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              [self setReadProtectWithCompleteBlock:completeBlock];
                                              return;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"ReadProtect incorrect response"}]);
                                              }
                                              return;
                                          }
                                          if (completeBlock) {
                                              completeBlock(nil);
                                          }
                                      }];
}

- (void)setFlashEraseWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    sleep(3);
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetFlashErase
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              [self setFlashEraseWithCompleteBlock:completeBlock];
                                              return;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"FlashErase incorrect response"}]);
                                              }
                                              return;
                                          }
                                          unsigned char *commandFF00 = malloc(2);
                                          commandFF00[0] = 0xff;
                                          commandFF00[1] = 0x00;
                                          sleep(2);
                                          [[SNBLEManager sharedManager] writeBinaryData:[NSData dataWithBytes:commandFF00 length:2]
                                                                       isChecksumNeeded:NO
                                                                               complete:^(NSError *error) {
                                                                                   if (error) {
                                                                                       if (completeBlock) {
                                                                                           completeBlock(error);
                                                                                       }
                                                                                       return ;
                                                                                   }
                                                                                   if (completeBlock) {
                                                                                       completeBlock(error);
                                                                                   }
                                                                               }];
                                      }];
}

- (void)setSerialDefaultWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetSerialDefault
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              [self setSerialDefaultWithCompleteBlock:completeBlock];
                                              return;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"SerialDefault incorrect response"}]);
                                              }
                                              return;
                                          }
                                          if (completeBlock) {
                                              completeBlock(nil);
                                          }
                                      }];
}

- (void)setBootUpdateEnableWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetBootUpdateEnable
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              [self setBootUpdateEnableWithCompleteBlock:completeBlock];
                                              return;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"BootUpdEnable incorrect response"}]);
                                              }
                                              return;
                                          }
                                          if (completeBlock) {
                                              completeBlock(nil);
                                          }
                                      }];
}

- (void)setApplyAppImageWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetApplyAppImage
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              [self setApplyAppImageWithCompleteBlock:completeBlock];
                                              return;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"AppImage incorrect response"}]);
                                              }
                                              return;
                                          }
                                          if ((int)value & 0xff00) {
                                              if (completeBlock) {
                                                  completeBlock(nil);
                                              }
                                          }
                                          else {
                                              [[SNBLEManager sharedManager] expectValueForCharacterictic:SNCharacteristicTypeCommandConfirm
                                                                                                complete:^(NSError *error, double value) {
                                                                                                    if (error) {
                                                                                                        if (completeBlock) {
                                                                                                            completeBlock(error);
                                                                                                        }
                                                                                                        return ;
                                                                                                    }
                                                                                                    if (value == 0x1f) {
                                                                                                        [self setApplyAppImageWithCompleteBlock:completeBlock];
                                                                                                        return;
                                                                                                    }
                                                                                                    if (value != 0x79 && value != 0x7979) {
                                                                                                        if (completeBlock) {
                                                                                                            completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"AppImage incorrect response"}]);
                                                                                                        }
                                                                                                        return;
                                                                                                    }
                                                                                                    if (completeBlock) {
                                                                                                        completeBlock(nil);
                                                                                                    }
                                                                                                }];
                                          }
                                      }];
}

- (void)setApplyBootImageWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetApplyBootImage
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              [self setApplyBootImageWithCompleteBlock:completeBlock];
                                              return;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"BootImage incorrect response"}]);
                                              }
                                              return;
                                          }
                                          if ((int)value & 0xff00) {
                                              if (completeBlock) {
                                                  completeBlock(nil);
                                              }
                                          }
                                          else {
                                              [[SNBLEManager sharedManager] expectValueForCharacterictic:SNCharacteristicTypeCommandConfirm
                                                                                                complete:^(NSError *error, double value) {
                                                                                                    if (error) {
                                                                                                        if (completeBlock) {
                                                                                                            completeBlock(error);
                                                                                                        }
                                                                                                        return ;
                                                                                                    }
                                                                                                    if (value == 0x1f) {
                                                                                                        [self setApplyBootImageWithCompleteBlock:completeBlock];
                                                                                                        return;
                                                                                                    }
                                                                                                    if (value != 0x79 && value != 0x7979) {
                                                                                                        if (completeBlock) {
                                                                                                            completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"BootImage incorrect response"}]);
                                                                                                        }
                                                                                                        return;
                                                                                                    }
                                                                                                    if (completeBlock) {
                                                                                                        completeBlock(nil);
                                                                                                    }
                                                                                                }];
                                          }
                                      }];
}

- (void)setProxyExitWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [self setProxyExitCount:269 withCompleteBlock:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        [self setProxyExitResponseCount:4 withCompleteBlock:completeBlock];
    }];
}

- (void)setProxyExitResponseCount:(NSUInteger)count withCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    if (count == 0) {
        if (completeBlock) {
            completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"ProxyExit incorrect response"}]);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetProxyExit
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error && [error.domain isEqualToString:@"Expectation"]) {
                                              [self setProxyExitResponseCount:count - 1 withCompleteBlock:completeBlock];
                                              return ;
                                          }
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value == 0x1f) {
                                              [self setProxyExitResponseCount:count - 1 withCompleteBlock:completeBlock];
                                              return;
                                          }
                                          if (value != 0x7a) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"ProxyExit incorrect response"}]);
                                              }
                                              return;
                                          }
                                          if (completeBlock) {
                                              completeBlock(nil);
                                          }
                                      }];

}

- (void)setProxyExitCount:(NSUInteger)count withCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    if (count == 0) {
        if (completeBlock) {
            completeBlock(nil);
        }
        return;
    }
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetProxyExit
                                   withPayload:nil
                               isConfirmNeeded:NO
                                      complete:^(NSError *error) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          [self setProxyExitCount:count - 1 withCompleteBlock:completeBlock];
                                      } isTimeout:YES];
}

- (void)writeFirmwareChunk:(NSData *)data chunkOffset:(NSUInteger)offset withCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    [[SNBLEManager sharedManager] writeCommand:SNBTCommandSetChunkWrite
         withValueResponseInCharacteristicType:SNCharacteristicTypeCommandConfirm
                                      complete:^(NSError *error, double value) {
                                          if (error) {
                                              if (completeBlock) {
                                                  completeBlock(error);
                                              }
                                              return ;
                                          }
                                          if (value != 0x79 && value != 0x7979) {
                                              if (completeBlock) {
                                                  completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"chunk incorrect response"}]);
                                              }
                                              return;
                                          }
                                          NSData *offsetData = [SNDataToIntegerConverter dataBigEndianFromUInteger:offset];
                                          [[SNBLEManager sharedManager] writeBinaryData:[offsetData subdataWithRange:NSMakeRange(offsetData.length - 4, 4)]
                                                                               complete:^(NSError *error) {
                                                                                   if (error) {
                                                                                       if (completeBlock) {
                                                                                           completeBlock(error);
                                                                                       }
                                                                                       return ;
                                                                                   }
                                                                                   if (value != 0x79 && value != 0x7979) {
                                                                                       if (completeBlock) {
                                                                                           completeBlock([NSError errorWithDomain:@"Firmware" code:0 userInfo:@{NSLocalizedDescriptionKey: @"chunk incorrect response"}]);
                                                                                       }
                                                                                       return;
                                                                                   }
                                                                                   NSUInteger len = data.length - 1;
                                                                                   NSMutableData *dataToWrite = [[NSData dataWithBytes:&len length:1] mutableCopy];
                                                                                   [dataToWrite appendData:data];
                                                                                   [[SNBLEManager sharedManager] writeBinaryData:[dataToWrite copy] complete:^(NSError *error) {
                                                                                       if (error) {
                                                                                           if (completeBlock) {
                                                                                               completeBlock(error);
                                                                                           }
                                                                                           return ;
                                                                                       }
                                                                                       if (completeBlock) {
                                                                                           completeBlock(nil);
                                                                                       }
                                                                                   }];
                                                                               }];
                                      }];
}

@end
