//
//  SNConvertor.h
//  Spinar
//
//  Created by Bohdan Pozharskyi on 12/4/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Simple convertor class for converting between miles and kilometres and other kind of data for this app
 *
 *   Created to remove some logic connected with exact presentation of data from model and controller
 *   to one file.
 */
@interface SNConvertor : NSObject

/**
 * Converts miles to kilometres
 * 
 * @param miles Given number of miles
 *
 * @return Kilometres
 */
+ (double)milesToKilometres:(double)miles;

/**
 * Converts kilometres to miles
 *
 * @param miles Given number of kilometres
 *
 * @return Miles
 */
+ (double)kilometresToMiles:(double)kilometres;

@end
