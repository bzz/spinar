//
//  SNSettings.h
//  Spinar
//
//  Created by User on 10/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNSettings : NSObject
/**
 *  Class describes settings and just store them without any updates.
 *  NSUserDefaults used as data storage.
 */


@property (assign, nonatomic) BOOL areDisplayingUnitsMiles;

/**
 * Returns a singletone of SNSettings
 */
+ (SNSettings *)sharedSettings;

/**
 * Bunch of help-methods for displaying units
 */
+ (NSString *)stringForDistanceUnits:(BOOL)areDisplayingUnitsMiles;
+ (NSString *)stringForTimeUnitsHours;
+ (NSString *)stringForTimeUnitsMinutes;
+ (NSString *)stringForSpeedUnits:(BOOL)areDisplayingUnitsMiles;

+ (UIColor *)colorForErrorCode:(NSUInteger)errorCode;
+ (NSString *)shortDescriptionForErrorCode:(NSUInteger)errorCode;
+ (NSString *)descriptionForErrorCode:(NSUInteger)errorCode;

@end
