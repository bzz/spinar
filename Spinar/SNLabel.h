//
//  SNLabel.h
//  Spinar
//
//  Created by Duhov Filipp on 7/19/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface SNLabel : UILabel

@end
