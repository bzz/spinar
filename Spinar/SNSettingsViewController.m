//
//  SNSettingsViewController.m
//  Spinar
//
//  Created by User on 10/28/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "SNSettingsViewController.h"
#import "SNSettings.h"
#import "SNWheel.h"
#include <math.h>
#import "VRSegmentedControl.h"
#import "UIColorFromRGB.h"
#import "UIAlertView+SNMessage.h"
#import "SNQuestionAlertView.h"
#import "SNFirmwareLoader.h"
#import "MBProgressHUD.h"
#import "SNRemoteFirmware.h"
#import "SNFirmwareUpdater.h"
#import "SNConvertor.h"
#import "AppDelegate.h"
#import "SNWheelConnector.h"

#define MIN_SPEED_LIMIT_KPH 10
#define MAX_SPEED_LIMIT_KPH 18

static NSString *const imageNameMiles = @"ic_settings_mph";
static NSString *const imageNameKilometres = @"ic_settings_kph";

@interface SNSettingsViewController () <SNWheelDelegate, SNWheelConnectorDelegate, UIAlertViewDelegate, SNFirmwareUpdaterDelegate>
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *unitLabels;
@property (weak, nonatomic) IBOutlet UILabel *minSpeedLimitLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxSpeedLimitLabel;
@property (weak, nonatomic) IBOutlet UIView *speedLimitView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *speedLimitLayoutConstraint;
@property (weak, nonatomic) IBOutlet UILabel *speedLimitLabel;
@property (weak, nonatomic) IBOutlet UISlider *speedLimitSlider;

@property (weak, nonatomic) IBOutlet UISwitch *wheelLockSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *learningModeSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *safetySwitch;
@property (weak, nonatomic) IBOutlet UISlider *riderModeSlider;
@property (weak, nonatomic) IBOutlet VRSegmentedControl *displayingUnits;

@property (weak, nonatomic) IBOutlet UIView *updateOverlay;
@property (strong, nonatomic) SNFirmwareUpdater *updater;
@property (strong, nonatomic) NSDictionary *textForUpdaterState;
@property (strong, nonatomic) MBProgressHUD *hud;

@end

@implementation SNSettingsViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.textForUpdaterState = @{@(SNFirmwareUpdaterStateCheckingFirmwaresVersions): @"Checking Firmware versions",
                                 @(SNFirmwareUpdaterStateCheckingNewVersions): @"Checking for new versions",
                                 @(SNFirmwareUpdaterStateDownloadingFirmware): @"Downloading firmwares",
                                 @(SNFirmwareUpdaterStateInstallingFirmware): @"Installing actions",
                                 @(SNFirmwareUpdaterStateWaitingForReboot): @"Waiting for restart",
                                 @(SNFirmwareUpdaterStateWritingWheelBootloader): @"Writing motor boot firmware",
                                 @(SNFirmwareUpdaterStateWritingBtBootloader): @"Writing main boot firmware",
                                 @(SNFirmwareUpdaterStateWritingWheel): @"Writing motor firmware",
                                 @(SNFirmwareUpdaterStateWritingBt): @"Writing main firmware",
                                 @(SNFirmwareUpdaterStateDone): @"Done"};

    _updater = [SNFirmwareUpdater sharedUpdater];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.displayingUnits.indexChangeBlock = ^(NSUInteger index, BOOL animated) {
        [SNSettings sharedSettings].areDisplayingUnitsMiles = (index == 0);
        [self refreshUnits];
    };
    [self.displayingUnits setSelectedIndex:[[SNSettings sharedSettings] areDisplayingUnitsMiles] ? 0 : 1];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.updater setDelegate:self];
    [self refreshUnits];
    if ([[SNWheel currentWheel] state] == SNWheelStateLoadingFirmware) {
        [self stateChanged:self.updater.state];
    }
    [[SNWheel currentWheel] setDelegate:self];
    [[SNWheelConnector sharedInstance] setDelegate:self];
    [self update];
}

- (void)update
{
    [[SNWheel currentWheel] updateRidingModeWithBlock:^(NSError *error) {
        SNRidingMode mode = [[SNWheel currentWheel] ridingMode];
        [self.riderModeSlider setValue:mode animated:YES];
        [[SNWheel currentWheel] updateSafetyOnWithBlock:^(NSError *error) {
            [self.safetySwitch setOn:[[SNWheel currentWheel] isSafetyOn]];
            [[SNWheel currentWheel] updateWheelLocked:^(NSError *error) {
                [self.wheelLockSwitch setOn:[[SNWheel currentWheel] isWheelLocked]];
                [[SNWheel currentWheel] updateSpeedLimitWithBlock:^(NSError *error) {
                    double speedLimit = [[SNWheel currentWheel] speedLimit];
                    [self.learningModeSwitch setOn:(NSInteger)speedLimit < 22];
                    [self.speedLimitSlider setValue:(speedLimit - MIN_SPEED_LIMIT_KPH)/(MAX_SPEED_LIMIT_KPH - MIN_SPEED_LIMIT_KPH) animated:YES];
                    [self refreshSpeedLimiter];
                    [self.tableView reloadData];
                    [self.updater updateFirmwaresToUpdateWithBlock:^(NSError *error) {
                        [self refreshFirmwareAvailability];
                    }];
                }];
            }];
        }];
    }];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    // This code makes separators in ios 8.x without left insets
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[SNWheel currentWheel] setDelegate:nil];
    [super viewWillDisappear:animated];
}

- (IBAction)sliderSpeedLimitDidEndEditing:(id)sender {
    NSInteger value = MIN_SPEED_LIMIT_KPH * (1 - self.speedLimitSlider.value) + MAX_SPEED_LIMIT_KPH * self.speedLimitSlider.value;
    [[SNWheel currentWheel] setSpeedLimit:value withBlock:^(NSError *error) {
        double speedLimit = [[SNWheel currentWheel] speedLimit];
        [self.speedLimitSlider setValue:(speedLimit - MIN_SPEED_LIMIT_KPH)/(MAX_SPEED_LIMIT_KPH - MIN_SPEED_LIMIT_KPH) animated:YES];
        [self refreshSpeedLimiter];
    }];
}

- (IBAction)sliderSpeedLimitValueChanged:(id)sender {
    [self refreshSpeedLimiter];
}

- (IBAction)sliderRidingModeValueChanged:(UISlider *)slider
{
    NSInteger oldvalue = (NSInteger)[[SNWheel currentWheel] ridingMode];
    NSInteger newvalue = (NSInteger)(slider.value+0.5);
    [slider setValue:(NSInteger)(slider.value+0.5) animated:NO];
    if (oldvalue == newvalue)
        return;
    SNRidingMode newMode = newvalue;
    [[SNWheel currentWheel] setRidingMode:newMode withBlock:^(NSError *error) {
        SNRidingMode mode = [[SNWheel currentWheel] ridingMode];
        [self.riderModeSlider setValue:mode animated:YES];
        [UIAlertView showMessage:@"Please restart the wheel to change the rider mode" withHeader:@"Rider Mode"];
    }];
}

- (IBAction)learningModeSwitchDidChangeValue:(UISwitch *)switchUI
{
    [self.tableView reloadData];
    if (![self.learningModeSwitch isOn]) {
        [[SNWheel currentWheel] setSpeedLimit:22 withBlock:^(NSError *error) {
            if (!error) {
                [UIAlertView showMessage:@"By disabling the speed limiter you will be approaching the physical limits of the device at your own risk." withHeader:@"Speed Limiter"];
            }
        }];
    }
    else {
        NSInteger value = MIN_SPEED_LIMIT_KPH * (1 - self.speedLimitSlider.value) + MAX_SPEED_LIMIT_KPH * self.speedLimitSlider.value;
        [[SNWheel currentWheel] setSpeedLimit:value withBlock:^(NSError *error) {
            double speedLimit = [[SNWheel currentWheel] speedLimit];
            [self.speedLimitSlider setValue:(speedLimit - MIN_SPEED_LIMIT_KPH)/(MAX_SPEED_LIMIT_KPH - MIN_SPEED_LIMIT_KPH) animated:YES];
        }];
    }
}
- (IBAction)wheelLockSwitchDidChangeValue:(UISwitch *)switchUI
{
    [[SNWheel currentWheel] setWheelLocked:self.wheelLockSwitch.isOn
                                 withBlock:^(NSError *error) {
                                     [self.wheelLockSwitch setOn:[[SNWheel currentWheel] isWheelLocked]];
                                     if (!error) {
                                         [UIAlertView showMessage:@"Please restart the wheel to apply Lock" withHeader:@"Lock Wheel"];
                                     }
                                 }];
}
- (IBAction)safetySwitchDidChangeValue:(id)sender {
    [[SNWheel currentWheel] setSafetyOn:self.safetySwitch.isOn
                              withBlock:^(NSError *error) {
                                  [self.safetySwitch setOn:[[SNWheel currentWheel] isSafetyOn]];
                                  if (!error) {
                                      [UIAlertView showMessage:@"Please restart the wheel to apply Safety Features" withHeader:@"Safety Features"];
                                  }
                              }];
}

#pragma mark - UITableViewDelegate protocol implementation

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3 && !self.learningModeSwitch.isOn) {
        return 53;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // This code makes separators in ios 8.x without left insets
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 5) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Device Name"
                                                            message:@"Input new device name"
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"OK", nil];
        [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alertView show];
    }
    if (indexPath.row == 6) {
        [self installWheelUpdate];
    }
}

- (void)refreshFirmwareAvailability
{
    [self.updateOverlay setBackgroundColor:(self.updater.firmwaresToUpdate.count ? [UIColor clearColor] : UIColorFromRGBA(22, 22, 22, 0.5))];
    [UIApplication sharedApplication].applicationIconBadgeNumber = self.updater.firmwaresToUpdate.count;
    self.tabBarController.tabBar.items.lastObject.badgeValue = self.updater.firmwaresToUpdate.count ? @(self.updater.firmwaresToUpdate.count).description : nil;
}

- (void)refreshSpeedLimiter
{
    self.speedLimitLayoutConstraint.constant = (self.speedLimitSlider.value - 0.5) * (self.speedLimitSlider.frame.size.width - 25);
    [self.speedLimitView layoutIfNeeded];
    NSInteger value = MIN_SPEED_LIMIT_KPH * (1 - self.speedLimitSlider.value) + MAX_SPEED_LIMIT_KPH * self.speedLimitSlider.value;
    if ([SNSettings sharedSettings].areDisplayingUnitsMiles) {
        value = [SNConvertor kilometresToMiles:value];
    }
    self.speedLimitLabel.text = @(value).description;
}

- (void)refreshUnits
{
    for (UILabel *label in self.unitLabels) {
        label.text = [SNSettings sharedSettings].areDisplayingUnitsMiles ? @"mph" : @"kph";
    }
    
    self.minSpeedLimitLabel.text = @((NSInteger)([SNSettings sharedSettings].areDisplayingUnitsMiles ?
                                                 [SNConvertor kilometresToMiles:MIN_SPEED_LIMIT_KPH] :
                                                 MIN_SPEED_LIMIT_KPH)).description;
    self.maxSpeedLimitLabel.text = @((NSInteger)([SNSettings sharedSettings].areDisplayingUnitsMiles ?
                                                 [SNConvertor kilometresToMiles:MAX_SPEED_LIMIT_KPH] :
                                                 MAX_SPEED_LIMIT_KPH)).description;
    NSInteger speedLimitValue = MIN_SPEED_LIMIT_KPH * (1 - self.speedLimitSlider.value) + MAX_SPEED_LIMIT_KPH * self.speedLimitSlider.value;
    self.speedLimitLabel.text = @((NSInteger)([SNSettings sharedSettings].areDisplayingUnitsMiles ?
                                              [SNConvertor kilometresToMiles:speedLimitValue] :
                                              speedLimitValue)).description;
}

#pragma mark - SNWheelDelegate protocol implementation

- (void)didUpdateProperties
{
}

#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        [[SNWheel currentWheel] setDeviceName:[[alertView textFieldAtIndex:0] text] withBlock:^(NSError *error) {
            
        }];
    }
}

#pragma mark - Wheel update

- (void)installWheelUpdate
{
    [SNQuestionAlertView showQuestion:@"To begin the update procedure place the wheel on its side and keep your phone within 3 metres"
                             okButton:@"Cancel"
                             noButton:@"Proceed"
                           withHeader:@"Uniwheel Update Step 1 of 2"
                        completeBlock:^(BOOL isOk) {
                            if (!isOk) {
                                [self.updater start];
                            }
                        }];
}
     
- (void)stateChanged:(SNFirmwareUpdaterState)state
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.hud)
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.hud setLabelText:self.textForUpdaterState[@(state)]];
        [self.hud setMode:(state == SNFirmwareUpdaterStateWritingWheelBootloader ||
                           state == SNFirmwareUpdaterStateWritingBtBootloader ||
                           state == SNFirmwareUpdaterStateWritingWheel ||
                           state == SNFirmwareUpdaterStateWritingBt ? MBProgressHUDModeDeterminate : MBProgressHUDModeIndeterminate)];
        if (state == SNFirmwareUpdaterStateWaitingForReboot) {
            if (self.updater.stage == SNFirmwareUpdaterStageLockWheelForLoad) {
                [UIAlertView showMessage:@"Press the On button to initialise the update" withHeader:@"Uniwheel Update Step 2 of 2"];
            }
            else {
                [UIAlertView showMessage:@"Restart Wheel to contine" withHeader:@"Firmware"];
            }
            
        }
        if (state == SNFirmwareUpdaterStateDone) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            self.hud = nil;
        }
    });
}

- (void)progressChanged:(double)progress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.hud) {
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self.hud setMode:MBProgressHUDModeDeterminate];
            [self.hud setLabelText:self.textForUpdaterState[@(self.updater.state)]];
        }
        [self.hud setProgress:progress];
    });
}

- (void)finishedWithError:(NSError *)error
{
    if (error && [error.domain isEqualToString:@"Firmware"] && error.code == 1) {
        [UIAlertView showMessage:@"Your Uniwheel has the latest software." withHeader:@"Firmware"];
        return;
    }
    [UIAlertView showMessage:error ? [NSString stringWithFormat:@"Finished with error %@. Turn off and then turn on the wheel, and firmware loading will start again.", error.localizedDescription] : @"Successfully finished. Turn off and then turn on the wheel." withHeader:@"Firmware"];
    [self refreshFirmwareAvailability];
}

#pragma mark - SNWheelConnectorDelegate

- (void)devicesUpdated {
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] connectionStateChanged];
    if ([[SNWheelConnector sharedInstance] connectedDevice]) {
        [[SNWheel currentWheel] writePassword:[@"123456" dataUsingEncoding:NSUTF8StringEncoding] withCompleteBlock:^(NSError *error) {
            if (error) {
                [UIAlertView showMessage:@"Wheel is not available now" withHeader:@"Error"];
                return ;
            }
            [self update];
        }];
    }
}


@end
