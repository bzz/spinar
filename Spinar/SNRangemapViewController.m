//
//  SNRangemapViewController.m
//  Spinar
//
//  Created by User on 11/4/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SNRangemapViewController.h"
#import "SNWheel.h"
#import "UIColorFromRGB.h"
#import "UIAlertView+SNMessage.h"
#import "SNSettings.h"

CGFloat const kBigCircleRadius = 1000;
CGFloat const kSmallCircleRadius = 700;

@interface SNRangemapViewController () <MKMapViewDelegate, SNWheelDelegate>

@property (weak, nonatomic) IBOutlet UIView *mapOverlayView;
@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) MKCircle *bigCircle;
@property (nonatomic, strong) MKCircle *smallCircle;

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation SNRangemapViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.locationManager = ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)?[[CLLocationManager alloc] init]:nil;
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.zoomEnabled = YES;
    self.mapView.scrollEnabled = YES;
    self.mapView.userTrackingMode = MKUserTrackingModeFollow;
    
    self.navigationItem.rightBarButtonItem.tintColor = UIColorFromRGB(62, 186, 231);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[SNWheel currentWheel] setDelegate:self];
}

- (void)didUpdateProperties
{
    self.navigationItem.rightBarButtonItem.tintColor = [SNSettings colorForErrorCode:[[SNWheel currentWheel] errorCode]];
}

#pragma mark - MKMapViewDelegate protocol implementation

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{
    if (self.mapOverlayView.alpha < 1) {
        MKCoordinateRegion region = {{0.0, 0.0}, {0.0, 0.0}};
        region.center.latitude = 51.5010;
        region.center.longitude = 0.1416;
        region.span.longitudeDelta = 0.05f;
        region.span.longitudeDelta = 0.05f;
        [mapView setRegion:region animated:YES];

        [UIView animateWithDuration:kDefaultAnimationDuration
                         animations:^{
                             self.mapOverlayView.alpha = 1;
                         }];
        if (!([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied && [CLLocationManager locationServicesEnabled])) {
            [UIAlertView showMessage:@"Please, enable location services." withHeader:@"Rangemap"];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (self.mapOverlayView.alpha > 0) {
        [UIView animateWithDuration:kDefaultAnimationDuration
                         animations:^{
                             self.mapOverlayView.alpha = 0;
                         }];
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    }
    if (self.smallCircle) {
        [mapView removeOverlay:self.smallCircle];
    }
    if (self.bigCircle) {
        [mapView removeOverlay:self.bigCircle];
    }
    self.smallCircle = [MKCircle circleWithCenterCoordinate:userLocation.location.coordinate radius:([[SNWheel currentWheel] distanceEstimate]/2)*0.9*1000];
    self.bigCircle = [MKCircle circleWithCenterCoordinate:userLocation.location.coordinate radius:([[SNWheel currentWheel] distanceEstimate]/2)*0.75*1000];
    [mapView addOverlays:@[ self.smallCircle, self.bigCircle ] level:MKOverlayLevelAboveRoads];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    MKCircleRenderer *aRenderer = [[MKCircleRenderer alloc] initWithCircle:overlay];
    aRenderer.lineWidth = 3;
    aRenderer.fillColor = [UIColor clearColor];
    if ([overlay isEqual:self.smallCircle]) {
        aRenderer.strokeColor = [UIColor colorWithRed:61.0 / 255 green:182.0 / 255 blue:226.0 / 255 alpha:1];
    }
    else if ([overlay isEqual:self.bigCircle]) {
        aRenderer.strokeColor = [UIColor whiteColor];
    }
    
    return aRenderer;
}

@end
