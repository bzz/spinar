//
//  SNConvertor.m
//  Spinar
//
//  Created by Bohdan Pozharskyi on 12/4/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "SNConvertor.h"

static double RatioKilometresToMiles = 1.609344;

@implementation SNConvertor

+ (double)milesToKilometres:(double)miles {
    return miles * RatioKilometresToMiles;
}

+ (double)kilometresToMiles:(double)kilometres {
    return kilometres / RatioKilometresToMiles;
}

@end
