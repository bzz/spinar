//
//  SNDashboardContentViewController.h
//  Spinar
//
//  Created by Hena Sofronov on 2/29/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNLabel.h"
#import "SNSpeedProgressBarView.h"

@protocol SNDashboardContentViewControllerDelegate <NSObject>

- (void)speedViewTapped:(id)sender;
- (void)lightsSwitchValueChanged:(BOOL)isOn;

@end

@interface SNDashboardContentViewController : UIViewController
@property (weak, nonatomic) id<SNDashboardContentViewControllerDelegate> delegate;
@property (assign, nonatomic) NSUInteger pageIndex;

@property (weak, nonatomic) IBOutlet SNLabel *speedLabel;
@property (weak, nonatomic) IBOutlet SNSpeedProgressBarView *speedProgressBarView;
- (void)refresh;

@end
