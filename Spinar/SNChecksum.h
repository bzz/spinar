//
//  SNChecksum.h
//  Spinar
//
//  Created by Hena Sofronov on 4/8/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNChecksum : NSObject

+ (unsigned char)checksum:(NSData *)data;

@end
