//
//  SNSpeedDashboardContentViewController.h
//  Spinar
//
//  Created by Hena Sofronov on 2/29/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNDashboardContentViewController.h"

@interface SNSpeedDashboardContentViewController : SNDashboardContentViewController

@property (weak, nonatomic) IBOutlet UISwitch *lightsSwitch;

@end
