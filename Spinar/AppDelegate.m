//
//  AppDelegate.m
//  Spinar
//
//  Created by hena on 10/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "AppDelegate.h"
//#import <HockeySDK/HockeySDK.h>
#import "SNBLEManager.h"
#import "SNWheelConnector.h"
#import "SNSidebarTableViewController.h"

CGFloat const kDefaultAnimationDuration = .3;

@interface AppDelegate ()
@property (strong, nonatomic) IBOutlet UIView *overlayView;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//#ifndef DEBUG
//    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"d94de7fb45670b1eb3fd8f83524542a2"];
//    [[BITHockeyManager sharedHockeyManager] startManager];
//    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
//    [[[BITHockeyManager sharedHockeyManager] crashManager] setCrashManagerStatus:BITCrashManagerStatusAutoSend];
//#endif
   
    UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge
                                                                                         categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[NSBundle mainBundle] loadNibNamed:@"overlayView" owner:self options:nil];
    [self.overlayView setFrame:[UIScreen mainScreen].bounds];
    [self.window addSubview:self.overlayView];
    [self connectionStateChanged];
    return YES;
}

- (IBAction)overlayViewTapRecognized:(id)sender {
    NSLog(@"overlay tapped");
    UIViewController *topController = self.window.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    SNSidebarTableViewController *sidebarViewController = [topController.storyboard instantiateViewControllerWithIdentifier:@"SidebarTableViewControllerID"];
    [topController presentViewController:sidebarViewController animated:YES completion:NULL];
}

- (void)connectionStateChanged
{
    BOOL isNeedToDisplay = ![[SNWheelConnector sharedInstance] connectedDevice];
    [self.overlayView removeFromSuperview];
   
    if (isNeedToDisplay) {
//    if (!isNeedToDisplay) {
        UIViewController *topController = self.window.rootViewController;
        while (topController.presentedViewController) {
            topController = topController.presentedViewController;
        }
        [topController.view addSubview:self.overlayView];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SNBLEManager sharedManager] stopScan];
    [[SNBLEManager sharedManager] disconnect];
}

@end
