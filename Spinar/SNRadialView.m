//
//  SNRadialView.m
//  Spinar
//
//  Created by hena on 10/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "SNRadialView.h"

#define SLICE_NUMBER 120

@implementation SNRadialView

- (id)initWithFrame:(CGRect)frame andTheme:(MDRadialProgressTheme *)theme fullAngle:(double)fullAngle
{
    self = [super initWithFrame:frame andTheme:theme];
    if (self)
    {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setFullAngle:fullAngle];
        [self setProgressTotal:SLICE_NUMBER];
        [self setStartingSlice:(int)(((M_PI + ((M_PI * 2 - fullAngle) / 2))/(2*M_PI)) * SLICE_NUMBER) + 1];
        [self setClipsToBounds:NO];
    }
    return self;
}

- (void)setProgress:(double)progress
{
    _progress = (progress > 1 ? 1 : progress);
    [self setProgressCounter:SLICE_NUMBER * ((self.fullAngle/(2*M_PI))*_progress)];
}

@end
