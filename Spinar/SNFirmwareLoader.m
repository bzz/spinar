//
//  SNFirmwareLoader.m
//  Spinar
//
//  Created by hena on 6/26/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNFirmwareLoader.h"
#import "UIAlertView+SNMessage.h"
#import "SNDelayedInvoke.h"
#import "SNStatistics.h"

double const kFirmwareBinaryPacketSize = 256;

typedef void(^BootloaderCompleteBlock)(NSError *error);
typedef void(^WritingBinaryCompleteBlock)(NSError *error);

@interface SNFirmwareLoader ()

@end

@implementation SNFirmwareLoader

- (id)init
{
    self = [super init];
    if (self)
    {

    }
    return self;
}

- (void)setState:(SNFirmwareLoaderState)state
{
    _state = state;
    [self.delegate stateChanged:state];
}

- (void)lockForProgramming
{
    [[SNWheel currentWheel] setState:SNWheelStateLoadingFirmware];
    [self setState:SNFirmwareLoaderStateLocking];
    [[SNWheel currentWheel] setProgrammingLock:^(NSError *error) {
        NSLog(@"LOCKED WITH ERROR: %@", error);
        if (error) {
            [self finishWithError:error];
            return ;
        }
        [self setState:SNFirmwareLoaderStateWaitingForReboot];
    }];
}

- (void)lockForProgrammingOnlyBt
{
    [[SNWheel currentWheel] setState:SNWheelStateLoadingFirmware];
    [self setState:SNFirmwareLoaderStateLocking];
    [[SNWheel currentWheel] setProgrammingOnlyBtLock:^(NSError *error) {
        NSLog(@"LOCKED WITH ERROR: %@", error);
        if (error) {
            [self finishWithError:error];
            return ;
        }
        [self setState:SNFirmwareLoaderStateWaitingForReboot];
    }];
}

- (void)startLoadingWheelFirmware:(SNFirmware *)wheelFirmware andBTFirmware:(SNFirmware *)btFirmware
{
    if (!wheelFirmware && !btFirmware) {
        [self finishWithError:[NSError errorWithDomain:@"Firmware Loader" code:0 userInfo:@{NSLocalizedDescriptionKey: @"No firmwares to load"}]];
        return;
    }
    [self setFirmwareBT:btFirmware];
    [self setFirmwareWheel:wheelFirmware];
    [self setFirmwareBootloaderBT:nil];
    [self setFirmwareBootloaderWheel:nil];
    [[SNWheel currentWheel] setState:SNWheelStateLoadingFirmware];
    [[SNStatistics sharedInstance] resetStatistics];
    NSLog(@"loading bt version %lu.%lu.%lu wheel version %lu.%lu.%lu",
          (unsigned long)btFirmware.version.major, (unsigned long)btFirmware.version.minor, (unsigned long)btFirmware.version.build,
          (unsigned long)wheelFirmware.version.major, (unsigned long)wheelFirmware.version.minor, (unsigned long)wheelFirmware.version.build);
    [self continueLoadingAfterRebootFirmwares];
}

- (void)startLoadingBootloaderWheelFirmware:(SNFirmware *)wheelBootloaderFirmware andBTFirmware:(SNFirmware *)btBootloaderFirmware
{
    if (!wheelBootloaderFirmware || !btBootloaderFirmware) {
        [self finishWithError:[NSError errorWithDomain:@"Firmware Loader" code:0 userInfo:@{NSLocalizedDescriptionKey: @"No firmwares to load"}]];
        return;
    }
    [self setFirmwareBootloaderWheel:wheelBootloaderFirmware];
    [self setFirmwareBootloaderBT:btBootloaderFirmware];
    [self setFirmwareBT:nil];
    [self setFirmwareWheel:nil];
    [[SNWheel currentWheel] setState:SNWheelStateLoadingFirmware];
    [[SNStatistics sharedInstance] resetStatistics];
    NSLog(@"loading bt boot version %lu.%lu.%lu wheel boot version %lu.%lu.%lu",
          (unsigned long)btBootloaderFirmware.version.major, (unsigned long)btBootloaderFirmware.version.minor, (unsigned long)btBootloaderFirmware.version.build,
          (unsigned long)wheelBootloaderFirmware.version.major, (unsigned long)wheelBootloaderFirmware.version.minor, (unsigned long)wheelBootloaderFirmware.version.build);
    [self continueLoadingAfterRebootBootloaderFirmwares];
}

- (void)continueLoadingAfterRebootFirmwares
{
    [[SNWheel currentWheel] setState:SNWheelStateLoadingFirmware];
    [self setState:SNFirmwareLoaderStateInitingBootloader];
    [[SNWheel currentWheel] setProxyWithCompleteBlock:^(NSError *error) {
        NSLog(@"PROXY WITH ERROR: %@", error);
        if (error && error.code != 1) {
            [self finishWithError:error];
            return ;
        }
        if (error && error.code == 1) {
            [self writeBtFirmwareWithCompleteBlock:^(NSError *error) {
                if (error) {
                    [self finishWithError:error];
                    return ;
                }
                [self finishWithError:nil];
            }];
            return;
        }
        [self writeWheelFirmwareWithCompleteBlock:^(NSError *error) {
            if (error) {
                [self finishWithError:error];
                return ;
            }
            [self setState:SNFirmwareLoaderStateInitingBootloader];
            [[SNWheel currentWheel] setProxyExitWithCompleteBlock:^(NSError *error) {
                NSLog(@"PROXY EXIT WITH ERROR: %@", error);
                if (error) {
                    [self finishWithError:error];
                    return ;
                }
                [self writeBtFirmwareWithCompleteBlock:^(NSError *error) {
                    if (error) {
                        [self finishWithError:error];
                        return ;
                    }
                    [self finishWithError:nil];
                }];
            }];
        }];
    }];
}

- (void)writeBtFirmwareWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    if (!self.firmwareBT) {
        [[SNWheel currentWheel] setApplyAppImageWithCompleteBlock:^(NSError *error) {
            NSLog(@"APPLY APP IMG WITH ERROR: %@", error);
            if (error) {
                if (completeBlock) {
                    completeBlock(error);
                }
                return ;
            }
            if (completeBlock) {
                completeBlock(nil);
            }
        }];
        return;
    }
    [[SNWheel currentWheel] setFlashEraseWithCompleteBlock:^(NSError *error) {
        NSLog(@"FLASH ERASE WITH ERROR: %@", error);
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        [[SNWheel currentWheel] setSerialDefaultWithCompleteBlock:^(NSError *error) {
            NSLog(@"SERIAL DEFAULT WITH ERROR: %@", error);
            if (error) {
                if (completeBlock) {
                    completeBlock(error);
                }
                return ;
            }
            [self setState:SNFirmwareLoaderStateWritingBT];
            [self writePacketsOfBytesfromFirmware:self.firmwareBT withOffset:0 withCompleteBlock:^(NSError *error) {
                NSLog(@"BINARY WITH ERROR: %@", error);
                if (error) {
                    if (completeBlock) {
                        completeBlock(error);
                    }
                    return ;
                }
                [self setState:SNFirmwareLoaderStateInitingBootloader];
                [[SNWheel currentWheel] setApplyAppImageWithCompleteBlock:^(NSError *error) {
                    NSLog(@"APPLY APP IMG WITH ERROR: %@", error);
                    if (error) {
                        if (completeBlock) {
                            completeBlock(error);
                        }
                        return ;
                    }
                    if (completeBlock) {
                        completeBlock(nil);
                    }
                }];
            }];
        }];
    }];
}

- (void)writeWheelFirmwareWithCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    if (!self.firmwareWheel) {
        [[SNWheel currentWheel] setApplyAppImageWithCompleteBlock:^(NSError *error) {
            NSLog(@"APPLY APP IMG WITH ERROR: %@", error);
            if (error) {
                if (completeBlock) {
                    completeBlock(error);
                }
                return ;
            }
            if (completeBlock) {
                completeBlock(nil);
            }
        }];
        return;
    }
    [[SNWheel currentWheel] setFlashEraseWithCompleteBlock:^(NSError *error) {
        NSLog(@"FLASH ERASE WITH ERROR: %@", error);
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        [[SNWheel currentWheel] setSerialDefaultWithCompleteBlock:^(NSError *error) {
            NSLog(@"SERIAL DEFAULT WITH ERROR: %@", error);
            if (error) {
                if (completeBlock) {
                    completeBlock(error);
                }
                return ;
            }
            [self setState:SNFirmwareLoaderStateWritingWheel];
            [self writePacketsOfBytesfromFirmware:self.firmwareWheel withOffset:0 withCompleteBlock:^(NSError *error) {
                NSLog(@"BINARY WITH ERROR: %@", error);
                if (error) {
                    if (completeBlock) {
                        completeBlock(error);
                    }
                    return ;
                }
                [[SNWheel currentWheel] setApplyAppImageWithCompleteBlock:^(NSError *error) {
                    NSLog(@"APPLY APP IMG WITH ERROR: %@", error);
                    if (error) {
                        if (completeBlock) {
                            completeBlock(error);
                        }
                        return ;
                    }
                    if (completeBlock) {
                        completeBlock(nil);
                    }
                }];
            }];
        }];
    }];
}

- (void)continueLoadingAfterRebootBootloaderFirmwares
{
    [[SNWheel currentWheel] setState:SNWheelStateLoadingFirmware];
    [self setState:SNFirmwareLoaderStateInitingBootloader];
    [[SNWheel currentWheel] setProxyWithCompleteBlock:^(NSError *error) {
        NSLog(@"PROXY WITH ERROR: %@", error);
        if (error && error.code != 1) {
            [self finishWithError:error];
            return ;
        }
        if (error && error.code == 1) {
            [self setState:SNFirmwareLoaderStateInitingBootloader];
            [[SNWheel currentWheel] updatefirmwareVersionBTBootloaderWithCompleteBlock:^(NSError *error) {
                if ([SNFirmware compareVersion:[[SNWheel currentWheel] firmwareVersionBTBootloader] andVerion:self.firmwareBootloaderBT.version] != NSOrderedAscending) {
                    [self finishWithError:nil];
                }
                else {
                    [[SNWheel currentWheel] setSerialDefaultWithCompleteBlock:^(NSError *error) {
                        NSLog(@"SERIAL DEFAULT WITH ERROR: %@", error);
                        if (error) {
                            [self finishWithError:error];
                            return ;
                        }
                        [[SNWheel currentWheel] setBootUpdateEnableWithCompleteBlock:^(NSError *error) {
                            NSLog(@"BOOT UPD WITH ERROR: %@", error);
                            if (error) {
                                [self finishWithError:error];
                                return ;
                            }
                            [self setState:SNFirmwareLoaderStateWritingBT];
                            [self writePacketsOfBytesfromFirmware:self.firmwareBootloaderBT withOffset:0 withCompleteBlock:^(NSError *error) {
                                NSLog(@"BINARY WITH ERROR: %@", error);
                                if (error) {
                                    [self finishWithError:error];
                                    return ;
                                }
                                [self setState:SNFirmwareLoaderStateInitingBootloader];
                                [[SNWheel currentWheel] setApplyBootImageWithCompleteBlock:^(NSError *error) {
                                    NSLog(@"APPLY BOOT IMG WITH ERROR: %@", error);
                                    if (error) {
                                        [self finishWithError:error];
                                        return ;
                                    }
                                    [self finishWithError:nil];
                                }];
                            }];
                        }];
                    }];
                }
            }];
            return;
        }
        [[SNWheel currentWheel] updatefirmwareVersionWheelBootloaderWithCompleteBlock:^(NSError *error) {
            if ([SNFirmware compareVersion:[[SNWheel currentWheel] firmwareVersionWheelBootloader] andVerion:self.firmwareBootloaderWheel.version] != NSOrderedAscending) {
                [self setState:SNFirmwareLoaderStateInitingBootloader];
                [[SNWheel currentWheel] setProxyExitWithCompleteBlock:^(NSError *error) {
                    NSLog(@"PROXY EXIT WITH ERROR: %@", error);
                    if (error) {
                        [self finishWithError:error];
                        return ;
                    }
                    [[SNWheel currentWheel] updatefirmwareVersionBTBootloaderWithCompleteBlock:^(NSError *error) {
                        if ([SNFirmware compareVersion:[[SNWheel currentWheel] firmwareVersionBTBootloader] andVerion:self.firmwareBootloaderBT.version] != NSOrderedAscending) {
                            [self finishWithError:nil];
                        }
                        else {
                            [[SNWheel currentWheel] setSerialDefaultWithCompleteBlock:^(NSError *error) {
                                NSLog(@"SERIAL DEFAULT WITH ERROR: %@", error);
                                if (error) {
                                    [self finishWithError:error];
                                    return ;
                                }
                                [[SNWheel currentWheel] setBootUpdateEnableWithCompleteBlock:^(NSError *error) {
                                    NSLog(@"BOOT UPD WITH ERROR: %@", error);
                                    if (error) {
                                        [self finishWithError:error];
                                        return ;
                                    }
                                    [self setState:SNFirmwareLoaderStateWritingBT];
                                    [self writePacketsOfBytesfromFirmware:self.firmwareBootloaderBT withOffset:0 withCompleteBlock:^(NSError *error) {
                                        NSLog(@"BINARY WITH ERROR: %@", error);
                                        if (error) {
                                            [self finishWithError:error];
                                            return ;
                                        }
                                        [self setState:SNFirmwareLoaderStateInitingBootloader];
                                        [[SNWheel currentWheel] setApplyBootImageWithCompleteBlock:^(NSError *error) {
                                            NSLog(@"APPLY BOOT IMG WITH ERROR: %@", error);
                                            if (error) {
                                                [self finishWithError:error];
                                                return ;
                                            }
                                            [self finishWithError:nil];
                                        }];
                                    }];
                                }];
                            }];
                        }
                    }];
                }];
            }
            else {
                [[SNWheel currentWheel] setSerialDefaultWithCompleteBlock:^(NSError *error) {
                    NSLog(@"SERIAL DEFAULT WITH ERROR: %@", error);
                    if (error) {
                        [self finishWithError:error];
                        return ;
                    }
                    [[SNWheel currentWheel] setBootUpdateEnableWithCompleteBlock:^(NSError *error) {
                        NSLog(@"BOOT UPD WITH ERROR: %@", error);
                        if (error) {
                            [self finishWithError:error];
                            return ;
                        }
                        [self setState:SNFirmwareLoaderStateWritingWheel];
                        [self writePacketsOfBytesfromFirmware:self.firmwareBootloaderWheel withOffset:0 withCompleteBlock:^(NSError *error) {
                            NSLog(@"BINARY WITH ERROR: %@", error);
                            if (error) {
                                [self finishWithError:error];
                                return ;
                            }
                            [[SNWheel currentWheel] setApplyBootImageWithCompleteBlock:^(NSError *error) {
                                NSLog(@"APPLY BOOT IMG WITH ERROR: %@", error);
                                if (error) {
                                    [self finishWithError:error];
                                    return ;
                                }
                                [self setState:SNFirmwareLoaderStateInitingBootloader];
                                [[SNWheel currentWheel] setProxyExitWithCompleteBlock:^(NSError *error) {
                                    NSLog(@"PROXY EXIT WITH ERROR: %@", error);
                                    if (error) {
                                        [self finishWithError:error];
                                        return ;
                                    }
                                    [[SNWheel currentWheel] updatefirmwareVersionBTBootloaderWithCompleteBlock:^(NSError *error) {
                                        if ([SNFirmware compareVersion:[[SNWheel currentWheel] firmwareVersionBTBootloader] andVerion:self.firmwareBootloaderBT.version] != NSOrderedAscending) {
                                            [self finishWithError:nil];
                                        }
                                        else {
                                            [[SNWheel currentWheel] setSerialDefaultWithCompleteBlock:^(NSError *error) {
                                                NSLog(@"SERIAL DEFAULT WITH ERROR: %@", error);
                                                if (error) {
                                                    [self finishWithError:error];
                                                    return ;
                                                }
                                                [[SNWheel currentWheel] setBootUpdateEnableWithCompleteBlock:^(NSError *error) {
                                                    NSLog(@"BOOT UPD WITH ERROR: %@", error);
                                                    if (error) {
                                                        [self finishWithError:error];
                                                        return ;
                                                    }
                                                    [self setState:SNFirmwareLoaderStateWritingBT];
                                                    [self writePacketsOfBytesfromFirmware:self.firmwareBootloaderBT withOffset:0 withCompleteBlock:^(NSError *error) {
                                                        NSLog(@"BINARY WITH ERROR: %@", error);
                                                        if (error) {
                                                            [self finishWithError:error];
                                                            return ;
                                                        }
                                                        [self setState:SNFirmwareLoaderStateInitingBootloader];
                                                        [[SNWheel currentWheel] setApplyBootImageWithCompleteBlock:^(NSError *error) {
                                                            NSLog(@"APPLY BOOT IMG WITH ERROR: %@", error);
                                                            if (error) {
                                                                [self finishWithError:error];
                                                                return ;
                                                            }
                                                            [self finishWithError:nil];
                                                        }];
                                                    }];
                                                }];
                                            }];
                                        }
                                    }];
                                }];
                            }];
                        }];
                    }];
                }];
            }
        }];
    }];
}

- (void)writePacketsOfBytesfromFirmware:(SNFirmware *)firmware withOffset:(NSInteger)offset withCompleteBlock:(WritingValueCompleteBlock)completeBlock
{
    if (offset >= firmware.data.length)
    {
        if (completeBlock)
            completeBlock(nil);
        return;
    }
    [self.delegate progressChanged:(double)offset/firmware.data.length];
    [[SNWheel currentWheel] writeFirmwareChunk:[firmware.data subdataWithRange:NSMakeRange(offset, MIN(kFirmwareBinaryPacketSize, firmware.data.length - offset))]
                                   chunkOffset:offset + firmware.firmwareOffset
                             withCompleteBlock:^(NSError *error) {
                                 if (error) {
                                     if (completeBlock) {
                                         completeBlock(error);
                                     }
                                     return ;
                                 }
                                 [self writePacketsOfBytesfromFirmware:firmware withOffset:offset + MIN(kFirmwareBinaryPacketSize, firmware.data.length - offset) withCompleteBlock:completeBlock];
                             }];
}

- (void)finishWithError:(NSError *)error
{
    NSLog(@"firmware bt bytes %lu, wheel bytes %lu", self.firmwareBT.data.length, self.firmwareWheel.data.length);
    NSLog(@"%@", [[SNStatistics sharedInstance] description]);
    [self setState:SNFirmwareLoaderStateDone];
    [self setFirmwareBT:nil];
    [self setFirmwareWheel:nil];
    [self setFirmwareBootloaderBT:nil];
    [self setFirmwareBootloaderWheel:nil];
    [self.delegate finishedWithError:error];
}

@end
