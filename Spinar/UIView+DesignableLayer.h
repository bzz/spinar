//
//  UIView+DesignableLayer.h
//  Cricket
//
//  Created by Duhov Filipp on 5/27/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

IB_DESIGNABLE

@interface UIView (DesignableLayer)

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable UIColor *borderColor;

@end
