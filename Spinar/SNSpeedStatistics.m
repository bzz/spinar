//
//  SNSpeedStatistics.m
//  Spinar
//
//  Created by Hena Sofronov on 6/7/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNSpeedStatistics.h"
#import "SNWheel.h"

#define STATS_COUNT 15

@interface SNSpeedStatistics ()
@property (strong, nonatomic) NSMutableArray<NSNumber *> *results;
@property (strong, nonatomic) NSTimer *timer;
@end

@implementation SNSpeedStatistics

+ (instancetype)sharedInstance {
    static SNSpeedStatistics *manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self clearStats];
    }
    return self;
}

- (void)startStats
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:30/STATS_COUNT target:self selector:@selector(updateStats) userInfo:nil repeats:YES];
}

- (void)clearStats
{
    _results = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < STATS_COUNT; i++) {
        [self.results addObject:@0];
    }
}

- (void)updateStats
{
    double speed = [[SNWheel currentWheel] speed];
    [self.results removeObjectAtIndex:0];
    [self.results addObject:@(speed)];
}

- (NSArray<NSNumber *> *)lastResults
{
    return [self.results copy];
}

- (double)average
{
    double sum = 0;
    for (NSNumber *num in self.results) {
        sum += num.doubleValue;
    }
    return sum / self.results.count;
}

@end
