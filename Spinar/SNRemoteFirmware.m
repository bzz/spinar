//
//  SNRemoteFirmware.m
//  Spinar
//
//  Created by Hena Sofronov on 3/28/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNRemoteFirmware.h"
#import "FTPManager.h"

static NSString * const kServerEndpoint = @"139.162.207.77";
static NSString * const kBTPathComponent = @"BIN/BTB";
static NSString * const kWheelPathComponent = @"BIN/MCB";
static NSString * const kBTBootloaderPathComponent = @"BIN/BTB";
static NSString * const kWheelBootloaderPathComponent = @"BIN/MCB";
static NSString * const kMaunalPathComponent = @"MobApp";

static NSString * const kUsername = @"uniwheel1";
static NSString * const kPassword = @"@uniwheel123";

@interface SNRemoteFirmware()

@property (strong, nonatomic) FMServer *firmwareServerBT;
@property (strong, nonatomic) FMServer *firmwareServerWheel;
@property (strong, nonatomic) FMServer *firmwareServerBTBootloader;
@property (strong, nonatomic) FMServer *firmwareServerWheelBootloader;
@property (strong, nonatomic) FMServer *manualServer;
@property (strong, nonatomic) FTPManager *ftpManager;

@end

@implementation SNRemoteFirmware

- (id)init
{
    self = [super init];
    if (self) {
        _firmwareServerBT = [FMServer serverWithDestination:[kServerEndpoint stringByAppendingPathComponent:kBTPathComponent] username:kUsername password:kPassword];
        _firmwareServerWheel = [FMServer serverWithDestination:[kServerEndpoint stringByAppendingPathComponent:kWheelPathComponent] username:kUsername password:kPassword];
        _firmwareServerBTBootloader = [FMServer serverWithDestination:[kServerEndpoint stringByAppendingPathComponent:kBTBootloaderPathComponent] username:kUsername password:kPassword];
        _firmwareServerWheelBootloader = [FMServer serverWithDestination:[kServerEndpoint stringByAppendingPathComponent:kWheelBootloaderPathComponent] username:kUsername password:kPassword];
        _manualServer = [FMServer serverWithDestination:[kServerEndpoint stringByAppendingPathComponent:kMaunalPathComponent] username:kUsername password:kPassword];
        _ftpManager = [[FTPManager alloc] init];
    }
    return self;
}

- (FMServer *)serverForType:(SNFirmwareType)firmwareType
{
    if (firmwareType == SNFirmwareTypeBT) {
        return self.firmwareServerBT;
    }
    if (firmwareType == SNFirmwareTypeWheel) {
        return self.firmwareServerWheel;
    }
    if (firmwareType == SNFirmwareTypeBootloaderBT) {
        return self.firmwareServerBTBootloader;
    }
    if (firmwareType == SNFirmwareTypeBootloaderWheel) {
        return self.firmwareServerWheelBootloader;
    }
    return nil;
}

- (NSString *)firmwareFilenameForType:(SNFirmwareType)firmwareType
{
    FMServer *server = [self serverForType:firmwareType];
    if (!server) {
        return nil;
    }
    NSArray *content = [self.ftpManager contentsOfServer:server];
    NSString *latestVersionFileName = nil;
    SNFirmwareVersion maxVersion;
    maxVersion.major = 0;
    maxVersion.minor = 0;
    maxVersion.build = 0;
    for (NSDictionary *fileInfo in content) {
        NSString *fileName = fileInfo[(__bridge NSString *)kCFFTPResourceName];
        if ([SNFirmware typeForFirmwareName:fileName] == firmwareType) {
            SNFirmwareVersion fileNameVersion = [SNFirmware versionForFirmwareName:fileName];
            if ([SNFirmware compareVersion:maxVersion andVerion:fileNameVersion] == NSOrderedAscending) {
                maxVersion = fileNameVersion;
                latestVersionFileName = fileName;
            }
        }
    }
    return latestVersionFileName;
}

- (void)versionForType:(SNFirmwareType)firmwareType withCompleteBlock:(FirmwareVersionCompleteBlock)completeBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        SNFirmwareVersion version = [SNFirmware versionForFirmwareName:[self firmwareFilenameForType:firmwareType]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completeBlock) {
                completeBlock(YES, version);
            }
        });
    });
}

- (SNFirmwareVersion)versionForType:(SNFirmwareType)firmwareType
{
    SNFirmwareVersion version = [SNFirmware versionForFirmwareName:[self firmwareFilenameForType:firmwareType]];
    return version;
}

- (SNFirmware *)firmwareForType:(SNFirmwareType)firmwareType
{
    NSString *filename = [self firmwareFilenameForType:firmwareType];
    BOOL success = [self.ftpManager downloadFile:filename
                                     toDirectory:[NSURL fileURLWithPath:NSTemporaryDirectory()]
                                      fromServer:[self serverForType:firmwareType]];
    if (!success) {
        return nil;
    }
    SNFirmware *firmware = [SNFirmware firmwareWithFilePath:[NSTemporaryDirectory() stringByAppendingPathComponent:filename]];
    return firmware;
}

- (NSString *)manualPath
{
    NSString *filename = @"manual.pdf";
    BOOL success = [self.ftpManager downloadFile:filename
                                     toDirectory:[NSURL fileURLWithPath:NSTemporaryDirectory()]
                                      fromServer:self.manualServer];
    if (!success) {
        return nil;
    }
    return [NSTemporaryDirectory() stringByAppendingPathComponent:filename];
}

@end
