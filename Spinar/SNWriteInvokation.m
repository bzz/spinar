//
//  SNWriteInvokation.m
//  Spinar
//
//  Created by Hena Sofronov on 3/22/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNWriteInvokation.h"
#import "SNStatistics.h"

@interface SNWriteInvokation ()

@property (strong, nonatomic) CBPeripheral *peripheral;
@property (strong, nonatomic) CBCharacteristic *characteristic;
@property (strong, nonatomic) NSData *data;
@property (copy, nonatomic) WriteCompleteBlock completeBlock;

@end

@implementation SNWriteInvokation

- (id)initWithPeripheral:(CBPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic data:(NSData *)data writeCompletion:(WriteCompleteBlock)completeBlock
{
    self = [super init];
    if (self) {
        _peripheral = peripheral;
        _characteristic = characteristic;
        _data = data;
        _completeBlock = completeBlock;
        _state = SNWriteInvokationStateWaiting;
    }
    return self;
}

- (void)invoke
{
    _state = SNWriteInvokationStateInvoking;
    NSLog(@"set ch:%@ val:%@", self.characteristic.UUID.UUIDString, self.data);
    self.data.length > 16 ? [[SNStatistics sharedInstance] startDataWriteInvokation] : [[SNStatistics sharedInstance] startWriteInvokation];
    [self.peripheral writeValue:self.data forCharacteristic:self.characteristic type:CBCharacteristicWriteWithResponse];
}

- (void)finishWithError:(NSError *)error
{
    self.data.length > 16 ? [[SNStatistics sharedInstance] finishDataWriteInvokation] : [[SNStatistics sharedInstance] finishWriteInvokation];
    _state = SNWriteInvokationStateDone;
    if (self.completeBlock) {
        self.completeBlock(error);
    }
    [self.delegate invokationDone:self];
}

@end
