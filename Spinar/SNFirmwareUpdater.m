//
//  SNFirmwareUpdater.m
//  Spinar
//
//  Created by Hena Sofronov on 5/5/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNFirmwareUpdater.h"
#import "SNFirmwareLoader.h"
#import "SNRemoteFirmware.h"
#import "SNWheel.h"

@interface SNFirmwareUpdater ()<SNFirmwareLoaderDelegate>

@property (strong, nonatomic, readonly) SNFirmwareLoader *loader;
@property (strong, nonatomic, readonly) SNRemoteFirmware *remote;

@end

@implementation SNFirmwareUpdater

+ (instancetype)sharedUpdater {
    static SNFirmwareUpdater *manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _loader = [[SNFirmwareLoader alloc] init];
        [_loader setDelegate:self];
        _remote = [[SNRemoteFirmware alloc] init];
        _stage = SNFirmwareUpdaterStageFinish;
        _firmwaresToUpdate = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)continueLoadingAfterReboot
{
    if (self.stage == SNFirmwareUpdaterStageWaitingForRebootAfterBootloader) {
        [self goNextStage];
        return;
    }
    [self setStage:SNFirmwareUpdaterStageBootloaderFirmware];
}

- (void)setState:(SNFirmwareUpdaterState)state
{
    _state = state;
    [self.delegate stateChanged:state];
}

- (void)setStage:(SNFirmwareUpdaterStage)stage
{
    _stage = stage;
    switch (stage) {
        case SNFirmwareUpdaterStageCheckingVersions:
            [self checkVersions];
            break;
        case SNFirmwareUpdaterStageLockWheelForLoad:
            [self lockForProgramming];
            break;
        case SNFirmwareUpdaterStageWaitingForRebootAfterLocking:
            [self setState:SNFirmwareUpdaterStateWaitingForReboot];
            break;
        case SNFirmwareUpdaterStageBootloaderFirmware:
            [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
            [self startWritingBootloader];
            break;
        case SNFirmwareUpdaterStageWaitingForRebootAfterBootloader:
            [self setState:SNFirmwareUpdaterStateWaitingForReboot];
            break;
        case SNFirmwareUpdaterStageFirmware:
            [self startWritingFirmware];
            break;
        case SNFirmwareUpdaterStageFinish:
            [[SNWheel currentWheel] setState:SNWheelStateReady];
            [self setState:SNFirmwareUpdaterStateDone];
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            break;
        default:
            break;
    }
}

- (void)goNextStage
{
    if (self.stage == SNFirmwareUpdaterStageFinish) {
        [self setStage:SNFirmwareUpdaterStageCheckingVersions];
        return;
    }
    [self setStage:self.stage + 1];
}

- (void)start
{
    _stage = SNFirmwareUpdaterStageFinish;
    [self goNextStage];
}

- (void)updateFirmwaresToUpdateWithBlock:(CheckUpdatesCompleteBlock)completeBlock
{
    [[SNWheel currentWheel] updatefirmwareVersionWheelWithCompleteBlock:^(NSError *error) {
        if (error) {
            if (completeBlock) {
                completeBlock(error);
            }
            return ;
        }
        [[SNWheel currentWheel] updatefirmwareVersionBTWithCompleteBlock:^(NSError *error) {
            if (error) {
                if (completeBlock) {
                    completeBlock(error);
                }
                return ;
            }
            SNFirmwareVersion oldVersionBt = [[SNWheel currentWheel] firmwareVersionBT];
            SNFirmwareVersion oldVersionWheel = [[SNWheel currentWheel] firmwareVersionWheel];
            __block SNFirmwareVersion newVersionBt;
            [self.remote versionForType:SNFirmwareTypeBT
                      withCompleteBlock:^(BOOL isSuccess, SNFirmwareVersion version) {
                          newVersionBt = version;
                          __block SNFirmwareVersion newVersionWheel;
                          [self.remote versionForType:SNFirmwareTypeWheel
                                    withCompleteBlock:^(BOOL isSuccess, SNFirmwareVersion version) {
                                        newVersionWheel = version;
                                        BOOL isBtNeeded = [SNFirmware compareVersion:oldVersionBt andVerion:newVersionBt] == NSOrderedAscending;
                                        BOOL isWheelNeeded = [SNFirmware compareVersion:oldVersionWheel andVerion:newVersionWheel] == NSOrderedAscending;
                                        NSMutableSet *neededFirmwares = [[NSMutableSet alloc] init];
                                        if (isBtNeeded) {
                                            [neededFirmwares addObject:@(SNFirmwareTypeBT)];
                                        }
                                        if (isWheelNeeded) {
                                            [neededFirmwares addObject:@(SNFirmwareTypeWheel)];
                                        }
                                        _firmwaresToUpdate = [neededFirmwares copy];
                                        if (completeBlock) {
                                            completeBlock(nil);
                                        }
                                    }];
                      }];
        }];
    }];
}

- (void)checkVersions
{
    [self setState:SNFirmwareUpdaterStateCheckingFirmwaresVersions];
    [self updateFirmwaresToUpdateWithBlock:^(NSError *error) {
        if (error) {
            [self setStage:SNFirmwareUpdaterStageFinish];
            [self.delegate finishedWithError:error];
            return;
        }
        if (!self.firmwaresToUpdate.count) {
            [self setStage:SNFirmwareUpdaterStageFinish];
            [self.delegate finishedWithError:[NSError errorWithDomain:@"Firmware" code:1 userInfo:@{NSLocalizedDescriptionKey: @"No update needed"}]];
            return ;
        }
        [self goNextStage];
    }];
}

- (void)lockForProgramming
{
    [self.loader lockForProgramming];
}

- (void)lockForProgrammingOnlyBt
{
    [self.loader lockForProgrammingOnlyBt];
}

- (void)startWritingBootloader
{
    [self setState:SNFirmwareUpdaterStateDownloadingFirmware];
    SNFirmware *firmwareBootloaderBt = [self.remote firmwareForType:SNFirmwareTypeBootloaderBT];
    SNFirmware *firmwareBootloaderWheel = [self.remote firmwareForType:SNFirmwareTypeBootloaderWheel];
    [self setState:SNFirmwareUpdaterStateInstallingFirmware];
    [self.loader startLoadingBootloaderWheelFirmware:firmwareBootloaderWheel andBTFirmware:firmwareBootloaderBt];
}

- (void)startWritingFirmware
{
    if (!self.firmwaresToUpdate.count) {
        _firmwaresToUpdate = [NSSet setWithArray:@[@(SNFirmwareTypeBT), @(SNFirmwareTypeWheel)]];
    }
    [self setState:SNFirmwareUpdaterStateDownloadingFirmware];
    SNFirmware *firmwareBt = [self.firmwaresToUpdate containsObject:@(SNFirmwareTypeBT)] ? [self.remote firmwareForType:SNFirmwareTypeBT] : nil;
    SNFirmware *firmwareWheel = [self.firmwaresToUpdate containsObject:@(SNFirmwareTypeWheel)] ? [self.remote firmwareForType:SNFirmwareTypeWheel] : nil;
    [self setState:SNFirmwareUpdaterStateInstallingFirmware];
    [self.loader startLoadingWheelFirmware:firmwareWheel andBTFirmware:firmwareBt];
}

#pragma mark - SNFirmwareLoaderDelegate

- (void)finishedWithError:(NSError *)error
{
    if (error) {
        [self setStage:SNFirmwareUpdaterStageFinish];
        [self.delegate finishedWithError:error];
        return;
    }
    [self goNextStage];
    if (self.state == SNFirmwareUpdaterStateDone) {
        _firmwaresToUpdate = [NSSet set];
        [self.delegate finishedWithError:nil];
    }
}

- (void)stateChanged:(SNFirmwareLoaderState)state
{
    switch (state) {
        case SNFirmwareLoaderStateLocking:
        case SNFirmwareLoaderStateInitingBootloader:
        case SNFirmwareLoaderStateDone:
            [self setState:SNFirmwareUpdaterStateInstallingFirmware];
            break;
        case SNFirmwareLoaderStateWaitingForReboot:
            [self setState:SNFirmwareUpdaterStateWaitingForReboot];
            break;
        case SNFirmwareLoaderStateWritingBT:
            [self setState:(self.stage == SNFirmwareUpdaterStageBootloaderFirmware) ? SNFirmwareUpdaterStateWritingBtBootloader : SNFirmwareUpdaterStateWritingBt];
            break;
        case SNFirmwareLoaderStateWritingWheel:
            [self setState:(self.stage == SNFirmwareUpdaterStageBootloaderFirmware) ? SNFirmwareUpdaterStateWritingWheelBootloader : SNFirmwareUpdaterStateWritingWheel];
            break;
        default:
            break;
    }
}

- (void)progressChanged:(double)progress
{
    _progress = progress;
    [self.delegate progressChanged:self.progress];
}

@end
