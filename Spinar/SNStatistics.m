//
//  SNStatistics.m
//  Spinar
//
//  Created by Hena Sofronov on 4/19/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNStatistics.h"

@interface SNStatistics()
@property (assign, nonatomic) NSUInteger packetsCount;
@property (strong, nonatomic) NSDate *firstPacketDate;
@property (assign, nonatomic) NSTimeInterval totalWaitingCount;
@property (assign, nonatomic) NSTimeInterval totalDataWainingCount;

@property (strong, nonatomic) NSDate *lastPacketDate;
@end

@implementation SNStatistics

+ (instancetype)sharedInstance {
    static SNStatistics *manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self resetStatistics];
    }
    return self;
}

- (void)resetStatistics
{
    _packetsCount = 0;
    _totalWaitingCount = 0;
    _totalDataWainingCount = 0;
    _firstPacketDate = nil;
}

- (void)startWriteInvokation
{
    if (!self.firstPacketDate) {
        self.firstPacketDate = [NSDate date];
    }
    self.lastPacketDate = [NSDate date];
}

- (void)finishWriteInvokation
{
    self.packetsCount ++;
    self.totalWaitingCount -= [self.lastPacketDate timeIntervalSinceNow];
}

- (void)startDataWriteInvokation
{
    if (!self.firstPacketDate) {
        self.firstPacketDate = [NSDate date];
    }
    self.lastPacketDate = [NSDate date];
}

- (void)finishDataWriteInvokation
{
    self.packetsCount ++;
    self.totalDataWainingCount -= [self.lastPacketDate timeIntervalSinceNow];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"\ntotal packets: %lu\nwaiting for write confirmation: %f\nwaiting for data write confirmation: %f\ntotal time since first packet: %f",self.packetsCount, self.totalWaitingCount, self.totalDataWainingCount, -[self.firstPacketDate timeIntervalSinceNow]];
}


@end
