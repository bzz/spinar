//
//  SNSidebarTableViewController.m
//  Spinar
//
//  Created by Bohdan Pozharskyi on 11/18/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "SNSidebarTableViewController.h"
#import "SNWheelConnector.h"
#import "UIAlertView+SNMessage.h"
#import "SNWheel.h"

@interface SNSidebarTableViewController () <SNWheelConnectorDelegate>

@property (strong, nonatomic) NSArray *discoveredDevices;
@property (strong, nonatomic) SNDevice *connectedDevice;
- (IBAction)cancelButtonPressed:(id)sender;

@end

@implementation SNSidebarTableViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[SNWheelConnector sharedInstance] setDelegate:self];
    [self setDiscoveredDevices:[[SNWheelConnector sharedInstance] discoveredDevices]];
    [self setConnectedDevice:[[SNWheelConnector sharedInstance] connectedDevice]];
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    SNBLEState bleState = [[SNWheelConnector sharedInstance] bleState];
    if (bleState == SNBLEStateOff || bleState == SNBLEStateUnavailable) {
        [UIAlertView showMessage:bleState == SNBLEStateOff ? @"Please enable bluetooth on your device" : @"Bluetooth unavailable" withHeader:@"Bluetooth"];
        [self dismissViewControllerAnimated:YES completion:NULL];
        return;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - SNBLEManagerDelegate

- (void)devicesUpdated
{
    [self setDiscoveredDevices:[[SNWheelConnector sharedInstance] discoveredDevices]];
    [self setConnectedDevice:[[SNWheelConnector sharedInstance] connectedDevice]];
    [self.tableView reloadData];
    if (self.connectedDevice) {
        [[SNWheel currentWheel] writePassword:[@"123456" dataUsingEncoding:NSUTF8StringEncoding] withCompleteBlock:^(NSError *error) {
            if (error) {
                [UIAlertView showMessage:@"Wheel is not available now" withHeader:@"Error"];
                return ;
            }
            [self dismissViewControllerAnimated:YES completion:NULL];
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Discovering...";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.discoveredDevices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SNDeviceCell" forIndexPath:indexPath];
    [cell.textLabel setText:[(SNDevice *)self.discoveredDevices[indexPath.row] name]];
    [cell.detailTextLabel setText:[(SNDevice *)self.discoveredDevices[indexPath.row] uuid]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[SNWheelConnector sharedInstance] connectDevice:self.discoveredDevices[indexPath.row]];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
