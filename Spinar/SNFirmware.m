//
//  SNFirmware.m
//  Spinar
//
//  Created by hena on 9/14/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNFirmware.h"
#import "SNDataToIntegerConverter.h"

@implementation SNFirmware

+ (NSComparisonResult)compareVersion:(SNFirmwareVersion)version1 andVerion:(SNFirmwareVersion)version2
{
//    return NSOrderedAscending;
    if (version1.major != version2.major) {
        return version1.major < version2.major ? NSOrderedAscending : NSOrderedDescending;
    }
    if (version1.minor != version2.minor) {
        return version1.minor < version2.minor ? NSOrderedAscending : NSOrderedDescending;
    }
    if (version1.build != version2.build) {
        return version1.build < version2.build ? NSOrderedAscending : NSOrderedDescending;
    }
    return NSOrderedSame;
}


- (id)initWithData:(NSData *)data type:(SNFirmwareType)type version:(SNFirmwareVersion)version {
    self = [super init];
    if (self) {
        _firmwareOffset = [SNDataToIntegerConverter unsignedIntegerFromData:[data subdataWithRange:NSMakeRange(0, 4)]];
        _data = [data subdataWithRange:NSMakeRange(4, data.length - 4)];
        _type = type;
        _version = version;
    }
    return self;
}

+ (SNFirmwareVersion)versionForFirmwareName:(NSString *)fileName
{
    SNFirmwareVersion version;
    version.major = 0;
    version.minor = 0;
    version.build = 0;
    if (![self isPathValid:fileName]) {
        return version;
    }
    NSArray<NSString *> *items = [fileName componentsSeparatedByString:@"_"];
    for (NSString *item in items) {
        if ([item hasPrefix:@"v"]) {
            NSArray<NSString *> *versionItems = [[item substringFromIndex:1] componentsSeparatedByString:@"."];
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            if (versionItems.count >= 1)
                version.major = [[numberFormatter numberFromString:versionItems[0]] integerValue];
            if (versionItems.count >= 2)
                version.minor = [[numberFormatter numberFromString:versionItems[1]] integerValue];
            if (versionItems.count >= 3)
                version.build = [[numberFormatter numberFromString:versionItems[2]] integerValue];
            return version;
        }
    }
    return version;
}

+ (SNFirmwareType)typeForFirmwareName:(NSString *)fileName
{
    NSDictionary *typeForPrefix = @{@"whl_firm_mcb": @(SNFirmwareTypeWheel),
                                    @"whl_firm_btb": @(SNFirmwareTypeBT),
                                    @"whl_boot_mcb": @(SNFirmwareTypeBootloaderWheel),
                                    @"whl_boot_btb": @(SNFirmwareTypeBootloaderBT)};
    for (NSString *prefix in [typeForPrefix allKeys]) {
        if ([fileName hasPrefix:prefix]) {
            return (SNFirmwareType)[typeForPrefix[prefix] integerValue];
        }
    }
    return SNFirmwareTypeUndefined;
}

+ (BOOL)isPathValid:(NSString *)path {
    NSString *fileName = [path lastPathComponent];
    NSArray *items = [fileName componentsSeparatedByString:@"_"];
    if (![items.firstObject isEqualToString:@"whl"])
        return NO;
    return YES;
}

+ (instancetype)firmwareWithFilePath:(NSString *)path {
    if (![self isPathValid:path]) {
        return nil;
    }
    SNFirmware *instance = [[self alloc] initWithData:[NSData dataWithContentsOfFile:path] type:[self typeForFirmwareName:[path lastPathComponent]] version:[self versionForFirmwareName:[path lastPathComponent]]];
    
    return instance;
}

@end
