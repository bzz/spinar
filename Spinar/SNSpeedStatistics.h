//
//  SNSpeedStatistics.h
//  Spinar
//
//  Created by Hena Sofronov on 6/7/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNSpeedStatistics : NSObject

+ (instancetype)sharedInstance;

- (void)startStats;
- (void)clearStats;
- (NSArray<NSNumber *> *)lastResults;
- (double)average;
@end
