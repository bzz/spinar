//
//  SNSegmentView.m
//  Spinar
//
//  Created by Duhov Filipp on 7/20/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNSegmentView.h"
#import "SNAnimatableShapeLayer.h"
#import "CGCustomPaths.h"
#import "CGRectOffset.h"

@interface SNSegmentView ()

@property (nonatomic, strong) SNAnimatableShapeLayer *segmentLayer;

@end

@implementation SNSegmentView

- (void)setSegmentColor:(UIColor *)segmentColor
{
    _segmentColor = segmentColor;
    self.segmentLayer.fillColor = segmentColor.CGColor;
}

- (void)setProgress:(CGFloat)progress
{
    progress = MIN(1, MAX(0, progress));
    if (_progress != progress) {
        _progress = progress;
        [self setNeedsLayout];
    }
}

#pragma mark - Initialization

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self customInit];
    }
    return self;
}

- (void)customInit
{
    __weak typeof(self) weakSelf = self;

    self.segmentLayer = [SNAnimatableShapeLayer layer];
    self.segmentLayer.bounds = self.layer.bounds;
    self.segmentLayer.pathGeneratorBlock = ^CGPathRef(CGFloat progress) {
        CGFloat startAngle = SNAngleWithDegrees(weakSelf.startDegree);
        CGFloat endAngle = SNAngleWithDegrees(weakSelf.endDegree);
        return CGPathSegment(weakSelf.bounds, startAngle, (endAngle + 2 * M_PI - startAngle) * progress + startAngle, weakSelf.segmentThickness);
    };
    [self.layer addSublayer:self.segmentLayer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.segmentLayer.progress = self.progress;
}

@end
