//
//  VRSegmentedControl.h
//  VideoReverse
//
//  Created by Duhov Filipp on 3/18/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

typedef void (^VRIndexChangeBlock)(NSUInteger index, BOOL animated);
typedef void (^VRSameIndexBlock)(BOOL isHighlighted);

typedef NS_ENUM(NSUInteger, VRSegmentedControlSelectionType) {
    VRSegmentedControlSelectionTypeTemplate,
    VRSegmentedControlSelectionTypeBackground,
};

IB_DESIGNABLE

@interface VRSegmentedControl : UIControl

@property (nonatomic, strong) IBInspectable UIColor *selectedTintColor;
@property (nonatomic, assign) VRSegmentedControlSelectionType selectionType;
@property (nonatomic, readonly, weak) UIView *contentView;
@property (nonatomic, readonly) NSUInteger segmentsCount;
@property (nonatomic, assign) IBInspectable NSUInteger selectedIndex;
- (void)setSelectedIndex:(NSUInteger)selectedIndex animated:(BOOL)animated;
@property (nonatomic, copy) VRIndexChangeBlock indexChangeBlock;
@property (nonatomic, assign, getter=isSelectedSegmentHighlighted) BOOL selectedSegmentHighlighted;
@property (nonatomic, copy) VRSameIndexBlock sameIndexBlock;
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;

@end
