//
//  SNWriteInvokation.h
//  Spinar
//
//  Created by Hena Sofronov on 3/22/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNBLEManager.h"

typedef NS_ENUM(NSInteger, SNWriteInvokationState) {
    SNWriteInvokationStateWaiting,
    SNWriteInvokationStateInvoking,
    SNWriteInvokationStateDone
};

@protocol SNWriteInvokationDelegate <NSObject>

- (void)invokationDone:(id)sender;
@end

@interface SNWriteInvokation : NSObject

- (id)initWithPeripheral:(CBPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic data:(NSData *)data writeCompletion:(WriteCompleteBlock)completeBlock;
@property (weak, nonatomic) id<SNWriteInvokationDelegate> delegate;
@property (assign, nonatomic, readonly) SNWriteInvokationState state;
- (void)invoke;
- (void)finishWithError:(NSError *)error;
@end
