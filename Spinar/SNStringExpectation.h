//
//  SNStringExpectation.h
//  Spinar
//
//  Created by hena on 10/13/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNExpectation.h"

@interface SNStringExpectation : SNExpectation
- (id)initWithCharacteristic:(SNCharacteristicType)characteristic stringBlock:(WriteWithStringResponseCompleteBlock)block useTimeout:(BOOL)isTimeout;
- (NSString *)stringFromData:(NSData *)data;

@end
