//
//  CGRectOffset.h
//  Spinar
//
//  Created by Duhov Filipp on 7/20/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#ifndef Spinar_CGRectOffset_h
#define Spinar_CGRectOffset_h

static inline CGRect CGRectWithOffset(CGRect rect, CGFloat offset)
{
    rect.origin.x = rect.origin.y = offset;
    rect.size.width -= offset * 2;
    rect.size.height -= offset * 2;
    return rect;
}

static inline CGFloat SNAngleWithDegrees(CGFloat degrees)
{
    return degrees / 180 * M_PI;
}

#endif
