//
//  SNAnimatableShapeLayer.m
//  Spinar
//
//  Created by Duhov Filipp on 7/18/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNAnimatableShapeLayer.h"
#import "UIView+AnimatedProperty.h"

NSString *const kProgressKeyPath = @"progress";

@interface SNAnimatableShapeLayer () {
    CGFloat _progress;
}

@property (nonatomic, weak) SNAnimatableShapeLayer *originalLayer;

@end

@implementation SNAnimatableShapeLayer

@dynamic progress;

- (instancetype)initWithLayer:(id)layer
{
    if ((self = [super initWithLayer:layer]) && [layer isKindOfClass:[self class]]) {
        self.originalLayer = layer;
    }
    return self;
}

- (void)setProgress:(CGFloat)progress
{
    if ([UIView currentAnimation]) {
        [[UIView currentAnimation] animateLayer:self keyPath:kProgressKeyPath toValue:@(progress)];
    }
    else {
        [self setValue:@(progress) forKeyPath:kProgressKeyPath];
    }
    _progress = progress;
}

- (CGFloat)progress
{
    return _progress;
}

+ (BOOL)needsDisplayForKey:(NSString *)key
{
    if ([key isEqualToString:kProgressKeyPath]) {
        return YES;
    }
    else {
        return [super needsDisplayForKey:key];
    }
}

- (void)setValue:(id)value forKeyPath:(NSString *)keyPath
{
    if ([keyPath isEqualToString:kProgressKeyPath]) {
        if (self.originalLayer) {
            [self.originalLayer setValue:value forKey:keyPath];
        }
        else if (self.pathGeneratorBlock && ![UIView currentAnimation]) {
            CGAffineTransform translation = CGAffineTransformMakeTranslation(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
            self.path = CGPathCreateCopyByTransformingPath(self.pathGeneratorBlock([value floatValue]), &translation);
        }
    }
    else {
        [super setValue:value forKeyPath:keyPath];
    }
}

@end
