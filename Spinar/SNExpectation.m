//
//  SNExpectation.m
//  Spinar
//
//  Created by hena on 9/9/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNExpectation.h"

#define TIMEOUT 10

@interface SNExpectation ()
@property (strong, nonatomic) NSTimer *timeoutTimer;

@property (assign, nonatomic) SNCharacteristicType requestedCharacteristic;
@end

@implementation SNExpectation
- (id)initWithCharacteristic:(SNCharacteristicType)characteristic useTimeout:(BOOL)isTimeout {
    self = [super init];
    if (self) {
        _requestedCharacteristic = characteristic;
        if (isTimeout) {
            _timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:TIMEOUT target:self selector:@selector(timeoutInvoke) userInfo:nil repeats:NO];
        }
    }
    return self;
}

- (BOOL)isExpectCharacteristic:(SNCharacteristicType)characteristic {
    return self.requestedCharacteristic == characteristic;
}

- (void)timeoutInvoke {
    NSLog(@"EXP TIMEOUT %ld", (long)self.requestedCharacteristic);
    [self invokeForCharacteristic:self.requestedCharacteristic binaryData:nil error:[NSError errorWithDomain:@"Expectation" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Timeout"}]];
}

- (BOOL)invokeForCharacteristic:(SNCharacteristicType)characteristic binaryData:(NSData *)data error:(NSError *)error {
    if (characteristic != self.requestedCharacteristic)
        return NO;
    [self.timeoutTimer invalidate];
    return YES;
}
@end
