//
//  SNDiscoveredDevice.h
//  Spinar
//
//  Created by hena on 10/12/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNDevice : NSObject

- (id)initWithUUID:(NSString *)uuid andName:(NSString *)name;

@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) BOOL isReady;

@end
