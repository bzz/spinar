//
//  SNSidebarTableViewController.h
//  Spinar
//
//  Created by Bohdan Pozharskyi on 11/18/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNSidebarTableViewController : UITableViewController

@end
