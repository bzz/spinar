//
//  SNValueExpectation.h
//  Spinar
//
//  Created by hena on 10/13/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNExpectation.h"

@interface SNValueExpectation : SNExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic
                    isSigned:(BOOL)isSigned
                  valueBlock:(WriteWithValueResponseCompleteBlock)block
                  useTimeout:(BOOL)isTimeout;
- (double)valueFromData:(NSData *)data;

@end
