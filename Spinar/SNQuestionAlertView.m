//
//  SNQuestionAlertView.m
//  Spinar
//
//  Created by hena on 10/7/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNQuestionAlertView.h"

@implementation SNQuestionAlertView

+ (void)showQuestion:(NSString *)question okButton:(NSString *)okString noButton:(NSString *)noString withHeader:(NSString *)header completeBlock:(YesNoCompletion)completeBlock
{
    SNQuestionAlertView *alert = [[SNQuestionAlertView alloc] initWithTitle:header message:question delegate:nil cancelButtonTitle:okString otherButtonTitles:noString, nil];
    [alert setDelegate:alert];
    [alert setCompleteBlock:completeBlock];
    [alert show];
}

+ (void)showQuestion:(NSString *)question completeBlock:(YesNoCompletion)completeBlock {
    [self showQuestion:question okButton:@"Yes" noButton:@"No" withHeader:@"Spinar" completeBlock:completeBlock];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (self.completeBlock)
        self.completeBlock([alertView cancelButtonIndex] == buttonIndex);
}
@end
