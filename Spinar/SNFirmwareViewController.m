//
//  SNFirmwareViewController.m
//  Spinar
//
//  Created by hena on 6/8/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNFirmwareViewController.h"
#import "SNWheel.h"
#import "UIAlertView+SNMessage.h"
#import "SNFirmwareLoader.h"
#import "MBProgressHUD.h"
#import "SNDelayedInvoke.h"
#import "SNRemoteFirmware.h"

@interface SNFirmwareViewController ()<SNFirmwareLoaderDelegate>

@property (strong, nonatomic) SNFirmwareLoader *loader;
@property (strong, nonatomic) NSDictionary *textForState;
@property (strong, nonatomic) MBProgressHUD *hud;

@property (strong, nonatomic) SNFirmware *binaryWheel;
@property (strong, nonatomic) SNFirmware *binaryBT;
@property (assign, nonatomic) SNFirmwareLoaderState loaderState;

@property (strong, nonatomic) SNRemoteFirmware *remote;
@end

@implementation SNFirmwareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _loader = [[SNFirmwareLoader alloc] init];
    [self.loader setDelegate:self];
    self.textForState = @{@(SNFirmwareLoaderStateLocking): @"Locking",
                          @(SNFirmwareLoaderStateInitingBootloader): @"Initializing Bootloader",
                          @(SNFirmwareLoaderStateWaitingForReboot): @"Waiting for reboot",
                          @(SNFirmwareLoaderStateWritingWheel): @"Loading wheel",
                          @(SNFirmwareLoaderStateWritingBT): @"Loading bt",
                          @(SNFirmwareLoaderStateDone): @"Done"};
                          
    _binaryBT = [SNFirmware firmwareWithFilePath:[[NSBundle mainBundle] pathForResource:@"whl_btb_mmcu_gen0_rev_i_p_ed" ofType:@"bin"]];
    _binaryWheel = [SNFirmware firmwareWithFilePath:[[NSBundle mainBundle] pathForResource:@"whl_mcb_gen0_rev_i_p_ed" ofType:@"bin"]];
    
    _remote = [[SNRemoteFirmware alloc] init];
}
- (IBAction)loadButtonPressed:(id)sender {
    [self.loader startLoadingWheelFirmware:self.binaryWheel andBTFirmware:self.binaryBT];
}
- (IBAction)getRemoteButtonPressed:(id)sender {
    SNFirmwareVersion btVersion = [self.remote versionForType:SNFirmwareTypeBT];
    SNFirmwareVersion wheelVersion = [self.remote versionForType:SNFirmwareTypeWheel];
    NSLog(@"bt version %ld.%ld.%ld",btVersion.major, btVersion.minor, btVersion.build);
    NSLog(@"wheel version %ld.%ld.%ld",wheelVersion.major, wheelVersion.minor, wheelVersion.build);
    SNFirmware *btFirmware = [self.remote firmwareForType:SNFirmwareTypeBT];
    SNFirmware *wheelFirmware = [self.remote firmwareForType:SNFirmwareTypeWheel];
    NSLog(@"btdata count %lu", (unsigned long)btFirmware.data.length);
    NSLog(@"wheeldata count %lu", (unsigned long)wheelFirmware.data.length);
}

- (void)stateChanged:(SNFirmwareLoaderState)state
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.loaderState = state;
        if (!self.hud)
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.hud setLabelText:self.textForState[@(state)]];
        [self.hud setMode:(state == SNFirmwareLoaderStateWritingBT || state == SNFirmwareLoaderStateWritingWheel ? MBProgressHUDModeDeterminate : MBProgressHUDModeIndeterminate)];
        if (state == SNFirmwareLoaderStateWaitingForReboot) {
            [UIAlertView showMessage:@"Reboot Wheel to contine"];
        }
        if (state == SNFirmwareLoaderStateDone) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            self.hud = nil;
        }
    });
}

- (void)progressChanged:(double)progress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.hud) {
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self.hud setMode:MBProgressHUDModeDeterminate];
            [self.hud setLabelText:self.textForState[@(self.loaderState)]];
        }
        [self.hud setProgress:progress];
    });
}

- (void)finishedWithError:(NSError *)error
{
    [UIAlertView showMessage:error ? [NSString stringWithFormat:@"Finished with error %@", error.localizedDescription] : @"Successfully finished"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
