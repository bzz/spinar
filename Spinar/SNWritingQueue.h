//
//  SNWritingQueue.h
//  Spinar
//
//  Created by Hena Sofronov on 3/22/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNWriteInvokation.h"

@interface SNWritingQueue : NSObject

- (void)addWriteInvokation:(SNWriteInvokation *)writeInvokation;
- (void)finishInvokationWithError:(NSError *)error;
- (void)finishAllInvokationsWithError:(NSError *)error;

@end
