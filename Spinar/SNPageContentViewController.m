//
//  SNPageContentViewController.m
//  Spinar
//
//  Created by Duhov Filipp on 9/9/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNPageContentViewController.h"

@interface SNPageContentViewController ()

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *valueLabels;
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *distanceUnitLabels;
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *speedUnitLabels;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *timeHoursUnitLabels;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *timeMinutesUnitLabels;
- (IBAction)viewPressed:(id)sender;

@end

@implementation SNPageContentViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil pageIndex:(NSUInteger)pageIndex
{
    if (self = [super init]) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:self options:nil];
        NSArray *nibViews = [self viewsFromArray:nibObjects];
        if (pageIndex >= nibViews.count) {
            return nil;
        }
        self.view = nibViews[pageIndex];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewPressed:)];
        [self.view addGestureRecognizer:tapRecognizer];
        self.view.frame = [UIScreen mainScreen].bounds;
        _pageIndex = pageIndex;
        
        NSArray *ibCollectionKeypath = @[@"distanceUnitLabels",@"speedUnitLabels",@"timeHoursUnitLabels",@"timeMinutesUnitLabels",@"valueLabels"];
        for (NSString *keypath in ibCollectionKeypath) {
            NSMutableArray *views = [NSMutableArray array];
            for (UIView *view in [self valueForKeyPath:keypath]) {
                UIView *superview = view;
                while (superview) {
                    if ([superview isEqual:self.view]) {
                        [views addObject:view];
                        break;
                    }
                    superview = superview.superview;
                }
            }
            [self setValue:views.copy forKey:keypath];
        }
        _contentValueLabels = self.valueLabels;
    }
    return self;
}

+ (NSUInteger)pagesCountForNibWithName:(NSString *)nibNameOrNil
{
    id contentController = [[self alloc] init];
    NSArray *nibElements = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:contentController options:nil];
    return [[contentController viewsFromArray:nibElements] count];
}

- (void)viewPressed:(id)sender {
    [self.delegate pageSelected:self];
}

- (NSArray *)viewsFromArray:(NSArray *)array {
    NSMutableArray *views = [[NSMutableArray alloc] init];
    for (id element in array) {
        if ([element isKindOfClass:[UIView class]]) {
            [views addObject:element];
        }
    }
    return [views copy];
}

@end
