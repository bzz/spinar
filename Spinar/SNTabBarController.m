//
//  SNTabBarController.m
//  Spinar
//
//  Created by Duhov Filipp on 7/17/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNTabBarController.h"
#import "UIColorFromRGB.h"

static BOOL isLoggined = YES;

@implementation SNTabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBar.tintColor = UIColorFromRGB(61, 182, 226);
    for (UITabBarItem *tabBarItem in self.tabBar.items) {
//        tabBarItem.image = [tabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [tabBarItem setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateNormal];
        [tabBarItem setTitleTextAttributes:@{ NSForegroundColorAttributeName : self.tabBar.tintColor } forState:UIControlStateSelected];
        if (tabBarItem == self.tabBar.items.lastObject) {
            tabBarItem.badgeValue = [[UIApplication sharedApplication] applicationIconBadgeNumber] ? @([[UIApplication sharedApplication] applicationIconBadgeNumber]).description : nil;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!isLoggined) {
        [self performSegueWithIdentifier:@"SignUpSegue" sender:self];
        isLoggined = YES;
    }
}

@end
