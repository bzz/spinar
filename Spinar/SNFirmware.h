//
//  SNFirmware.h
//  Spinar
//
//  Created by hena on 9/14/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SNFirmwareType)  {
    SNFirmwareTypeUndefined,
    SNFirmwareTypeBT,
    SNFirmwareTypeWheel,
    SNFirmwareTypeBootloaderBT,
    SNFirmwareTypeBootloaderWheel
};

typedef struct {
    NSUInteger major;
    NSUInteger minor;
    NSUInteger build;
} SNFirmwareVersion;

@interface SNFirmware : NSObject

+ (NSComparisonResult)compareVersion:(SNFirmwareVersion)version1 andVerion:(SNFirmwareVersion)version2;


+ (SNFirmwareVersion)versionForFirmwareName:(NSString *)fileName;
+ (SNFirmwareType)typeForFirmwareName:(NSString *)fileName;

+ (instancetype)firmwareWithFilePath:(NSString *)path;

@property (readonly, assign, nonatomic) SNFirmwareVersion version;
@property (readonly, assign, nonatomic) SNFirmwareType type;
@property (readonly, assign, nonatomic) NSUInteger firmwareOffset;
@property (readonly, strong, nonatomic) NSData *data;

@end
