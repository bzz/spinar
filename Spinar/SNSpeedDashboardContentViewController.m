//
//  SNSpeedDashboardContentViewController.m
//  Spinar
//
//  Created by Hena Sofronov on 2/29/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNSpeedDashboardContentViewController.h"
#import "SNWheel.h"
#import "SNSettings.h"
#import "SNConvertor.h"


@interface SNSpeedDashboardContentViewController ()
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@end

@implementation SNSpeedDashboardContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (IBAction)lightsSwitchValueChanged:(id)sender {
    [self.delegate lightsSwitchValueChanged:self.lightsSwitch.isOn];
}

- (void)refresh
{
    [self.lightsSwitch setOn:[[SNWheel currentWheel] isLightOn]];
    NSString *distanceUnits = [SNSettings stringForDistanceUnits:[SNSettings sharedSettings].areDisplayingUnitsMiles];
    self.distanceLabel.text = distanceUnits;
    [super refresh];
}

@end
