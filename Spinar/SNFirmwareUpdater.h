//
//  SNFirmwareUpdater.h
//  Spinar
//
//  Created by Hena Sofronov on 5/5/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CheckUpdatesCompleteBlock)(NSError *error);

typedef NS_ENUM(NSInteger, SNFirmwareUpdaterState) {
    SNFirmwareUpdaterStateCheckingFirmwaresVersions,
    SNFirmwareUpdaterStateCheckingNewVersions,
    SNFirmwareUpdaterStateDownloadingFirmware,
    SNFirmwareUpdaterStateInstallingFirmware,
    SNFirmwareUpdaterStateWaitingForReboot,
    SNFirmwareUpdaterStateWritingWheelBootloader,
    SNFirmwareUpdaterStateWritingBtBootloader,
    SNFirmwareUpdaterStateWritingWheel,
    SNFirmwareUpdaterStateWritingBt,
    SNFirmwareUpdaterStateDone
};

typedef NS_ENUM(NSInteger, SNFirmwareUpdaterStage) {
    SNFirmwareUpdaterStageCheckingVersions,
    SNFirmwareUpdaterStageLockWheelForLoad,
    SNFirmwareUpdaterStageWaitingForRebootAfterLocking,
    SNFirmwareUpdaterStageBootloaderFirmware,
    SNFirmwareUpdaterStageWaitingForRebootAfterBootloader,
    SNFirmwareUpdaterStageFirmware,
    SNFirmwareUpdaterStageFinish
};


@protocol SNFirmwareUpdaterDelegate <NSObject>

- (void)stateChanged:(SNFirmwareUpdaterState)state;
- (void)progressChanged:(double)progress;
- (void)finishedWithError:(NSError *)error;

@end

@interface SNFirmwareUpdater : NSObject
+ (instancetype)sharedUpdater;

- (void)updateFirmwaresToUpdateWithBlock:(CheckUpdatesCompleteBlock)completeBlock;
//contains NSNumbers with SNFirmwareType values
@property (strong, nonatomic, readonly) NSSet *firmwaresToUpdate;

@property (weak, nonatomic) id<SNFirmwareUpdaterDelegate> delegate;
- (void)continueLoadingAfterReboot;
@property (assign, nonatomic) SNFirmwareUpdaterState state;
@property (assign, nonatomic) SNFirmwareUpdaterStage stage;
@property (assign, nonatomic, readonly) double progress;
- (void)start;

@end
