//
//  SNDataToIntegerConverter.h
//  Spinar
//
//  Created by Hena Sofronov on 3/25/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNDataToIntegerConverter : NSObject

+ (NSInteger)signedIntegerFromData:(NSData *)data;
+ (NSUInteger)unsignedIntegerFromData:(NSData *)data;

+ (NSData *)dataBigEndianFromUInteger:(NSUInteger)value;
@end
