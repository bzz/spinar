//
//  SNDataToIntegerConverter.m
//  Spinar
//
//  Created by Hena Sofronov on 3/25/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNDataToIntegerConverter.h"

@implementation SNDataToIntegerConverter

+ (NSInteger)signedIntegerFromData:(NSData *)data
{
    NSData *dataForGettingValue = data;
    if (data.length > sizeof(NSUInteger)) {
        dataForGettingValue = [data subdataWithRange:NSMakeRange(0, sizeof(NSUInteger))];
    }
    NSUInteger lenght = data.length;
    NSUInteger unsignedValue = 0;
    unsigned char *bytes = malloc(sizeof(unsignedValue));
    bzero(bytes, sizeof(unsignedValue));
    [data getBytes:bytes];
    BOOL isNegative = ((bytes[lenght - 1]) & 0x80) != 0;
    if (isNegative) {
        for (NSInteger byteIndex = lenght; byteIndex < sizeof(unsignedValue); byteIndex ++) {
            bytes[byteIndex] = 0xFF;
        }
    }
    for (NSInteger byteIndex = sizeof(unsignedValue) - 1; byteIndex >= 0; byteIndex--) {
        unsignedValue = (unsignedValue << 8) | bytes[byteIndex];
    }
    NSInteger signedValue = (NSInteger)unsignedValue;
    return signedValue;
}

+ (NSUInteger)unsignedIntegerFromData:(NSData *)data
{
    NSData *dataForGettingValue = data;
    if (data.length > sizeof(NSUInteger)) {
        dataForGettingValue = [data subdataWithRange:NSMakeRange(0, sizeof(NSUInteger))];
    }
    NSUInteger lenght = data.length;
    unsigned char *bytes = malloc(lenght);
    bzero(bytes, lenght);
    [data getBytes:bytes];
    NSUInteger unsignedValue = 0;
    for (NSInteger byteIndex = lenght - 1; byteIndex >= 0; byteIndex--) {
        unsignedValue = (unsignedValue << 8) | bytes[byteIndex];
    }
    return unsignedValue;
}

+ (NSData *)dataBigEndianFromUInteger:(NSUInteger)value
{
    NSUInteger dataBytesLen = sizeof(value);
    unsigned char *dataBytes = malloc(dataBytesLen);
    for (NSUInteger index = 0; index < dataBytesLen; index ++) {
        dataBytes[index] = (value >> ((dataBytesLen - index - 1) * 8)) & 0xff;
    }
    return [NSData dataWithBytes:dataBytes length:dataBytesLen];
}

@end
