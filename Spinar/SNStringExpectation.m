//
//  SNStringExpectation.m
//  Spinar
//
//  Created by hena on 10/13/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNStringExpectation.h"

@interface SNStringExpectation ()
@property (copy, nonatomic) WriteWithStringResponseCompleteBlock invokeBlock;
@end

@implementation SNStringExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic stringBlock:(WriteWithStringResponseCompleteBlock)block useTimeout:(BOOL)isTimeout
{
    self = [super initWithCharacteristic:characteristic useTimeout:isTimeout];
    if (self) {
        _invokeBlock = block;
    }
    return self;
}

- (BOOL)invokeForCharacteristic:(SNCharacteristicType)characteristic binaryData:(NSData *)data error:(NSError *)error
{
    if (![super invokeForCharacteristic:characteristic binaryData:data error:error])
        return NO;
    if (self.invokeBlock)
        self.invokeBlock(error, [self stringFromData:data]);
    [self.delegate expectationInvoked:self];
    return YES;
}

- (NSString *)stringFromData:(NSData *)data
{
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

@end
