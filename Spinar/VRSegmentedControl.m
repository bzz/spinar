//
//  VRSegmentedControl.m
//  VideoReverse
//
//  Created by Duhov Filipp on 3/18/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "VRSegmentedControl.h"

@interface VRSegmentedControl ()

@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *segments;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *separators;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *selectedViewCenterConstraint;
@property (nonatomic, weak) IBOutlet UIView *selectionView;

@end

@implementation VRSegmentedControl

- (void)setCornerRadius:(CGFloat)cornerRadius
{
    _cornerRadius = cornerRadius;
    self.contentView.layer.cornerRadius = cornerRadius;
    self.contentView.layer.masksToBounds = cornerRadius > 0;
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    _borderWidth = borderWidth;
    self.contentView.layer.borderWidth = borderWidth;
}

- (void)setContentView:(UIView *)contentView
{
    _contentView = contentView;
    _contentView.layer.cornerRadius = self.cornerRadius;
    _contentView.layer.masksToBounds = self.cornerRadius > 0;
    _contentView.layer.borderWidth = self.borderWidth;
}

- (void)setSelectedViewCenterConstraint:(NSLayoutConstraint *)selectedViewCenterConstraint
{
    if (_selectedViewCenterConstraint != selectedViewCenterConstraint) {
        [self removeConstraint:_selectedViewCenterConstraint];
        [self addConstraint:selectedViewCenterConstraint];
        _selectedViewCenterConstraint = selectedViewCenterConstraint;
    }
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    [self setSelectedIndex:selectedIndex animated:NO];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex animated:(BOOL)animated
{
    if (_selectedIndex != selectedIndex) {
        _selectedIndex = selectedIndex;
        [self setSelectedSegmentHighlighted:NO shouldSendCompletion:NO];
        [self updateSegmentTintsAnimated:YES];
        
        if (self.selectedIndex < self.segmentsCount) {
            CGPoint selectedViewCenter = [self.segments[self.selectedIndex] center];
            self.selectedViewCenterConstraint.constant = (self.isHorisontical ? selectedViewCenter.x : selectedViewCenter.y);
        }
        
        self.selectedViewCenterConstraint = [NSLayoutConstraint constraintWithItem:self.segments[self.selectedIndex]
                                                                         attribute:NSLayoutAttributeCenterX
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.selectionView
                                                                         attribute:NSLayoutAttributeCenterX
                                                                        multiplier:1
                                                                          constant:0];
        void (^animationBlock)() = ^() {
            [self layoutIfNeeded];
        };
        if (animated) {
            [UIView animateWithDuration:kDefaultAnimationDuration animations:animationBlock];
        }
        else {
            animationBlock();
        }
        if (self.indexChangeBlock) {
            self.indexChangeBlock(selectedIndex, animated);
        }
    }
    else {
        self.selectedSegmentHighlighted = !self.selectedSegmentHighlighted;
    }
}

- (void)setSelectedSegmentHighlighted:(BOOL)selectedSegmentHighlighted
{
    [self setSelectedSegmentHighlighted:selectedSegmentHighlighted shouldSendCompletion:YES];
}

- (void)setSelectedSegmentHighlighted:(BOOL)selectedSegmentHighlighted shouldSendCompletion:(BOOL)shouldSendCompletion
{
    if (_selectedSegmentHighlighted != selectedSegmentHighlighted) {
        _selectedSegmentHighlighted = selectedSegmentHighlighted;
        [self updateHighlighting];
        if (shouldSendCompletion && self.sameIndexBlock) {
            self.sameIndexBlock(_selectedSegmentHighlighted);
        }
    }
}

- (NSUInteger)segmentsCount
{
    return self.segments.count;
}

- (void)tintColorDidChange
{
    [super tintColorDidChange];
    [self updateSegmentTints];
}

- (void)setSelectedTintColor:(UIColor *)selectedTintColor
{
    if (_selectedTintColor != selectedTintColor) {
        _selectedTintColor = selectedTintColor;
        [self updateSegmentTints];
    }
}

- (void)setSelectionType:(VRSegmentedControlSelectionType)selectionType
{
    _selectionType = selectionType;
    [self updateSegmentTints];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    _selectedIndex = NSNotFound;
    
    CGFloat (*VRRectPointGetter)(CGRect);
    if (self.isHorisontical) {
        VRRectPointGetter = CGRectGetMinX;
    }
    else {
        VRRectPointGetter = CGRectGetMinY;
    }
    self.segments = [self.segments sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return VRRectPointGetter([obj1 frame]) > VRRectPointGetter([obj2 frame]);
    }];
    
    NSMutableArray *subviews = [NSMutableArray arrayWithArray:self.segments];
    for (NSUInteger i = 0; i < subviews.count; i++) {
        UIView *subview = subviews[i];
        [subview setUserInteractionEnabled:NO];
        [subviews addObjectsFromArray:subview.subviews];
        if ([subview isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (id)subview;
            imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            imageView.highlightedImage = [imageView.highlightedImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        }
    }
    [self updateSegmentTints];
}

#pragma mark - UIControl methods

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super endTrackingWithTouch:touch withEvent:event];
    CGPoint touchLocation = [touch locationInView:self];
    if (CGRectContainsPoint(self.bounds, touchLocation)) {
        
        [self.segments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            CGRect viewFrame = [obj frame];
            if (((self.isHorisontical && (touchLocation.x > CGRectGetMinX(viewFrame)) && (touchLocation.x < CGRectGetMaxX(viewFrame)))) ||
                (!self.isHorisontical && (touchLocation.y > CGRectGetMinY(viewFrame)) && (touchLocation.y < CGRectGetMaxY(viewFrame)))) {
                [self setSelectedIndex:idx animated:YES];
                *stop = YES;
            }
        }];
    }
}

#pragma mark - View helpers

- (BOOL)isHorisontical
{
    return (CGRectGetMinX([self.segments[0] frame]) != CGRectGetMinX([self.segments[1] frame]));
}

- (void)updateSegmentTintsAnimated:(BOOL)animated
{
    void (^block)() = ^{
        [self updateSegmentTints];
    };
    if (animated) {
        [UIView animateWithDuration:kDefaultAnimationDuration animations:block];
    }
    else {
        block();
    }
}

- (void)updateSegmentTints
{
    [self.segments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView *view = obj;
        view.tintColor = nil;
        view.backgroundColor = [UIColor clearColor];
        switch (self.selectionType) {
            case VRSegmentedControlSelectionTypeTemplate: {
                if (idx != self.selectedIndex) {
                    view.tintColor = self.selectedTintColor;
                }
                break;
            }
            case VRSegmentedControlSelectionTypeBackground: {
                if (idx == self.selectedIndex) {
                    view.backgroundColor = self.selectedTintColor;
                }
            }
            default:
                break;
        }
    }];
    [[self.separators arrayByAddingObject:self.selectionView] enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL * stop) {
        view.backgroundColor = self.selectedTintColor;
    }];
    self.contentView.layer.borderColor = self.selectedTintColor.CGColor;
}

- (void)updateHighlighting
{
    [self.segments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSMutableArray *subviews = [NSMutableArray arrayWithObject:obj];
        for (NSUInteger i = 0; i < subviews.count; i++) {
            UIView *subview = subviews[i];
            [subviews addObjectsFromArray:subview.subviews];
            
            if ([subview isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView = (id)subview;
                if (imageView.highlightedImage) {
                    imageView.highlighted = idx == self.selectedIndex ? self.selectedSegmentHighlighted : NO;
                }
            }
        }
    }];
}

@end
