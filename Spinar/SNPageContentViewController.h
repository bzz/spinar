//
//  SNPageContentViewController.h
//  Spinar
//
//  Created by Duhov Filipp on 9/9/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

@protocol SNPageContentViewControllerDelegate <NSObject>

- (void)pageSelected:(id)sender;

@end

@interface SNPageContentViewController : UIViewController

@property (nonatomic, readonly) NSArray<UILabel*> *contentValueLabels;
@property (nonatomic, readonly) NSArray *distanceUnitLabels;
@property (nonatomic, readonly) NSArray *speedUnitLabels;
@property (nonatomic, readonly) NSArray *timeHoursUnitLabels;
@property (nonatomic, readonly) NSArray *timeMinutesUnitLabels;

@property (nonatomic, readonly) NSUInteger pageIndex;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil pageIndex:(NSUInteger)pageIndex;
+ (NSUInteger)pagesCountForNibWithName:(NSString *)nibNameOrNil;
@property (weak, nonatomic) id<SNPageContentViewControllerDelegate> delegate;

@end
