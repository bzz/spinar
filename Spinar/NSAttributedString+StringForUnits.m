//
//  NSAttributedString+StringForUnits.m
//  Spinar
//
//  Created by User on 10/29/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import "NSAttributedString+StringForUnits.h"
#import <UIKit/UIKit.h>

@implementation NSAttributedString (StringForUnits)

+ (NSAttributedString *)createAttributedStringWithNumber:(double)number units:(NSString *)units {
    NSString *stringNumber = [NSString stringWithFormat:@"%.0f",number];
    NSString *stringUnits = units;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",stringNumber, stringUnits]];
    [attributedString addAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],
                                      NSFontAttributeName: [UIFont systemFontOfSize:24.0]}
                              range:NSMakeRange(0, stringNumber.length)];
    
    [attributedString addAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],
                                      NSFontAttributeName: [UIFont systemFontOfSize:12.0]}
                              range:NSMakeRange(stringNumber.length, stringUnits.length)];
    return attributedString;
}


@end
