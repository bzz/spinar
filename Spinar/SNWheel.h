//
//  SNWheel.h
//  Spinar
//
//  Created by hena on 10/23/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNFirmware.h"
#import <UIKit/UIKit.h>

FOUNDATION_EXPORT double const kWheelMaxSpeed;
FOUNDATION_EXPORT double const kWheelMaxAbsPitch;
FOUNDATION_EXPORT double const kWheelMaxTemperature;

typedef NS_ENUM(NSInteger, SNRidingMode) {
    SNRidingModeComfort = 1,
    SNRidingModeStandard,
    SNRidingModeSport
};

typedef NS_ENUM(NSInteger, SNWheelState) {
    SNWheelStateReady,
    SNWheelStateLoadingFirmware
};

typedef void(^ReadingValueCompleteBlock)(NSError *error, double value);
typedef void(^WritingValueCompleteBlock)(NSError *error);

@protocol SNWheelDelegate;

@interface SNWheel : NSObject
/**
 *  Current connected Wheel
 *
 *  @return SNWheel instance related to connected Wheel
 */
+ (SNWheel *)currentWheel;

/**
 *  Delegate that will be called after every update of properties
 */
@property id<SNWheelDelegate> delegate;

/**
 *  Wheel state for sending commands
 */
@property (assign, nonatomic) SNWheelState state;

#pragma mark - Live Data

/**
 *  Current speed live data
 *
 *  @return value in kmph
 */
- (double)speed;

/**
 *  Battery life live data
 *
 *  @return value from 0.0 to 1.0
 */
- (double)batteryLife;

/**
 *  Traveled distance from start live data
 *
 *  @return value in km
 */
- (double)distanceTraveled;

/**
 *  Traveled time from start live data
 *
 *  @return value in hours
 */
- (double)timeTraveled;

/**
 *  Pitch Angle liva data
 *
 *  @return value in degrees
 */
- (double)pitch;

/**
 *  Temperature in live data
 *
 *  @return value in degrees
 */
- (double)temperature;

/**
 *  Error code of wheel in live data
 *
 *  @return if whell not ready integer value of error from 0 to 255. 0 - no error
 */
- (NSUInteger)errorCode;

/**
 *  Filtered Voltage in live data
 *
 *  @return voltage in Volts
 */
- (double)voltage;

/**
 *  Torque used for power usage
 *
 *  @return torque in undefined units
 */
- (double)motorTorque;


#pragma mark - Calculated Data

/**
 *  Estimated distance according to battery life left
 *
 *  @return double value in km
 */
- (double)distanceEstimate;

#pragma mark - Getting Non-Live Data

/**
 *  Firmware version, BT module
 */
@property (assign, nonatomic, readonly) SNFirmwareVersion firmwareVersionBT;
- (void)updatefirmwareVersionBTWithCompleteBlock:(WritingValueCompleteBlock)completeBlock; 

/**
 *  Firmware version, Wheel module
 */
@property (assign, nonatomic, readonly) SNFirmwareVersion firmwareVersionWheel;
- (void)updatefirmwareVersionWheelWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

/**
 *  Firmware version, Wheel module
 */
@property (assign, nonatomic, readonly) SNFirmwareVersion firmwareVersionBL;
- (void)updatefirmwareVersionBLWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;



@property (assign, nonatomic, readonly) SNFirmwareVersion firmwareVersionBTBootloader;
- (void)updatefirmwareVersionBTBootloaderWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

@property (assign, nonatomic, readonly) SNFirmwareVersion firmwareVersionWheelBootloader;
- (void)updatefirmwareVersionWheelBootloaderWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

/**
 *  Resetable total time wheel used in hours
 */
@property (assign, nonatomic, readonly) double journeyTime;
- (void)updateJourneyTimeWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)resetJourneyTimeWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

/**
 *  Resetable total distance wheel went in km
 */
@property (assign, nonatomic, readonly) double journeyDistance;
- (void)updateJourneyDistanceWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)resetJourneyDistanceWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

/**
 *  Resetable max speed wheel reached in kmph
 */
@property (assign, nonatomic, readonly) double maxSpeed;
- (void)updateMaxSpeedWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)resetMaxSpeedWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

/**
 *  Unresetable total time wheel used in hours
 */
@property (assign, nonatomic, readonly) double totalTime;
- (void)updateTotalTimeWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

/**
 *  Unresetable total distance wheel went in km
 */
@property (assign, nonatomic, readonly) double totalDistance;
- (void)updateTotalDistanceWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

/**
 *  Wheel id string presentation
 */
@property (strong, nonatomic, readonly) NSString *wheelId;
- (void)updateWheelIdWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;

#pragma mark - Getting and Setting Wheel properties

@property (assign, nonatomic, readonly, getter=isWheelLocked) BOOL wheelLocked;
- (void)setWheelLocked:(BOOL)isLocked withBlock:(WritingValueCompleteBlock)completeBlock;
- (void)updateWheelLocked:(WritingValueCompleteBlock)completeBlock;

@property (assign, nonatomic, readonly, getter=isLearningOn) BOOL learningOn;
- (void)setLearningOn:(BOOL)isOn withBlock:(WritingValueCompleteBlock)completeBlock;
- (void)updateLearningOnWithBlock:(WritingValueCompleteBlock)completeBlock;

@property (assign, nonatomic, readonly) double speedLimit;
- (void)setSpeedLimit:(double)speedLimit withBlock:(WritingValueCompleteBlock)completeBlock;
- (void)updateSpeedLimitWithBlock:(WritingValueCompleteBlock)completeBlock;

@property (assign, nonatomic, readonly) SNRidingMode ridingMode;
- (void)setRidingMode:(SNRidingMode)ridingMode withBlock:(WritingValueCompleteBlock)completeBlock;
- (void)updateRidingModeWithBlock:(WritingValueCompleteBlock)completeBlock;

@property (assign, nonatomic, readonly, getter=isLightOn) BOOL lightOn;
- (void)setLightOn:(BOOL)isOn withBlock:(WritingValueCompleteBlock)completeBlock;
//No update, because it is in live-data

@property (assign, nonatomic, readonly, getter=isSafetyOn) BOOL safetyOn;
- (void)setSafetyOn:(BOOL)isOn withBlock:(WritingValueCompleteBlock)completeBlock;
- (void)updateSafetyOnWithBlock:(WritingValueCompleteBlock)completeBlock;

- (void)setDeviceName:(NSString *)name withBlock:(WritingValueCompleteBlock)completeBlock;

- (void)setResetSettingsWithBlock:(WritingValueCompleteBlock)completeBlock;
#pragma mark - Password

- (void)writePassword:(NSData *)data withCompleteBlock:(WritingValueCompleteBlock)completeBlock;

#pragma mark - Firmware Loading Commands
- (void)setProgrammingOnlyBtLock:(WritingValueCompleteBlock)completeBlock;
- (void)setProgrammingLock:(WritingValueCompleteBlock)completeBlock;
- (void)setBootloaderWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)setProxyWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)setReadProtectWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)setFlashEraseWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)setSerialDefaultWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)setBootUpdateEnableWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)setApplyAppImageWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)setApplyBootImageWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)setProxyExitWithCompleteBlock:(WritingValueCompleteBlock)completeBlock;
- (void)writeFirmwareChunk:(NSData *)data chunkOffset:(NSUInteger)offset withCompleteBlock:(WritingValueCompleteBlock)completeBlock;

@end

@protocol SNWheelDelegate <NSObject>

- (void)didUpdateProperties;

@end
