//
//  SNLabel.m
//  Spinar
//
//  Created by Duhov Filipp on 7/19/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNLabel.h"

@interface SNLabel ()

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *baselineConstraint;

@end

@implementation SNLabel

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGSize textSize = CGSizeZero;
    NSUInteger fontSize = self.font.pointSize + 1;
    
    do {
        textSize = [self.text sizeWithAttributes:@{NSFontAttributeName:[self.font fontWithSize:--fontSize]}];
    }
    while ( ceil(textSize.width) > self.frame.size.width && fontSize > 1);
    CGFloat baselineConstraint = (self.frame.size.height - ceil(textSize.height))/2;
    if (fabs(self.baselineConstraint.constant - baselineConstraint) > 3) {
        self.baselineConstraint.constant = baselineConstraint;
    }
}

@end
