//
//  SNDelayedInvoke.m
//  Spinar
//
//  Created by hena on 9/29/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNDelayedInvoke.h"

@interface SNDelayedInvoke ()
@property (copy, nonatomic) void(^invokeBlock)();
@end

@implementation SNDelayedInvoke

+ (instancetype)sharedInstance {
    static SNDelayedInvoke *manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (void)addInvokeBlock:(void (^)())block {
    self.invokeBlock = block;
}

- (void)invoke {
    if (self.invokeBlock) {
        self.invokeBlock();
    }
}
@end
