//
//  SNSpecificValueExpectation.h
//  Spinar
//
//  Created by Hena Sofronov on 3/23/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNValueExpectation.h"

@interface SNSpecificValueExpectation : SNValueExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic
                    isSigned:(BOOL)isSigned
                       value:(double)value
               completeBlock:(WriteCompleteBlock)block
                  useTimeout:(BOOL)isTimeout;

@end
