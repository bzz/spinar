//
//  SNDelayedInvoke.h
//  Spinar
//
//  Created by hena on 9/29/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNDelayedInvoke : NSObject

+ (id)sharedInstance;
- (void)addInvokeBlock:(void(^)())block;
- (void)invoke;

@end
