//
//  main.m
//  Spinar
//
//  Created by hena on 10/22/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
