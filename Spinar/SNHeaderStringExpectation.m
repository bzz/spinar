//
//  SNHeaderStringExpectation.m
//  Spinar
//
//  Created by Hena Sofronov on 3/23/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNHeaderStringExpectation.h"

@interface SNHeaderStringExpectation ()
@property (assign, nonatomic) double headerValue;
@end

@implementation SNHeaderStringExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic headerValue:(double)headerValue completeBlock:(WriteWithStringResponseCompleteBlock)block useTimeout:(BOOL)isTimeout
{
    self = [super initWithCharacteristic:characteristic stringBlock:block useTimeout:isTimeout];
    if (self) {
        _headerValue = headerValue;
    }
    return self;
}

- (BOOL)invokeForCharacteristic:(SNCharacteristicType)characteristic binaryData:(NSData *)data error:(NSError *)error
{
    if (error) {
        return [super invokeForCharacteristic:characteristic binaryData:data error:error];
    }
    unsigned char header;
    [data getBytes:&header range:NSMakeRange(0, 1)];
    if (header != (unsigned char)self.headerValue) {
        return NO;
    }
    return [super invokeForCharacteristic:characteristic binaryData:data error:error];
}

- (NSString *)stringFromData:(NSData *)data
{
    return [super stringFromData:[data subdataWithRange:NSMakeRange(1, data.length - 1)]];
}

@end
