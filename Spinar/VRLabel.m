//
//  VRLabel.m
//  VideoReverse
//
//  Created by Duhov Filipp on 4/24/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "VRLabel.h"

@implementation VRLabel

- (void)tintColorDidChange
{
    [super tintColorDidChange];
    self.textColor = self.tintColor;
}

@end
