//
//  SNHeaderValueExpectation.m
//  Spinar
//
//  Created by Hena Sofronov on 3/23/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNHeaderValueExpectation.h"

@interface SNHeaderValueExpectation ()
@property (assign, nonatomic) double headerValue;
@end

@implementation SNHeaderValueExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic isSigned:(BOOL)isSigned headerValue:(double)headerValue completeBlock:(WriteWithValueResponseCompleteBlock)block useTimeout:(BOOL)isTimeout
{
    self = [super initWithCharacteristic:characteristic isSigned:(BOOL)isSigned valueBlock:block useTimeout:isTimeout];
    if (self) {
        _headerValue = headerValue;
    }
    return self;
}

- (BOOL)invokeForCharacteristic:(SNCharacteristicType)characteristic binaryData:(NSData *)data error:(NSError *)error
{
    if (error) {
        return [super invokeForCharacteristic:characteristic binaryData:data error:error];
    }
    unsigned char header;
    [data getBytes:&header range:NSMakeRange(0, 1)];
    if (header != (unsigned char)self.headerValue) {
        return NO;
    }
    return [super invokeForCharacteristic:characteristic binaryData:data error:error];
}

- (double)valueFromData:(NSData *)data
{
    return [super valueFromData:[data subdataWithRange:NSMakeRange(1, data.length - 1)]];
}

@end
