//
//  SNWritingQueue.m
//  Spinar
//
//  Created by Hena Sofronov on 3/22/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNWritingQueue.h"

@interface SNWritingQueue ()<SNWriteInvokationDelegate>
@property (strong, nonatomic) NSMutableArray<SNWriteInvokation *> *queue;
@end

@implementation SNWritingQueue

- (id)init
{
    self = [super init];
    if (self) {
        _queue = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)invokationDone:(id)sender
{
    [self.queue removeObject:sender];
    [self invokeNext];
}

- (void)addWriteInvokation:(SNWriteInvokation *)writeInvokation
{
    [writeInvokation setDelegate:self];
    [self.queue addObject:writeInvokation];
    [self invokeNext];
}

- (void)finishInvokationWithError:(NSError *)error
{
    if ([self.queue count] && [[self.queue firstObject] state] == SNWriteInvokationStateInvoking) {
        [[self.queue firstObject] finishWithError:error];
    }
}

- (void)finishAllInvokationsWithError:(NSError *)error
{
    NSArray<SNWriteInvokation *> *queueCopy = [self.queue copy];
    [self.queue removeAllObjects];
    for (SNWriteInvokation *invokation in queueCopy) {
        [invokation finishWithError:error];
    }
}

- (void)invokeNext
{
    if ([self.queue count]) {
        if ([[self.queue firstObject] state] == SNWriteInvokationStateWaiting) {
            [[self.queue firstObject] invoke];
        }
    }
}

@end
