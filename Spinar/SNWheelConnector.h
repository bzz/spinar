//
//  SNBLEAutoConnect.h
//  Spinar
//
//  Created by hena on 9/30/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNDevice.h"

typedef NS_ENUM(NSInteger, SNBLEState) {
    SNBLEStateOn,
    SNBLEStateOff,
    SNBLEStateUnavailable
};

@protocol SNWheelConnectorDelegate<NSObject>

- (void)devicesUpdated;
@end

@interface SNWheelConnector : NSObject

+ (SNWheelConnector *)sharedInstance;
- (SNBLEState)bleState;
@property (weak, nonatomic) id<SNWheelConnectorDelegate> delegate;

@property (strong, nonatomic, readonly) SNDevice *connectedDevice;
@property (strong, nonatomic, readonly) NSArray *discoveredDevices;

- (void)connectDevice:(SNDevice *)device;
- (BOOL)disconnect;

- (void)startTrying;
- (void)stopTrying;

@end
