//
//  SNBLEAutoConnect.m
//  Spinar
//
//  Created by hena on 9/30/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNWheelConnector.h"
#import "SNBLEManager.h"
#import "SNWheel.h"
#import "AppDelegate.h"

NSString * const kUUIDKey = @"spinarLastConnectedUUID";

typedef NS_ENUM(NSInteger, SNWheelConnectorState) {
    SNWheelConnectorStateNotConnected,
    SNWheelConnectorStateConnecting,
    SNWheelConnectorStateConnected,
    SNWheelConnectorStateDisconnecting
};

@interface SNWheelConnector()<SNBLEManagerConnectionDelegate>

@property (assign, nonatomic) SNWheelConnectorState state;

@property (strong, nonatomic) NSString *deviceToConnectUUID;
@property (strong, nonatomic) NSTimer *timer;

@end

@implementation SNWheelConnector

@synthesize deviceToConnectUUID = _deviceToConnectUUID;

- (id)init {
    self = [super init];
    if (self) {
        [[SNBLEManager sharedManager] setConnectionDelegate:self];
        self.state = SNWheelConnectorStateNotConnected;
    }
    return self;
}

+ (id)sharedInstance {
    static SNWheelConnector *manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (SNBLEState)bleState
{
    CBCentralManagerState centralState = [[SNBLEManager sharedManager] bleState];
    if (centralState == CBCentralManagerStatePoweredOn) {
        return SNBLEStateOn;
    }
    if (centralState == CBCentralManagerStateResetting || centralState == CBCentralManagerStatePoweredOff) {
        return SNBLEStateOff;
    }
    return SNBLEStateUnavailable;
}

- (void)setDeviceToConnectUUID:(NSString *)deviceToConnectUUID {
    _deviceToConnectUUID = deviceToConnectUUID;
    if (_deviceToConnectUUID) {
        [[NSUserDefaults standardUserDefaults] setObject:_deviceToConnectUUID forKey:kUUIDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUUIDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSString *)deviceToConnect {
    if (!_deviceToConnectUUID) {
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSString *uuidString = [[NSUserDefaults standardUserDefaults] stringForKey:kUUIDKey];
        if (uuidString) {
            _deviceToConnectUUID = uuidString;
        }
    }
    return _deviceToConnectUUID;
}

#pragma mark - Autoconnection

- (void)startTrying {
    [[SNBLEManager sharedManager] startScan];
    [self tryConnect];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(tryConnect) userInfo:nil repeats:YES];
}

- (void)stopTrying {
    [self.timer invalidate];
}

#pragma mark - Connection

- (void)connectDevice:(SNDevice *)device
{
    [self setDeviceToConnectUUID:device.uuid];
    if (![self disconnect]) {
        [self tryConnect];
    }
}

- (BOOL)disconnect
{
    if ([[SNBLEManager sharedManager] disconnect]) {
        [self setState:SNWheelConnectorStateDisconnecting];
        return YES;
    }
    [self setState:SNWheelConnectorStateNotConnected];
    return NO;
}

#pragma mark - SNBLEManagerConnectionDelegate

- (void)deviceListUpdated
{
    [self updateDiscoveredDevices];
}

- (void)deviceConnectedWithUUID:(NSString *)uuidString name:(NSString *)name
{
    _connectedDevice = [[SNDevice alloc] initWithUUID:uuidString andName:name];
    [self setState:SNWheelConnectorStateConnected];
    [self updateDiscoveredDevices];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] connectionStateChanged];
}

- (void)deviceDisconnected
{
    _connectedDevice = nil;
    [self setState:SNWheelConnectorStateNotConnected];
    [self updateDiscoveredDevices];
    [self tryConnect];
}

#pragma mark - Helper

- (void)tryConnect {
    if (![self deviceToConnect]) {
        return;
    }
    if (self.state == SNWheelConnectorStateNotConnected) {
        if ([[SNBLEManager sharedManager] connectToUUID:self.deviceToConnect]) {
            [self setState:SNWheelConnectorStateConnecting];
        }
        return;
    }
    if (self.state == SNWheelConnectorStateConnecting) {
        if (![self disconnect]) {
            [self tryConnect];
        }
        return;
    }
}

- (void)updateDiscoveredDevices {
    NSMutableArray *discoveredDevices = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in [[SNBLEManager sharedManager] discoveredDevices]) {
        if (![dict[kDeviceUUID] isEqualToString:self.connectedDevice.uuid]) {
            [discoveredDevices addObject:[[SNDevice alloc] initWithUUID:dict[kDeviceUUID] andName:dict[kDeviceName]]];
        }
    }
    _discoveredDevices = [discoveredDevices copy];
    [self.delegate devicesUpdated];
}

@end
