//
//  SNPageTableViewCell.m
//  Spinar
//
//  Created by Duhov Filipp on 9/9/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "SNPageTableViewCell.h"
#import "SNPageContentViewController.h"

NSString *const kDistanceUnitKey = @"distanceUnitKey";
NSString *const kSpeedUnitKey = @"speedUnitKey";
NSString *const kTimeUnitKey = @"timeUnitKey";

@interface SNPageTableViewCell () <UIPageViewControllerDelegate, UIPageViewControllerDataSource, SNPageContentViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;

@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) NSMutableDictionary *pages;
@property (nonatomic, assign) BOOL needsUpdateUnitsLabels;

@end

@implementation SNPageTableViewCell

#pragma mark - Properties

- (void)setPagesNibName:(NSString *)pagesNibName
{
    _pagesNibName = [pagesNibName copy];
    [self updatePageVC];
}

- (NSMutableDictionary *)pages
{
    if (!_pages) {
        _pages = [NSMutableDictionary dictionary];
    }
    return _pages;
}

- (void)setDataSource:(id<SNPageTableViewCellDataSource>)dataSource
{
    if (_dataSource != dataSource) {
        _dataSource = dataSource;
        [self setNeedsUpdateData];
    }
}

#pragma mark - View lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                              navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                            options:nil];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    self.pageViewController.view.frame = self.contentView.bounds;
    [self.contentView insertSubview:self.pageViewController.view atIndex:0];
    [self updatePageVC];
    [self.pageControl addTarget:self action:@selector(pageControlValueChanged:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - Acitons

- (void)pageControlValueChanged:(id)sender
{
    NSUInteger index = ((SNPageContentViewController *)self.pageViewController.viewControllers.firstObject).pageIndex;
    NSUInteger newIndex = self.pageControl.currentPage;
    __weak typeof(self) weakSelf = self;
    [self.pageViewController setViewControllers:@[ [self viewControllerWithIndex:newIndex] ]
                                      direction:(newIndex > index) ? UIPageViewControllerNavigationDirectionForward
                                               : UIPageViewControllerNavigationDirectionReverse
                                       animated:YES
                                     completion:^(BOOL finished) {
                                         if (finished) {
                                             [weakSelf.delegate pageDidAppearWithIndex:newIndex];
                                         }
                                     }];
}

#pragma mark - UIPageViewControllerDataSource protocol implementation

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SNPageContentViewController *)viewController).pageIndex + 1;
    return [self viewControllerWithIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SNPageContentViewController *)viewController).pageIndex - 1;
    return [self viewControllerWithIndex:index];
}

#pragma mark - UIPageViewControllerDelegate protocol implementation

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    for (id vc in pendingViewControllers) {
        [self updateLabelsForViewController:vc];
    }
}

- (void)pageViewController:(UIPageViewController *)viewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
    if (!completed) {
        return;
    }
    
    SNPageContentViewController *currentViewController = self.pageViewController.viewControllers.lastObject;
    self.pageControl.currentPage = currentViewController.pageIndex;
    [self.delegate pageDidAppearWithIndex:currentViewController.pageIndex];
}

#pragma mark - SNPageContentViewControllerDelegate

- (void)pageSelected:(id)sender
{
    [self.delegate pageSelectedWithIndex:[(SNPageContentViewController *)sender pageIndex]];
}

#pragma mark - Helper methods

- (UIViewController *)viewControllerWithIndex:(NSUInteger)index
{
    SNPageContentViewController *viewController = self.pages[@(index)];
    if (!viewController) {
        viewController = [[SNPageContentViewController alloc] initWithNibName:self.pagesNibName pageIndex:index];
        [viewController setDelegate:self];
        viewController.view.backgroundColor = nil;
        [viewController.view setNeedsUpdateConstraints];
        [viewController.view setNeedsLayout];
        if (viewController)
            self.pages[@(index)] = viewController;
    }
    [self updateLabelsForViewController:viewController];
    return viewController;
}

- (void)updatePageVC
{
    [self.pages removeAllObjects];
    
    id vc = [self viewControllerWithIndex:0];
    if (vc) {
        [self.pageViewController setViewControllers:@[ vc ]
                                          direction:UIPageViewControllerNavigationDirectionForward
                                           animated:NO
                                         completion:nil];
    }
    self.pageControl.numberOfPages = [SNPageContentViewController pagesCountForNibWithName:self.pagesNibName];
}

- (void)updateLabelsForViewController:(SNPageContentViewController *)viewController
{
    if ([viewController isKindOfClass:[SNPageContentViewController class]]) {
        for (UILabel *label in viewController.distanceUnitLabels) {
            label.text = [[self.dataSource pageCell:self valueForKey:kDistanceUnitKey] description];
        }
        for (UILabel *label in viewController.speedUnitLabels) {
            label.text = [[self.dataSource pageCell:self valueForKey:kSpeedUnitKey] description];
        }
        for (UILabel *label in viewController.timeHoursUnitLabels) {
            label.text = [[[self.dataSource pageCell:self valueForKey:kTimeUnitKey] firstObject] description];
        }
        for (UILabel *label in viewController.timeMinutesUnitLabels) {
            label.text = [[[self.dataSource pageCell:self valueForKey:kTimeUnitKey] lastObject] description];
        }
        if (viewController.contentValueLabels.count > 1) {
            NSInteger hours = [[self.dataSource pageCell:self valueCellPage:viewController.pageIndex]?:@0 doubleValue];
            NSInteger minutes = ([[self.dataSource pageCell:self valueCellPage:viewController.pageIndex]?:@0 doubleValue] - hours) * 60;
            UILabel *firstLabel = viewController.contentValueLabels.firstObject;
            UILabel *lastLabel = viewController.contentValueLabels.lastObject;
            UILabel *leftLabel = firstLabel.frame.origin.x < lastLabel.frame.origin.x ? firstLabel : lastLabel;
            UILabel *rightLabel = firstLabel.frame.origin.x > lastLabel.frame.origin.x ? firstLabel : lastLabel;
            leftLabel.text = @(hours).description;
            rightLabel.text = @(minutes).description;
        }
        else {
            viewController.contentValueLabels.firstObject.text = [NSString stringWithFormat:@"%.1f",[[self.dataSource pageCell:self valueCellPage:viewController.pageIndex]?:@0 doubleValue]];
        }
    }
}

- (void)setNeedsUpdateData
{
    if (!self.needsUpdateUnitsLabels) {
        self.needsUpdateUnitsLabels = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateDataIfNeeded];
        });
    }
}

- (void)updateDataIfNeeded
{
    if (self.needsUpdateUnitsLabels) {
        [self reloadData];
    }
}

- (void)reloadData
{
    for (id vc in self.pageViewController.viewControllers) {
        [self updateLabelsForViewController:vc];
    }
    self.needsUpdateUnitsLabels = NO;
}

- (void)reloadDataCellPage:(NSUInteger)cellPage
{
    for (SNPageContentViewController *vc in self.pageViewController.viewControllers) {
        if ([vc isKindOfClass:[SNPageContentViewController class]] && vc.pageIndex == cellPage) {
            [self updateLabelsForViewController:vc];
            break;
        }
    }
}

@end
