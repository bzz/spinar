//
//  SNSignUpViewController.m
//  Spinar
//
//  Created by Duhov Filipp on 9/24/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNSignUpViewController.h"
#import "UIColorFromRGB.h"

static NSString * const kPrivacyPolicyString = @"Privacy Policy";
static NSString * const kTermsOfServiceString = @"Terms of Service";

@interface SNSignUpViewController () <UITextViewDelegate>

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextView *agreementTextView;

@end

@implementation SNSignUpViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for (UITextField *textField in self.textFields) {
        if (textField.placeholder && textField.textColor) {
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder
                                                                              attributes:@{NSForegroundColorAttributeName : textField.textColor}];
        }
    }
    NSString *originalAgreementText = self.agreementTextView.text;
    NSMutableAttributedString *attributedAgreementText = [[NSMutableAttributedString alloc] initWithString:self.agreementTextView.text
                                                                                                attributes:@{NSForegroundColorAttributeName : self.agreementTextView.textColor}];
    
    NSRange privacyPolicyRange = [originalAgreementText rangeOfString:kPrivacyPolicyString];
    NSRange termsOfServiceRange = [originalAgreementText rangeOfString:kTermsOfServiceString];
    
    [attributedAgreementText addAttribute:NSLinkAttributeName
                                    value:[NSURL URLWithString:@"http://www.google.com"]
                                    range:privacyPolicyRange];
    [attributedAgreementText addAttribute:NSFontAttributeName
                                    value:[UIFont boldSystemFontOfSize:self.agreementTextView.font.pointSize]
                                    range:privacyPolicyRange];
    [attributedAgreementText addAttribute:NSLinkAttributeName
                                    value:[NSURL URLWithString:@"http://www.google.com"]
                                    range:termsOfServiceRange];
    [attributedAgreementText addAttribute:NSFontAttributeName
                                    value:[UIFont boldSystemFontOfSize:self.agreementTextView.font.pointSize]
                                    range:termsOfServiceRange];
    
    self.agreementTextView.linkTextAttributes =  @{NSUnderlineStyleAttributeName: @(NSUnderlinePatternSolid)};
    self.agreementTextView.attributedText = attributedAgreementText;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameChange:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

#pragma mark - Actions

- (IBAction)getWheelingButtonTapped:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Helper methods

- (void)keyboardFrameChange:(NSNotification *)note
{
    CGRect keyboardEndBounds = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect frame = self.view.frame;
    frame.size.height = keyboardEndBounds.origin.y - frame.origin.y;
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.view.frame = frame;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

@end
