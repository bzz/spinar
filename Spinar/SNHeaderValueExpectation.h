//
//  SNHeaderValueExpectation.h
//  Spinar
//
//  Created by Hena Sofronov on 3/23/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNValueExpectation.h"

@interface SNHeaderValueExpectation : SNValueExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic
                    isSigned:(BOOL)isSigned
                 headerValue:(double)headerValue
               completeBlock:(WriteWithValueResponseCompleteBlock)block
                  useTimeout:(BOOL)isTimeout;

@end
