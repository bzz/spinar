//
//  SNQuestionAlertView.h
//  Spinar
//
//  Created by hena on 10/7/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^YesNoCompletion)(BOOL isOk);


@interface SNQuestionAlertView : UIAlertView

@property (copy, nonatomic) YesNoCompletion completeBlock;
+ (void)showQuestion:(NSString *)question completeBlock:(YesNoCompletion)completeBlock;
+ (void)showQuestion:(NSString *)question okButton:(NSString *)okString noButton:(NSString *)noString withHeader:(NSString *)header completeBlock:(YesNoCompletion)completeBlock;

@end
