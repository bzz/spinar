//
//  SNRemoteFirmware.h
//  Spinar
//
//  Created by Hena Sofronov on 3/28/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNFirmware.h"

typedef void(^FirmwareVersionCompleteBlock)(BOOL isSuccess, SNFirmwareVersion version);

@interface SNRemoteFirmware : NSObject

- (void)versionForType:(SNFirmwareType)firmwareType withCompleteBlock:(FirmwareVersionCompleteBlock)completeBlock;
- (SNFirmwareVersion)versionForType:(SNFirmwareType)firmwareType;
- (SNFirmware *)firmwareForType:(SNFirmwareType)firmwareType;

- (NSString *)manualPath;
@end
