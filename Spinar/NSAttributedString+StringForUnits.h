//
//  NSAttributedString+StringForUnits.h
//  Spinar
//
//  Created by User on 10/29/14.
//  Copyright (c) 2014 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (StringForUnits)
/*
 * Creates attributed string with given number and given units with appropriate fonts
 * 
 * @param number Number to be displayed in string
 * @param units String with given units
 *
 * @return Attributed string with given number and units
 *
 */
+ (NSAttributedString *)createAttributedStringWithNumber:(double)number units:(NSString *)units;

@end
