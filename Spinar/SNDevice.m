//
//  SNDiscoveredDevice.m
//  Spinar
//
//  Created by hena on 10/12/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNDevice.h"

@implementation SNDevice

- (id)initWithUUID:(NSString *)uuid andName:(NSString *)name
{
    self = [super init];
    if (self) {
        _uuid = uuid;
        _name = name;
    }
    return self;
}

@end
