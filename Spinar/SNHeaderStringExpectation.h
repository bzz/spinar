//
//  SNHeaderStringExpectation.h
//  Spinar
//
//  Created by Hena Sofronov on 3/23/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNStringExpectation.h"

@interface SNHeaderStringExpectation : SNStringExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic headerValue:(double)headerValue completeBlock:(WriteWithStringResponseCompleteBlock)block useTimeout:(BOOL)isTimeout;

@end
