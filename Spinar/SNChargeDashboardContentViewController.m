//
//  SNChargeDashboardContentViewController.m
//  Spinar
//
//  Created by Hena Sofronov on 2/29/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNChargeDashboardContentViewController.h"
#import "SNWheel.h"
#import "SNSettings.h"
#import "SNConvertor.h"

@interface SNChargeDashboardContentViewController ()

@property (weak, nonatomic) IBOutlet UILabel *totalDistanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimeHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMinutesLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalDistanceUnitsLabel;
@end

@implementation SNChargeDashboardContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refresh
{
    SNWheel *currentWheel = [SNWheel currentWheel];
    double convertedDistanceTraveled = currentWheel.distanceTraveled;
    if ([SNSettings sharedSettings].areDisplayingUnitsMiles) {
        convertedDistanceTraveled = [SNConvertor kilometresToMiles:convertedDistanceTraveled];
    }
    self.totalTimeHoursLabel.text = [self roundedIntStringFromDouble:[[SNWheel currentWheel] timeTraveled]];
    double miunutes = ([[SNWheel currentWheel] timeTraveled] - (NSInteger)[[SNWheel currentWheel] timeTraveled])*60;
    self.totalMinutesLabel.text = [self roundedIntStringFromDouble:miunutes];
    self.totalDistanceLabel.text = [self roundedStringFromDouble:convertedDistanceTraveled];
    NSString *distanceUnits = [SNSettings stringForDistanceUnits:[SNSettings sharedSettings].areDisplayingUnitsMiles];
    self.totalDistanceUnitsLabel.text = distanceUnits;
}

- (NSString *)roundedIntStringFromDouble:(double)value
{
    return [NSString stringWithFormat:@"%ld",(long)value];
}

- (NSString *)roundedStringFromDouble:(double)value
{
    return [NSString stringWithFormat:@"%.1f", value];
}

@end
