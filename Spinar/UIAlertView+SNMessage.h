//
//  UIAlertView+SNMessage.h
//  Spinar
//
//  Created by hena on 6/3/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (SNMessage)
+ (void)showMessage:(NSString *)message;
+ (void)showMessage:(NSString *)message withHeader:(NSString *)header;

@end
