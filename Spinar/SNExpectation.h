//
//  SNExpectation.h
//  Spinar
//
//  Created by hena on 9/9/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNBLEManager.h"

@protocol SNExpectationDelegate <NSObject>

- (void)expectationInvoked:(id)sender;

@end

@interface SNExpectation : NSObject
- (id)initWithCharacteristic:(SNCharacteristicType)characteristic useTimeout:(BOOL)isTimeout;

- (BOOL)invokeForCharacteristic:(SNCharacteristicType)characteristic binaryData:(NSData *)data error:(NSError *)error;
- (BOOL)isExpectCharacteristic:(SNCharacteristicType)characteristic;
@property (weak, nonatomic) id<SNExpectationDelegate> delegate;
@end
