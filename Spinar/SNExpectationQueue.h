//
//  SNExpectationQueue.h
//  Spinar
//
//  Created by hena on 10/7/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNExpectation.h"

@interface SNExpectationQueue : NSObject

- (void)addExpectation:(SNExpectation *)expectation;
- (void)clearExpectations;
- (BOOL)isExpectForCharacteristic:(SNCharacteristicType)characteristicType;
- (BOOL)invokeExpectationForCharacteristicType:(SNCharacteristicType)characteristicType withBinaryData:(NSData *)data error:(NSError *)error;

@end
