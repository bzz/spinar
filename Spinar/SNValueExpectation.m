//
//  SNValueExpectation.m
//  Spinar
//
//  Created by hena on 10/13/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNValueExpectation.h"
#import "SNDataToIntegerConverter.h"

@interface SNValueExpectation ()

@property (copy, nonatomic) WriteWithValueResponseCompleteBlock invokeBlock;
@property (assign, nonatomic) BOOL isSigned;
@end

@implementation SNValueExpectation

- (id)initWithCharacteristic:(SNCharacteristicType)characteristic isSigned:(BOOL)isSigned valueBlock:(WriteWithValueResponseCompleteBlock)block useTimeout:(BOOL)isTimeout
{
    self = [super initWithCharacteristic:characteristic useTimeout:isTimeout];
    if (self) {
        _invokeBlock = block;
        _isSigned = isSigned;
    }
    return self;
}

- (BOOL)invokeForCharacteristic:(SNCharacteristicType)characteristic binaryData:(NSData *)data error:(NSError *)error
{
    if (![super invokeForCharacteristic:characteristic binaryData:data error:error])
        return NO;
    if (self.invokeBlock)
        self.invokeBlock(error, [self valueFromData:data]);
    [self.delegate expectationInvoked:self];
    return YES;
}

- (double)valueFromData:(NSData *)data
{
    if (self.isSigned) {
        return [SNDataToIntegerConverter signedIntegerFromData:data];
    }
    return [SNDataToIntegerConverter unsignedIntegerFromData:data];
}

@end
