//
//  SNManualViewController.m
//  Spinar
//
//  Created by Hena Sofronov on 5/13/16.
//  Copyright © 2016 Coderivium. All rights reserved.
//

#import "SNManualViewController.h"
#import "SNRemoteFirmware.h"

NSString * const kFileName = @"UserManual24.02.16V9";

@interface SNManualViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;


@end

@implementation SNManualViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSString *path = [[NSBundle mainBundle] pathForResource:kFileName ofType:@"pdf"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    SNRemoteFirmware *remote = [[SNRemoteFirmware alloc] init];
    NSString *path = [remote manualPath];
    [self.webView setHidden:NO];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
