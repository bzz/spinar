//
//  SNAdditionalInfoViewController.m
//  Spinar
//
//  Created by Duhov Filipp on 9/25/15.
//  Copyright © 2015 Coderivium. All rights reserved.
//

#import "SNAdditionalInfoViewController.h"
#import "SNSettings.h"
#import "SNWheel.h"
#import "UIColorFromRGB.h"

@interface SNAdditionalInfoViewController () <SNWheelDelegate>

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *distanceUnitsLabels;
@property (weak, nonatomic) IBOutlet UITextView *statusTextView;
@property (weak, nonatomic) IBOutlet UILabel *voltageLabel;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *serialLabel;
@property (weak, nonatomic) IBOutlet UILabel *odometerLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *firmwareVersionLabel;


@property (strong, nonatomic) NSDictionary *descriptionForMode;
@end

@implementation SNAdditionalInfoViewController

#pragma mark - View Controller LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.descriptionForMode = @{@(SNRidingModeComfort): @"Comfort",
                                @(SNRidingModeStandard): @"Standard",
                                @(SNRidingModeSport): @"Sport"};
    self.navigationItem.rightBarButtonItem.tintColor = UIColorFromRGB(62, 186, 231);
    [[SNWheel currentWheel] updateTotalTimeWithCompleteBlock:^(NSError *error) {
        [self refresh];
        [[SNWheel currentWheel] updateTotalDistanceWithCompleteBlock:^(NSError *error) {
            [self refresh];
            [[SNWheel currentWheel] updateRidingModeWithBlock:^(NSError *error) {
                [self refresh];
                [[SNWheel currentWheel] updatefirmwareVersionBTWithCompleteBlock:^(NSError *error) {
                    [self refresh];
                    [[SNWheel currentWheel] updatefirmwareVersionWheelWithCompleteBlock:^(NSError *error) {
                        [self refresh];
                        [[SNWheel currentWheel] updatefirmwareVersionBLWithCompleteBlock:^(NSError *error) {
                            [self refresh];
                        }];
                    }];
                }];
            }];
        }];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SNWheel currentWheel].delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [SNWheel currentWheel].delegate = nil;
    [super viewWillDisappear:animated];
}

#pragma mark - SNWheelDelegate protocol implementation

- (void)didUpdateProperties
{
    [self refresh];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.statusTextView scrollRangeToVisible:NSMakeRange(0, 1)];
}

#pragma mark - Helper methods

- (void)refresh
{
    self.navigationItem.rightBarButtonItem.tintColor = [SNSettings colorForErrorCode:[[SNWheel currentWheel] errorCode]];
    NSString *distanceUnits = [SNSettings stringForDistanceUnits:[SNSettings sharedSettings].areDisplayingUnitsMiles];
    for (UILabel *distanceUnitsLabel in self.distanceUnitsLabels) {
        distanceUnitsLabel.text = distanceUnits;
    }
    NSString *statusString = [NSString stringWithFormat:@"%@ (%@)",[SNSettings shortDescriptionForErrorCode:[[SNWheel currentWheel] errorCode]], [SNSettings descriptionForErrorCode:[[SNWheel currentWheel] errorCode]]];
    if (![self.statusTextView.text isEqualToString:statusString]) {
        self.statusTextView.text = statusString;
    }
    self.voltageLabel.text = [NSString stringWithFormat:@"%ld",(long)[[SNWheel currentWheel] voltage]];
    self.modeLabel.text = self.descriptionForMode[@([[SNWheel currentWheel] ridingMode])];
    self.serialLabel.text = [[SNWheel currentWheel] wheelId];
    self.odometerLabel.text = [NSString stringWithFormat:@"%ld",(long)[[SNWheel currentWheel] totalDistance]];;
    self.timeLabel.text = [NSString stringWithFormat:@"%ld",(long)[[SNWheel currentWheel] totalTime]];;
    self.firmwareVersionLabel.text = [NSString stringWithFormat:@"%lu.%lu:%lu.%lu:%lu.%lu",
                                      (unsigned long)[[SNWheel currentWheel] firmwareVersionWheel].major,
                                      (unsigned long)[[SNWheel currentWheel] firmwareVersionWheel].minor,
                                      (unsigned long)[[SNWheel currentWheel] firmwareVersionBT].major,
                                      (unsigned long)[[SNWheel currentWheel] firmwareVersionBT].minor,
                                      (unsigned long)[[SNWheel currentWheel] firmwareVersionBL].major,
                                      (unsigned long)[[SNWheel currentWheel] firmwareVersionBL].minor];
}

@end
