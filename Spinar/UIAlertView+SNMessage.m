//
//  UIAlertView+SNMessage.m
//  Spinar
//
//  Created by hena on 6/3/15.
//  Copyright (c) 2015 Coderivium. All rights reserved.
//

#import "UIAlertView+SNMessage.h"

@implementation UIAlertView (SNMessage)

+ (void)showMessage:(NSString *)message withHeader:(NSString *)header
{
    [[[UIAlertView alloc] initWithTitle:header message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

+ (void)showMessage:(NSString *)message
{
    [self showMessage:message withHeader:@"Spinar"];
}


@end
