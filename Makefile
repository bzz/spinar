BUILD_DIR= $(shell pwd)/build
SCHEME_NAME=Uniwheel
PROJECT_NAME=Spinar.xcodeproj
IPA_FILE=$(shell pwd)/ipa/$(SCHEME_NAME).ipa
 
TARGETS=build_app sign_ipa
 
ipa: $(TARGETS)
 
build_app:
	xcodebuild -project $(PROJECT_NAME) -scheme $(SCHEME_NAME) -configuration Release -sdk "iphoneos" build CONFIGURATION_BUILD_DIR=$(BUILD_DIR)
	zip -r $(SCHEME_NAME).app.dSYM.zip build/$(SCHEME_NAME).app.dSYM/
 
sign_ipa:
	mkdir -p ipa
	xcrun -sdk iphoneos PackageApplication $(BUILD_DIR)/$(SCHEME_NAME).app -o $(IPA_FILE) --verbose
 
clean:
	xcodebuild -project $(PROJECT_NAME) -scheme $(SCHEME_NAME) -configuration Release clean
	rm -rf build
	rm -rf ipa
	rm -rf $(SCHEME_NAME).app.dSYM.zip
